/**

 ChatWrapView is the view object that wraps 4 views:

 - ChatVideoView
 - ChatUtilitiesView
 - ChatStreamView
 - EditorView
 **/
App.ChatWrapView = Backbone.View.extend({

    className : 'eg-chat-wrap',

    template : _.template($('#tpl-wrap').html()),

    initialize : function() {

        $('#eg-chat-content').empty();

        //Initialize all the subviews
        App.chat_video_view = new App.ChatVideoView();
        App.chat_utilities_view = new App.ChatUtilitiesView();
        App.chat_stream_view = new App.ChatStreamView();
        App.editor = new App.Editor();
        App.altEngmtView = new App.AlternateEngagementView();

        $(window).on('resize.egain.chatwrap', _.bind(this.onWindowResize, this));
    },

    render : function() {
        resizeWindow(CONFIG_DIALOGUE_WIDGET_URL, eGainLiveConfig.chatWindowHeight);
        $('#eg-chat-header a.closechat').attr('title',L10N_WINDOW_END_BUTTON);

        this.$el.html(this.template());

        //Append the wrap to the content parent element
        $('#eg-chat-content').append(this.$el);

        //Append the wrapper element of the subviews.
        this.$('.video')
            .append(App.chat_video_view.el);

        this.$('.box')
            .append(App.chat_utilities_view.el)
            .append(App.chat_stream_view.el)
            .append(App.altEngmtView.el)
			.append(App.chat_close_view.el)
            .append(App.editor.el);

        //Render all the subviews
        App.chat_video_view.render();
        App.chat_utilities_view.render({
            'showVideo' : App.isVideoChatLicensed && eGainLiveConfig.showVideoButton && !App.utils.isVisitorMobile() ,
            'showAudio' : App.isVideoChatLicensed && eGainLiveConfig.showAudioButton && !App.utils.isVisitorMobile() ,
            'showSaveTranscript' : true && !App.utils.isVisitorMobile(),
            'showChangeFont' : true,
            'showPrintTranscript' : true,
            'showDivider' : false,
            'showFontDivider':true,
            'showOffRecord': App.isOffRecordEnabled,
            //show faq button only if kb url is configured
            'showFaq' : CONFIG_KNOWLEDGE_BASE_URL && CONFIG_KNOWLEDGE_BASE_URL.length>0
        });
        if( App.isVideoChatEnabled )
        {
            $("#videoBt").show();
            $("#audioBt").show();
            App.chat_video_view.adjustDirectChatButtons();
        }
        else
        {
            $("#videoBt").hide();
            $("#audioBt").hide();
        }
        App.altEngmtView.render();
        if(App.utils.isVisitorMobile())
            $('#eg-chat-content').addClass('noutilitiesbar');
        App.chat_stream_view.render();
        this.resize();
        if(typeof customDisableVideoHook != 'undefined')
            customDisableVideoHook();
        App.attachmentView = new App.AttachmentView();
        App.attachmentView.render();
    },

    showVideoButton: function(usrLicence) {
        var vCapable=usrLicence && eGainLiveConfig.showVideoButton && App.isVideoChatEnabled && App.isVideoChatLicensed;
        var aCapable=usrLicence && eGainLiveConfig.showAudioButton && App.isVideoChatEnabled && App.isVideoChatLicensed;
        var $videoButton = $("#videoBt");
        var $audioButton = $("#audioBt");
        if(vCapable)
        {
            $videoButton.show();
        }
        else
        {
            $videoButton.hide();
        }
        if(aCapable)
        {
            $audioButton.show();
        }
        else
        {
            $audioButton.hide();
        }

        if(vCapable || aCapable)
            App.chat_video_view.adjustDirectChatButtons();
    },
    showSidebar : function() {

        this.$el.animate({width:400});
    },

    fulfillWidth : function() {

        var width = $(window).width() - App.pageOffset;

        $('#eg-chat-content').animate({width:width});
        this.$el.animate({width:width});
        App.chat_stream_view.resize();
    },

    showOffHours : function(message){

	        //added code for onetag instrumentation
	        setTimeout(function(){
	            var EG_CALL_Q = window.EG_CALL_Q || [];
	            EG_CALL_Q.push(["send",
	                            "cht",
	                            "aac",
	                            document.URL,
	                            101,
	                            {
	                "EventName":"ChatOutOfHours"
	                            }
	            ]);},2000);
	        this.$el.html(this.template());

	        //Append the wrap to the content parent element
	        $('#eg-chat-content').append(this.$el);

	        this.$('.box')
	            .append(App.chat_stream_view.el);
	        App.chat_stream_view.render();
	        App.chat_stream_view.showOffHours(message);
    },

    showErrorPage : function(code){
        this.$el.html(this.template());

        //Append the wrap to the content parent element
        $('#eg-chat-content').append(this.$el);

        this.$('.box')
            .append(App.chat_stream_view.el);
        App.chat_stream_view.render();
        App.chat_stream_view.showErrorPage(code);
    },

    showGenericErrorPage : function(message){
        this.$el.html(this.template());

        //Append the wrap to the content parent element
        $('#eg-chat-content').append(this.$el);

        this.$('.box')
            .append(App.chat_stream_view.el);
        App.chat_stream_view.render();
        App.chat_stream_view.showGenericErrorPage(message);
    },

    showDuplicateSession : function() {
        this.$el.html(this.template());

        //Append the wrap to the content parent element
        $('#eg-chat-content').append(this.$el);

        this.$('.box')
            .append(App.chat_stream_view.el);
        App.chat_stream_view.render();
        App.chat_stream_view.showDuplicateSession();
    },

    showDomainMismatchError : function() {
        this.$el.html(this.template());
        //Append the wrap to the content parent element
        $('#eg-chat-content').append(this.$el);
        this.$('.box')
            .append(App.chat_stream_view.el);
        App.chat_stream_view.render();
        App.chat_stream_view.showDomainMismatchError();
    },

    resize : function() {

        //Resize height
        var windowHeight = $(window).height();
        this.$el.height(windowHeight - 300);

        var windowWidth = $(window).width();

        console.log(eGainLiveConfig.mi);

        //Resize width.
        //if the sidebar is visible and the eGainLiveConfig.showSidebar is true
        //,then resize to half the width
        //with some 20px padding buffer, if the windowWidth is still
        //wider than minWidthForShowingSidebar.
        if(eGainLiveConfig.minWidthForShowingSidebar < windowWidth
            &&
            eGainLiveConfig.showSidebar && App.chat_sidebar_view && App.chat_sidebar_view.isVisible())  {
            var width = windowWidth / 2 - App.pageOffset;
            this.$el.width(width);
            $('#eg-chat-content').css('width',width);
            $('#eg-chat-header').width(width);
            if(!App.utils.isVisitorMobile())
                this.$el.find('.submit-section').width(width);

        } else {

            //If the sidebar is not visible, then resize to whole window
            //width with 20px padding buffer.
            var width = windowWidth - App.pageOffset;
            this.$el.width(width);
            if(!App.utils.isVisitorMobile())
                this.$el.find('.submit-section').width(width);
        }
        App.altEngmtView.resize(windowWidth-App.pageOffset);
    },

    onWindowResize : function() {

        this.resize();
    }
});


/**

 ChatStreamView is the view object that is responsible for
 the stream of the chat messages views. It listens to the
 messenger for any new messages.

 **/
App.ChatStreamView = Backbone.View.extend({

    className : 'eg-chat-stream',
    chatPageurlbarshown: true,
    template : _.template($('#tpl-stream').html()),

    fonts : [
        '10pt', '12pt', '14pt', '16pt', '14pt', '12pt'
    ],
    events : {
        'keydown #wrapper' : 'keyPressEventHandlerScrollbar'
       },
       keyPressEventHandlerScrollbar: function(event){

     if (event.keyCode == 38) {
       // up arrow
         this.$('.window-view').tinyscrollbar_updatescroll(40);
         event.preventDefault();
     } else if (event.keyCode == 40) {
      // down arrow
         this.$('.window-view').tinyscrollbar_updatescroll(-40);
         event.preventDefault();
     }
   },

    fontSeq : 0,
    currFont : 10,

    initialize : function() {

        //Listen to the messenger's events.
        App.messenger.on('new-message', this.onNewMessage, this);
        App.messenger.on('chatter-start-typing',
            this.onChatterStartTyping, this);
        App.messenger.on('chatter-stop-typing',
            this.onChatterStopTyping, this);
        App.messenger.on('notification', this.onNotification, this);
        App.messenger.on('updateScrollOnImageAccept', this.onImageAccept, this);

        App.chat_video_view.on('show', this.onVideoShowing, this);
        $('body').on('selectstart', function(event){
            var target = $(event.originalEvent.target);
            if (target.parents('ul#chatbubbles').length) {
                //alert('Your clicked element is having div#hello as parent');
                return true;
            }
            event.preventDefault();
            event.stopPropagation();
            return false;
        });

        //Listen to window size change.
        $(window).on('resize.egain.chatstream', _.bind(this.onWindowSizeChange, this));

        //This is a map of temporary message bubbles, the key being
        //the typer's.
        this._temporaryMessageViews = {};
    },

    render : function() {


        this.$el.html(this.template());
        if(!App.utils.isVisitorMobile()) {
            this.$('.window-view').tinyscrollbar({ sizethumb: App.scrollsize });
        }
        if(App.utils.isVisitorMobile()) {
            this.scroll();

        }
        this.chatPageurlbarshown=true;
        setTimeout(function(){App.chat_stream_view.resize();},450);

    },

    scroll : function() {
        if(App.utils.isVisitorMobile()) {
            $.bind('touchmove',function(e){e.preventDefault();});
            this.myscroll = new iScroll('scroller',{zoom:true, hideScrollbar:false,onScrollEnd:function(){if (this.y==this.maxScrollY){$("html, body").animate({scrollTop: $(document).height()}, 100)}}});
        }
    },

    myscroll : null,
    onImageAccept: function() {
    	try{

            if(App.utils.isVisitorMobile()) {
                this.myscroll.refresh();
                this.myscroll.scrollTo(0, this.myscroll.maxScrollY);
            }
            if(!App.utils.isVisitorMobile()) {
            	scrollposition = Math.abs($('.overview').position().top);
                this.$('.window-view').tinyscrollbar_update(scrollposition);
            }
        }catch(error){}

    },


    //Callback that will be called everytime the messenger
    //receives a message.
    onNewMessage : function(message) {

        console.log('new message on', message);

        //Get the first name of the chatter.
        var firstName = message.get('author');

        //On new message, we'll check whether there is already a temporary
        //message view for the message author.
        var messageView = this._getTemporaryMessageView(firstName);

        //Render the new messages, passing along the messageView
        this._renderMessage(message, messageView);
        var attr = message.attributes;
        var sizePt = this.currFont +"pt";
        console.log('sizePt = ', sizePt);
        $(".bubble").css('font-size',sizePt);
        this.arrangeTypingMessagesAtEnd();
        //try catch to prevent error in IE
        try{

            if((this.myscroll != null) && App.utils.isVisitorMobile()) {
                this.myscroll.refresh();
                this.myscroll.scrollTo(0, this.myscroll.maxScrollY);
                {
                    if(!App.utils.isVisitorAndroid())
                        window.scrollTo(0,1);
                    var me = this;
                    setTimeout(function(){
                        var height = window.innerHeight-App.editor.$el.height()-App.editor.$el.offset().top-App.editor.$el.offset().bottom-App.submitSectionHeight;
                        me.$('.viewport').height(height);
                        me.$('.scrollbar').height(height);
                        me.$('.window-view').height(height);
                        window.scrollTo(0,$('#eg-chat-header').height()+$('#eg-chat-header').offset().top);
                        console.log('new message. height set. height='+height);
                    },300);
                }
            }
            else {
                this.$('.window-view').tinyscrollbar_update('bottom');
		    }
        }catch(error){}
    },

    arrangeTypingMessagesAtEnd : function(){
        for(author in this._temporaryMessageViews){
            var msg = this._temporaryMessageViews[author];
            if(msg.$el){
                var parent = msg.$el.parent();
                msg.$el.detach();
                parent.append(msg.$el);
            }
        }
        var sizePt = Math.floor(this.currFont*0.8) +"pt";
        console.log('typing sizePt = ', sizePt);
        $(".typing").css('font-size',sizePt);
    },

    increaseFontSize : function() {

        console.log('Currrent fontSize = ', this.currFont);
        this.currFont++;
        console.log('Font Size will be changed to = ', this.currFont);
        var sizePt = this.currFont +"pt";
        console.log('sizePt = ', sizePt);
        $(".bubble").css('font-size',sizePt);
        //try catch to prevent error in IE
        try{
            this.$('.window-view').tinyscrollbar_update('relative');
        }catch(error){}
    },

    decreaseFontSize : function() {
        console.log('fontSeq = ', this.fontSeq);
        if(this.currFont >=5)
        {   this.currFont--;
            console.log('Font Size will be changed to = ', this.currFont);
            var sizePt = this.currFont +"pt";
            console.log('sizePt = ', sizePt);
            $(".bubble").css('font-size',sizePt);
            //try catch to prevent error in IE
            try{
                this.$('.window-view').tinyscrollbar_update('relative');
            }catch(error){}
        }
    },

    //Callback that will be called everytime the server reports
    //that someone is typing.
    //Note : it needs the chatter name and also their type (agent or customer).
    onChatterStartTyping : function(chatter, type) {
        type = type || 'agent'; //make customer the default type

        //Get the first name of the chatter.
        var firstName = chatter||L10N_AGENT;
        var messageView = this._temporaryMessageViews[firstName];
        if(!messageView)
            this._createTemporaryMessageView(firstName, type);
        var sizePt = Math.floor(this.currFont*0.8) +"pt";
        console.log('typing sizePt = ', sizePt);
        $(".typing").css('font-size',sizePt);
    },

    //Temporary message view is basically a message view that shows a currently
    //typing message of a chatter. When that chatter finally submits a message,
    //we are going to reuse the same messageView to show the new message, so it
    //appears that the newly submitted message pops up within the same bubble.
    _createTemporaryMessageView : function(author, type) {

        var messageView = new App.MessageView({author:author, temporary:true, type:type});

        this._temporaryMessageViews[author] = messageView;

        this._insertMessageView(messageView, false);

         try{
            if(App.utils.isVisitorMobile()) {
                this.myscroll.refresh();
            }
           if(!App.utils.isVisitorMobile()) {

                //scroll to bottom only if the scrollbar is already at bottom
                var viewPortHeight = this.$('.window-view .scrollbar').height();
                var scrollPosition = this.$('.window-view .scrollbar .thumb').position().top;

                this.$('.window-view').tinyscrollbar_update(viewPortHeight - scrollPosition > 30 ? 'relative' : 'bottom');
           }
        }catch(error){}


        //This timeout is set to ensure that the customer is not seeing a stale
        //typing status in case the Agent browser console, after sending a typing message
        //to customer console, crashes.
        messageView.timeoutId = setTimeout(function(){
            App.chat_stream_view._deleteTemporaryMessageView(messageView);
        }, 300000);
    },

    _deleteTemporaryMessageView : function(messageView) {

        if(messageView && messageView.options.temporary) {

            console.log('delete temporary message');
            messageView.$el.fadeOut();
            // we should completely remove the messageView.
            setTimeout(function() {
                messageView.$el.remove();
                //Always scroll down to the bottom of the list.
                try{
                    if(App.utils.isVisitorMobile()) {
                        this.myscroll.refresh();
                      this.myscroll.scrollTo(0, this.myscroll.maxScrollY);
                    }
                    if(!App.utils.isVisitorMobile()) {
                        this.$('.window-view').tinyscrollbar_update('relative');
                    }
                }catch(error){}
            },800);

            delete this._temporaryMessageViews[messageView.options.author];
        }
    },

    _getTemporaryMessageView : function(author) {

        return this._temporaryMessageViews[author];
    },

    onChatterStopTyping : function(chatter) {

        var firstName = chatter||L10N_AGENT;
        var messageView = this._temporaryMessageViews[firstName];
        this._deleteTemporaryMessageView(messageView);
    },

    /**

     Callback that will be called when
     the window size changes.

     **/
    onWindowSizeChange : function(e) {

        console.log('on window size change!');
        this.resize();
    },

    onNotification : function(message) {

        var $hDiv = $('<div class="notification-message"/>');
        var ariaLabel = $('<div/>').html(message).text();
        var $notification = $('<span tabindex="0" role="application" aria-label="'+ariaLabel+'"/>');

        $notification.focus(function() {
            $(this).addClass('focused');
        });

        $notification.blur(function() {
            $(this).removeClass('focused');
        });

        $notification.html(message);

        $hDiv.append($notification);
        this.$('.window-view ul#chatbubbles').append($hDiv);
        $(".convertvideo").parent().parent().css("border-width","0px");
        $(".convertvideo").parent().parent().css("text-align","left");
        $(".convertvideo").parent().css("padding","0px");
        //Always scroll down to the bottom of the list
        this.arrangeTypingMessagesAtEnd();
        try{
            if(App.utils.isVisitorMobile()) {
                this.myscroll.refresh();
                this.myscroll.scrollTo(0, this.myscroll.maxScrollY);
            }
            if(!App.utils.isVisitorMobile()) {
                this.$('.window-view').tinyscrollbar_update('bottom');
            }
        }catch(error){}

        // Live notification for system messages
        $("#g_Notification").text($notification.text());

        /*For removing the typing-message if Agent leaves*/
        var agentIdentity = App.connection.agentIdentityForNotification;
        if(agentIdentity != null){
            var messageView = this._temporaryMessageViews[agentIdentity];
            if(typeof messageView != "undefined")
                this._deleteTemporaryMessageView(messageView);
        }
    },

    resize : function(decr) {

        var window_height = $(window).height();
        var offset = this.$el.find('.window-view').offset();

        console.log('offset', offset);
        console.log('window height', window_height);
        //110 for editor and  submit section
        var height = window_height - offset - (110+App.submitSectionHeight);
        //if off-record is enabled and mobile view we show 'off_record&send section'

        if($.browser.msie && $.browser.version == '7.0')
            height-=2;

        if(App.utils.isVisitorAndroid()){
            if(window.innerHeight < window.innerWidth)
            {
                height=115;
            }
            else
            {
                height=350;
            }
        }
        console.log('resize(decr): decr='+decr+', height='+height);
		if(!decr && App.utils.isVisitorIosPhone()){
            height= window.screen.height-offset.top-App.submitSectionHeight-185;
		}
        if(App.utils.isVisitorMobile() && App.isOffRecordEnabled)
            height -= 35;
		// decr value is mandatory for mobile browser to get inside this condition
        if((App.utils.isVisitorMobile() && decr) || decr){
            var tempHeight=height-decr;
            if(tempHeight > 15){
                height = tempHeight;
                this.$('.viewport').css('height',height);
                this.$('.scrollbar').height(height);
            }

            try{
                if(tempHeight > 15 && App.utils.isVisitorMobile()) {
                    this.myscroll.refresh();
    				this.myscroll.scrollTo(0, this.myscroll.maxScrollY);
                    {
                        if(!App.utils.isVisitorAndroid())
                            window.scrollTo(0,1);
                        var me = this;
                        setTimeout(function(){
                            window.scrollTo(0,$('#eg-chat-header').height()+$('#eg-chat-header').offset().top);
                        },300);
                    }
                }
                if(!App.utils.isVisitorMobile()) {
                    this.$('.window-view').tinyscrollbar_update('bottom');
                }
            }catch(error){}

            if(tempHeight > 15){
                this.$(".window-view").animate( { height:height}, {duration:500 });
            }
        }
        else if(App.editor.isfocus && App.utils.isVisitorMobile()) {
            this.myscroll.refresh();
            this.myscroll.scrollTo(0, this.myscroll.maxScrollY);
            if(!App.utils.isVisitorAndroid())
                window.scrollTo(0,1);
            var me = this;
            setTimeout(function(){
                var height = window.innerHeight-App.editor.$el.height()-App.editor.$el.offset().top-App.editor.$el.offset().bottom-App.submitSectionHeight;
                me.$('.viewport').height(height);
                me.$('.scrollbar').height(height);
                me.$('.window-view').height(height);
                window.scrollTo(0,$('#eg-chat-header').height()+$('#eg-chat-header').offset().top);
                console.log('new message. height set. height='+height);
            },300);
        }
        else
        {
            if ($('#alt-engmt-panel').is(':visible'))
            {
                height-=$('#outer').height();
            }
            else if($('#queueStatusTable').is(':visible'))
            {
                height-=$('#queueStatusTable').height();
            }

            this.$('.viewport').height(height);
            this.$('.scrollbar').height(height);
            this.$('.window-view').height(height);
            if((this.myscroll != null) && (typeof(this.myscroll) != 'undefined') &&!App.editor.isfocus && App.utils.hasMobile()){
                this.myscroll.refresh();
                this.myscroll.scrollTo(0, this.myscroll.maxScrollY);
            }
        }
        console.log('height', height);
        //try catch to prevent error in IE
        try{
            this.$('.window-view').tinyscrollbar_update('relative');
        }catch(error){}
        if((App.utils.isVisitorIos()) && (this.chatPageurlbarshown))
        {
            App.utils.hideAddressBar();
            this.chatPageurlbarshown=false;
        }
    },


    showConnectionError : function() {

        this.$('.notice').html(L10N_CONNFAIL).show();
    },

    /**

     Show off hour agent message.

     **/
    showOffHours : function(message) {
        this.$('.window-view').hide();
        if(message && message!=null)
        	this.$('.notice').html(message).show();
        else
        	this.$('.notice').html(L10N_OFF_HOURS).show();
    },

    showErrorPage : function(code) {
        this.$('.window-view').hide();
        this.$('.notice').html(App.utils.getFormattedMessage(L10N_SESSION_CREATION_FAILURE, [code])).show();
    },

    showGenericErrorPage:function(message) {
        this.$('.window-view').hide();
        this.$('.notice').html(App.utils.getFormattedMessage(message)).show();
    },

    showDuplicateSession : function() {
        this.$('.window-view').hide();
        this.$('.notice').html(L10N_SESSION_EXISTS).show();
    },

    showDomainMismatchError: function() {
        this.$('.window-view').hide();
        this.$('.notice').html(L10N_POST_DOMAIN_MISMATCH).show();
    },

    showTypingStatus : function(name, type) {


    },

    /**

     This is the specific animating resize function that
     will be called when the video is slided down.

     **/
    onVideoShowing : function(isShowingVideo) {

        console.log('on video showing', isShowingVideo);

        setTimeout(function(){App.chat_stream_view.resize()},370);
    },

    _renderMessage : function(message, messageView) {

        if(messageView) {
            //If there is already a temporaru messageView, insert the message to the
            //messageView, allowing it to transform from a temporary to a real one.
            messageView.insertMessage(message);

        } else {
            var messageView = new App.MessageView({message:message});
            //Insert the messageView
            this._insertMessageView(messageView, true);
        }
    },

    /**

     Inserts a single message view to the stream.

     @param : An App.Message model instance.

     **/
    _insertMessageView : function(messageView, isMessage) {

        this.$('ul#chatbubbles').append(messageView.el);

        //Render the message view.
        messageView.render(true);

        //Always scroll down to the bottom of the list in case of message.
        if(isMessage)
        {
          try{
                if(App.utils.isVisitorMobile()) {
                    this.myscroll.refresh();
                    this.myscroll.scrollTo(0, this.myscroll.maxScrollY);
             }
               if(!App.utils.isVisitorMobile()) {
                  this.$('.window-view').tinyscrollbar_update('bottom');
               }
           }catch(error){}
        }
    }
});


/**

 ChatUtilitiesView responsible for auxiliary functionalities such as
 saving, printing, call, increase and decrease font size.

 **/

App.ChatUtilitiesView = Backbone.View.extend({

    className : 'eg-chat-utilities',

    template : _.template($('#tpl-utilities').html()),

    events : {

        'click #videoBt' : 'onShowVideoClick',
        'click #audioBt' : 'onShowAudioClick',
        'mouseover #videoBt' : 'onVideoMouseOver',
        'mouseout #videoBt' : 'onVideoMouseOut',
        'mouseover #audioBt' : 'onAudioMouseOver',
        'mouseout #audioBt' : 'onAudioMouseOut',
        'click #endCallBt' : 'onShowEndCallClick',
        'click .js-save' : 'onSaveClick',
        'click .js-print' : 'onPrintClick',
        'click .js-click_to_call' : 'onClickToCallClick',
        'click .js-faq' : 'onFaqClick',
        'click .increase-font' : 'onIncreaseFontClick',
        'click .decrease-font' : 'onDecreaseFontClick',
        'keyup .increase-font' : 'onIncreaseFontEnter',
        'keyup .decrease-font' : 'onDecreaseFontEnter',
        'click .off-record' : 'onClickOffRecord',
        'mouseover .off-record' : 'onHoverOffRecord'
    },

    initialize : function() {
        App.offRecord = false;

    },

    render : function(options) {

        //We can put these settings on eGainLiveConfig.js
        this.$el.html(this.template(options));
        if(options.showOffRecord){
            this.$('a').addClass('off-record-enabled');
            this.$('a.off-record').css('display','block');
        }


    },

    onSaveClick : function() {
        // added for onetag instrumentation
        var EG_CALL_Q = window.EG_CALL_Q || [];
        if(App.connection.chatExited===true){
            EG_CALL_Q.push(["send",
                            "cht",
                            "uac",
                            document.URL,
                            101,
                            {
                "EventName":"ChatSurveyMenuButton",
                "ButtonDesc":"Save-Transcript"}]);

        }
        else{
            EG_CALL_Q.push(["send",
                            "cht",
                            "uac",
                            document.URL,
                            101,
                            {
                "EventName":"ChatMenuButton",
                "ButtonDesc": "save"
                            }
            ]);
        }
        chatSave();
    },

    onPrintClick : function() {
        //code added for onetag instrumentation
        var EG_CALL_Q = window.EG_CALL_Q || [];
        if(App.connection.chatExited===true){

            EG_CALL_Q.push(["send",
                            "cht",
                            "uac",
                            document.URL,
                            101,
                            {
                "EventName":"ChatSurveyMenuButton",
                "ButtonDesc":"Print-Transcript"}]);
        }
        else{
            EG_CALL_Q.push(["send",
                            "cht",
                            "uac",
                            document.URL,
                            101,
                            {
                "EventName":"ChatMenuButton",
                "ButtonDesc": "print"
                            }
            ]);

        }
        chatPrint();
    },

    onShowVideoClick : function(e) {
        // code added for onetag instrumentation
        var EG_CALL_Q = window.EG_CALL_Q || [];
        EG_CALL_Q.push(["send",
                        "cht",
                        "uac",
                        document.URL,
                        101,
                        {
            "EventName":"ChatMenuButton",
            "ButtonDesc": "video"
                        }
        ]);

        var $videoButton = $("#videoBt");
        if($videoButton.hasClass('inactive'))
            return;
        e.preventDefault();
        App.chat_video_view.videoClick();

    },

    onShowAudioClick : function(e) {
        // code added for onetag instrumentation
        var EG_CALL_Q = window.EG_CALL_Q || [];
        EG_CALL_Q.push(["send",
                        "cht",
                        "uac",
                        document.URL,
                        101,
                        {
            "EventName":"ChatMenuButton",
            "ButtonDesc": "voice"
                        }
        ]);

        var $audioButton = $("#audioBt");
        if($audioButton.hasClass('inactive'))
            return;
        e.preventDefault();
        App.chat_video_view.audioClick();
    },

    onShowEndCallClick : function(e) {

        e.preventDefault();
        App.chat_video_view.endCallClick();
    },

    onDecreaseSizeClick : function(e) {

        e.preventDefault();
    },

    onFaqClick : function(e) {
        e.preventDefault();
        if(CONFIG_KNOWLEDGE_BASE_URL && CONFIG_KNOWLEDGE_BASE_URL.length>0){
            var url = CONFIG_KNOWLEDGE_BASE_URL+ '#q='  + escape(App.session.get('subject'));
            var target = CONFIG_KNOWLEDGE_BASE_TARGET;
            if (target == '_parent') {
                window.opener.location.href = url;
            } else if (target == '_self') {
                window.location.href = url;
            } else if (target == '_top') {
                window.top.location.href = url;
            } else if (target == '_new') {
                window.open(url);
            } else {
                window.open(url, target, CONFIG_KNOWLEDGE_BASE_FEATURES);
            }
        }
    },



    onClickToCallClick : function(e) {

        e.preventDefault();

        this.trigger('click-to-call');
    },

    onIncreaseFontClick : function(e) {
         // added for onetag instrumentation
        var EG_CALL_Q = window.EG_CALL_Q || [];
        EG_CALL_Q.push(["send",
                        "cht",
                        "uac",
                        document.URL,
                        101,
                        {
            "EventName":"ChatMenuButton",
            "ButtonDesc": "increaseFontSize"
                        }
        ]);

        e.preventDefault();

        App.chat_stream_view.increaseFontSize();
    },

    onIncreaseFontEnter : function(e) {

        if(e.which === 13){
            e.preventDefault();

            App.chat_stream_view.increaseFontSize();
        }
    },

    onDecreaseFontEnter : function(e) {

        if(e.which === 13){
            e.preventDefault();

            App.chat_stream_view.decreaseFontSize();
        }
    },

    onDecreaseFontClick : function(e) {
        //added for onetag instrumentation
        var EG_CALL_Q = window.EG_CALL_Q || [];
        EG_CALL_Q.push(["send",
                        "cht",
                        "uac",
                        document.URL,
                        101,
                        {
            "EventName":"ChatMenuButton",
            "ButtonDesc": "decreaseFontSize"
                        }
        ]);

        e.preventDefault();

        App.chat_stream_view.decreaseFontSize();
    },

    /**
    Function called when pointer goes over video button
    */
    onVideoMouseOver : function(e){
        $videobt = $("#videoBt");
        if(!$videobt.hasClass('active') && !$videobt.hasClass('inactive'))
            $videobt.addClass('hover');
        return true;
   },
   /**
    Function called when pointer goes out of video button
    */
   onVideoMouseOut : function(e){
       $videobt = $("#videoBt");
       if($videobt.hasClass('hover'))
           $videobt.removeClass('hover');
   },
    /**
     Function called when pointer goes over audio button
     */
    onAudioMouseOver : function(e){
        $audiobt = $("#audioBt");
        if(!$audiobt.hasClass('active') && !$audiobt.hasClass('inactive'))
            $audiobt.addClass('hover');
        return true;
    },
    /**
     Function called when pointer goes out of audio button
     */
    onAudioMouseOut : function(e){
        $audiobt = $("#audioBt");
        if($audiobt.hasClass('hover'))
            $audiobt.removeClass('hover');
    },

    onClickOffRecord : function(){
        var msg;
        var cmd;
        if(App.offRecord){
            App.offRecord = false;
            msg = L10N_ON_RECORD_MESSAGE;
            cmd = 'onrecord';
        }
        else{
            App.offRecord =true;
            msg = L10N_OFF_RECORD_MESSAGE;
            cmd = 'offrecord';
        }
        if(App.isChatAttachmentEnabled){
            App.editor.showHideAttachmentIcon(App.isChatAttachmentEnabled,  App.offRecord);
            App.attachmentView.setDropNotAllowedMessage(L10N_OFF_RECORD_WARNING_MESSAGE);
        }

        if(App.connection.sendSystemMessage(msg,cmd)){
            App.messenger.showNotification(msg,'headline');
            if(cmd == 'offrecord'){
                App.offRecord =true;
                this.$('.off-record').addClass('off-record-inactive');
            }
            else{
                App.offRecord = false;
                this.$('.off-record').removeClass('off-record-inactive');
            }
        }
    },

    onHoverOffRecord : function(){
        if(App.offRecord)
            this.$('.off-record').attr('title', L10N_HOVER_ON_RECORD);
        else
            this.$('.off-record').attr('title',L10N_HOVER_OFF_RECORD);
    }
});

/**

 ChatVideoView is the view that wraps Adobe Flash Video Chat object.

 **/

App.ChatVideoView = Backbone.View.extend({

    className : function () {
        if( window.navigator.userAgent.indexOf("Trident") == -1 ) {
            return 'eg-chat-video';
        } else {
            return 'ie-eg-chat-video';
        }
    },

    mediaServerObject : null,

    template : _.template($('#tpl-video').html()),

    events : {

        'click .pause-video' : 'pauseVideo',
        'click .resume-video' : 'resumeVideo',
        'mouseenter' : 'onMouseEnter',
        'mouseleave' : 'onMouseLeave'
    },

    initialize : function() {

        this.videoStopped = false;
        this.videoInitialized = false;
        this.paused = false;
        this.video = new App.Video();
    },

    initializeVideo : function(val) {

        this.videoStopped = false;
        this.videoInitialized = val;
        if(typeof customEnableVideoHook != 'undefined')
            customEnableVideoHook();
        $('#endCallBt').addClass('active');
        $('#endCallBt').attr('title', L10N_END_CALL);

        this.show();
    },

    hideVideo : function() {
        if(this.video.videoShowing)
            this.show(false);
    },

    isVideoShowing : function() {
        return this.video.videoShowing;
    },
 // Deprecated:  was used by Wowza but is not used by TokBox
    setVideoChatConfig : function(obj) {
        obj.send = true;
        obj.agent = 0;
        var avmode = App.connection.getAvMode();
        if(avmode == AGENT_VIDEO_MODE || avmode == AGENT_AUDIO_MODE){
            obj.video = false;
            obj.audio = false;
        }
        else if (avmode == TWO_WAY_AUDIO_MODE || avmode == TWO_WAY_AUDIO_AGENT_VIDEO_MODE){
            obj.video = false;
            obj.audio = true;
        }
        else{
            obj.video = true;
            obj.audio = true;
        }
        obj.publishid =  "customer_" + App.connection._connection.sid;
        obj.subscribeid =  "agent_" + App.connection._connection.sid;
        obj.serverurl = this.mediaServerObject.mediaServerURL;
        obj.isSecure = (this.mediaServerObject.mediaServerSecEnabled == '1') ? true : false,
        obj.secureToken = this.mediaServerObject.mediaServerSecToken;
        obj.doConnectToken = this.mediaServerObject.mediaServerSecConnectParam;
        obj.doPlayToken = this.mediaServerObject.mediaServerSecPlayParam ;
        obj.doPublishToken = this.mediaServerObject.mediaServerSecPublishParam;
        obj.avmode = avmode;
        return obj;
    },

    render : function() {

        this.$el.html(this.template());
        this.$el.hide();
        //Adding browser specific flash class
        if( window.navigator.userAgent.indexOf("Trident") == -1 ) {
            $('#flash-wrap-id').addClass('flash-wrap');
        } else {
            $('#flash-wrap-id').addClass('ie-flash-wrap');
        }
    },

    adjustDirectChatButtons : function() {
        var $videoButton = $("#videoBt");
        if (App.isDirectVideoChatEnabled)
        {
        	$videoButton.attr('title',L10N_VIDEO_CHAT);
        	$videoButton.removeClass('inactive');
        }
        else
        {
        	$videoButton.removeAttr('title');
        	$videoButton.addClass('inactive');
       }

        var $audioButton = $("#audioBt");
        if (App.isDirectAudioChatEnabled)
        {
        	$audioButton.attr('title',L10N_AUDIO_CHAT);
        	$audioButton.removeClass('inactive');
        }
        else
        {
        	$audioButton.removeAttr('title');
        	$audioButton.addClass('inactive');
        }
    },

    show : function(notify) {
        if(!App.connection.sid)
            return;
        var $videoButton = $("#videoBt");
        var $audioButton = $('#audioBt');
        var $endCallButton = $('#endCallBt');

        if(this.videoInitialized && ! this.videoStopped)
            this.$el.slideToggle(function(){
                App.chat_stream_view.resize();
            });

        if(this.$el.hasClass('visible')) {

            this.stopVideo();
            this.videoStopped = true;

            //Check if the close message needs to sent to the agent.
            //Close should not be sent if the close is a result of agent closing the video
            if(typeof notify == 'undefined' || notify == true)
            {
                var avmode = App.connection.getAvMode();
            	var isVideo = $videoButton.hasClass('active') || avmode == TWO_WAY_AUDIO_VIDEO_MODE || avmode == TWO_WAY_AUDIO_AGENT_VIDEO_MODE || avmode == AGENT_VIDEO_MODE;
                var msg = isVideo ? video_chat_ended_cust : audio_chat_ended_cust;
                msg = msg.replace('{0}',App.session.get('name'));
                var cssClass = 'chatInput headline';
                if(App.connection.sendNormalMessage(msg, 'headline', 'stopVideo'))
                    App.messenger.showNotification(msg,cssClass);
            }

            this.video.videoShowing = false;
            $videoButton.removeClass('active');
            $videoButton.attr('tabIndex','-1');
            //audio to off state
            $audioButton.removeClass('active');
            $audioButton.attr('tabIndex','-1');
            this.adjustDirectChatButtons();
            //end call to off state
            $endCallButton.removeClass('active');
            $endCallButton.removeAttr('title');
            $endCallButton.addClass('inactive');
            $endCallButton.attr('tabIndex','-1');
            this.$el.toggleClass('visible');
            //Pass isVideoShowing as false
            this.trigger('show', false);

        } else {

            if (this.videoStopped)
            {
                this.video.invite2();
            }
            else if(this.videoInitialized)
            {
                //Get the flash wrap element.
                if( window.navigator.userAgent.indexOf("Trident") == -1 ) {
                    var flashWrap = this.$('.flash-wrap')[0];
                } else {
                    var flashWrap = this.$('.ie-flash-wrap')[0];
                }
                //And pass it to the video model.
                this.video.show(flashWrap);

                var avmode = App.connection.getAvMode();
                if (App.videoChatMaxEscalation != 2 && App.videoChatMaxEscalation != 4){
                	$videoButton.removeClass('inactive');
                	if(avmode == TWO_WAY_AUDIO_VIDEO_MODE ||
                                    avmode == TWO_WAY_AUDIO_CUSTOMER_VIDEO_MODE){
                        // video on
                        $videoButton.addClass('active');
                        $videoButton.attr('tabIndex','0');
                        $videoButton.attr('title',L10N_PAUSE_VIDEO);
                    }
                    else{
                        // video off
                        $videoButton.removeClass('active');
                        $videoButton.attr('tabIndex','-1');
                        $videoButton.attr('title',L10N_RESUME_VIDEO);
                    }
                }

                $audioButton.removeClass('inactive');
                if(avmode == AGENT_VIDEO_MODE || avmode == AGENT_AUDIO_MODE){
                    // audio off
                    $audioButton.removeClass('active');
                    $audioButton.attr('tabIndex','-1');
                    $audioButton.attr('title',L10N_UNMUTE);

                    $endCallButton.hide();
                    $audioButton.hide();
                    $videoButton.hide();
                }
                else{
                    // audio on
                    $audioButton.addClass('active');
                    $audioButton.attr('tabIndex','0');
                    $audioButton.attr('title',L10N_MUTE);
                }

                $endCallButton.removeClass('inactive');
                $endCallButton.addClass('active');
                $endCallButton.attr('tabIndex','0');
                $endCallButton.attr('title',L10N_END_CALL);
                this._hideTimeout = setTimeout(_.bind(this.hideControls, this),
                    1000);

                //Pass isVideoShowing as true
                this.trigger('show', true);
                this.$el.toggleClass('visible');

                var msg = video_chat_started.replace('{0}',(agentName == '') ? App.session.get('name') : agentName);
                var cssClass = 'chatInput headline';
                if(App.connection.sendNormalMessage(msg, 'headline', 'startVideo'))
                    App.messenger.showNotification(msg,cssClass);
            }
            else
            {
                this.video.invite();
            }
        }


    },

    pauseVideo : function() {
        this.paused = true;
        this.$('.pause-video').hide();
        this.$('.resume-video').show();
        this.video.pause();

        //Send system message for pause video action
        App.connection.sendSystemMessage(App.utils.getFormattedMessage(customer_pause_video_agent_console,[App.session.get('name')]),CMD_PAUSE_RESUME_VIDEO)
    },

    resumeVideo : function() {
        this.paused = false;
        this.$('.pause-video').show();
        this.$('.resume-video').hide();
        this.video.resume();

        //Send system message for resume video action
        App.connection.sendSystemMessage(App.utils.getFormattedMessage(customer_resume_video_agent_console,[App.session.get('name')]),CMD_PAUSE_RESUME_VIDEO)
    },


    stopVideo : function(callback) {
        this.video.stop(callback);
    },


    pauseAudio : function() {

        this.video.mike(false);

        //Send system message for mute action
        App.connection.sendSystemMessage(App.utils.getFormattedMessage(customer_mute_audio_agent_console,[App.session.get('name')]),CMD_MUTE_UNMUTE_AUDIO)
    },

    resumeAudio : function() {

        this.video.mike(true);

        //Send system message for unmute action
        App.connection.sendSystemMessage(App.utils.getFormattedMessage(customer_unmute_audio_agent_console,[App.session.get('name')]),CMD_MUTE_UNMUTE_AUDIO)
    },

    videoClick : function(e){
        if(typeof App.chat_video_view.video.videoShowing != 'undefined' && App.chat_video_view.video.videoShowing){
            var $videoButton = $("#videoBt");

            if($videoButton.hasClass('active')) {
                $videoButton.removeClass('active');
                $videoButton.attr('tabIndex','-1');
                $videoButton.attr('title', L10N_RESUME_VIDEO);
                this.pauseVideo();
            } else {
                $videoButton.addClass('active');
                $videoButton.attr('tabIndex','0');
                $videoButton.attr('title', L10N_PAUSE_VIDEO);
                this.resumeVideo();
            }
        } else {
            App.connection.avmode = TWO_WAY_AUDIO_VIDEO_MODE;
            this.show();
        }
    },

    audioClick : function(e){
        if(typeof App.chat_video_view.video.videoShowing != 'undefined' && App.chat_video_view.video.videoShowing){
            var $audioButton = $("#audioBt");

            if($audioButton.hasClass('active')) {
                $audioButton.removeClass('active');
                $audioButton.attr('tabIndex','-1');
                $audioButton.attr('title', L10N_UNMUTE);
                this.pauseAudio();
            } else {
                $audioButton.addClass('active');
                $audioButton.attr('tabIndex','0');
                $audioButton.attr('title', L10N_MUTE);
                this.resumeAudio();
            }
        } else {
            App.connection.avmode = TWO_WAY_AUDIO_MODE;
            this.show();
        }
    },

    endCallClick : function(e){

        this.show();
    },

    onMouseEnter : function() {

    	var avmode = App.connection.getAvMode();
        if(avmode != AGENT_VIDEO_MODE && avmode != AGENT_AUDIO_MODE){
            if(this._hideTimeout)
                clearTimeout(this._hideTimeout);

            this.showControls();
        }
    },

    onMouseLeave : function() {

        this._hideTimeout = setTimeout(_.bind(this.hideControls, this), 500);
    },

    showControls : function() {
        if(this.paused){
            this.$('.pause-video').hide();
            this.$('.resume-video').show();
        }
        else{
            this.$('.pause-video').show();
            this.$('.resume-video').hide();
        }
//        this.$('.flash-controls').fadeIn();
    },

    hideControls : function() {

//        this.$('.flash-controls').fadeOut();
    }
});


/**

 ChatSidebarView is the view that wraps the sidebar.

 **/
App.ChatSidebarView = Backbone.View.extend({

    el : $('#eg-chat-sidebar'),

    initialize : function() {

        $(window).on('resize.egain.sidebar', _.bind(this.onWindowResize, this));
    },

    render : function() {
        var url = 'blank.html';
        if (url && url.length > 0) {
            this.$sideFrame = $('<iframe>').attr({'src': url,'id':'sideFrame'}).css({width:'100%',height:'100%',border:0,background:'white'})
                .appendTo(this.$el.find('.sideFrameDiv'));
        }
        this.resize();
    },

    setURL : function(url){
        document.getElementById('sideFrame').contentWindow.location.href = url;
    },

    isVisible : function(){

        return this.$el.hasClass('visible');
    },

    resize : function() {

        var windowHeight = $(window).height();
        var windowWidth = $(window).width();

        this.$el.height(windowHeight);
        //bottom and top padding 22px
        this.$el.find('.sideFrameDiv').height(windowHeight - 22);
        //If the width is less than the minmum with for showing sidebar, then we'll just hide it.
        if(eGainLiveConfig.minWidthForShowingSidebar > windowWidth || !eGainLiveConfig.showSidebar) {
            this.hide();
        } else {
            this.show();
        }
        this.$el.width(windowWidth / 2 - App.pageOffset/2);
    },

    onWindowResize : function() {

        this.resize();
    },

    show : function() {
        if(!this.isVisible()) {
            this.$el.show();
            this.$el.addClass('visible');
            this.$el.animate({'margin-right' : 0});
        }
    },

    hide : function() {
        this.$el.removeClass('visible');
        var width = $(window).width()-App.pageOffset;
        $('#eg-chat-content').width(width);
        $('#eg-chat-header').width(width);
        this.$el.hide();
    }
});

App.ChatCloseView = Backbone.View.extend({

    template : _.template($('#tpl-close').html()),
	events : {

    },

    initialize : function() {

    },

    render : function() {
        var url = 'blank.html';
		this.$el.html(this.template());
    },
	onCancel: function() {
           if(App.editor.ckeditor)
					App.editor.ckeditor.focus();
    },
	onOk: function() {
            var EG_CALL_Q = window.EG_CALL_Q|| [];
				EG_CALL_Q.push(["send",
				                "cht",
				                "uac",
				                document.URL,
				                101,
				                {
					"EventName":"ChatExit",
					"Termination-Initiation":"User" }]);

				App.connection.chatExited=true;
				App.connection.logout();
    },
	hide : function() {

        this.$el.hide();
    }

});


App.ChatHeaderView = Backbone.View.extend({

});

var agentCssList = new Array();
/**

 MessageView is the view object that is responsible
 for a single chat message. It shows the data of
 an App.Message instance.

 **/
App.MessageView = Backbone.View.extend({

    template : _.template($('#tpl-message').html()),

    className : 'eg-chat-message',

    events : {
        'focus' : 'onFocus',
        'blur' : 'onBlur'
    },

    initialize : function(options) {

        this.message = options.message;

        this.temporary = !!options.temporary;
        this.author = options.author;
        this.type = options.type;
        this.cssList = new Array('','agent3','agent2','agent1');
        console.log("TEMPORARY", this.temporary);
    },

    render : function() {

        var _this = this;
        var typingStatus = false;

        this.$el.attr('tabIndex', '0');
        this.$el.attr('role', 'application');

        if(this.temporary) {

            this.$el.html(this.template({temporary:this.temporary, author:this.author}));
            this.temporary = false;
            var dotCount = 1;

            //Add the message type class
            this.$el.addClass(this.type);
            if(this.type == 'agent')
            {
                typingStatus = true;
                var cssClass = this.getCssClass(this.author);
                if(cssClass != undefined && cssClass!= '' )
                    this.$el.addClass(cssClass);
            }
            //Apply the right-to-left text direction to the body element if the config says so.
            if(chattextClass === 'chattextRtl') {

                this.$('.body').css({'direction':'rtl'});
            }
            this.$el.find(".author").addClass("typing");
            this.$el.find(".bubble").addClass("typing");

        } else {

            this.$el.html(this.template({temporary:this.temporary, message:this.message}));

            //Add the message type class
            this.$el.addClass(this.message.get('type'));
            if(this.message.get('type') == 'agent')
            {
                var cssClass = this.getCssClass(this.message.get('author'));
                if(cssClass != undefined && cssClass!= '' )
                    this.$el.addClass(cssClass);
            }

            // Add aria-label to div element for screen reader
            var label = this.getAriaLabel(this.message.get('author'), this.message.get('body'), this.message.get('time'));
            try{
                this.$el.attr('aria-label', label);
            }catch(error){
                this.$el[0]['aria-label'] = label;
            }
        }

        this.$el.fadeIn(function(){
            //try catch to prevent error in IE, scroll in case it's not typing status
            try{
                if(App.chat_stream_view) {
                    if (!typingStatus)
                      App.chat_stream_view.$('.window-view').tinyscrollbar_update('bottom');
                }
            }catch(error){}
        });
    },

    // 'focused' class added to add border style for keyboard navigation
    onFocus : function() {
        this.$el.addClass('focused');
    },

    onBlur : function(e) {
        this.$el.removeClass('focused');
    },

    getFormattedTranscriptMessage: function(msg) {
        var transcriptMessage = msg.replace(/<img[^>]*alt[=]\s*["']?([^\s"']*)["']?[^>]*>/gi,",$1 "+L10N_EMOTICON+",");
        transcriptMessage = transcriptMessage.replace(/<[^>]*>/gi,'');
        transcriptMessage = transcriptMessage.replace('&nbsp;',' ');
        return transcriptMessage;
    },

    getNotificationMessage: function(author, msg) {
        var notificationMessage = this.getFormattedTranscriptMessage(msg);
        notificationMessage = App.utils.getFormattedMessage(L10N_MESSAGE_NOTIFICATION, [author, notificationMessage]);
        return notificationMessage;
    },

    getAriaLabel: function(author, msg, time) {
        var ariaLabel = this.getFormattedTranscriptMessage(msg);
        ariaLabel = App.utils.getFormattedMessage(L10N_MESSAGE_LABEL, [author, ariaLabel, time]);
        return ariaLabel;
    },

    getCssClass : function(agent){
        var cls ='';
        var cssLen = 0;
        var id = 0;
        cls = agentCssList[agent];
        if(cls == undefined || cls == null)
        {
            for(a in agentCssList)
                cssLen++;
            id = cssLen%this.cssList.length;
            cls = agentCssList[agent] = this.cssList[id];
        }
        return cls;
    },

    //This is a method that transform a messageView to a real one.
    //(Basically it just inserts the body and the bottom information)
    insertMessage : function(message) {

        if(this._interval) clearInterval(this._interval);
        if(this.timeoutId) clearTimeout(this.timeoutId)
        this.$el.hide();
        var parent = this.$el.parent();
        this.$el.detach();

        this.options.temporary = false;
        console.log('new message', message);
        this.$el.find(".author").removeClass("typing");
        this.$el.find(".bubble").removeClass("typing");

        this.$el.find('.body').html(message.get('body'));
        this.$el.find('.bottom').html('<b>'+message.get('author') + '</b> <span aria-hidden="true">&bull;</span> ' + new Date().format(L10N_TIME_FORMAT));
        App.chat_stream_view._temporaryMessageViews[message.get('author')] = null;
        delete App.chat_stream_view._temporaryMessageViews[message.get('author')];
        parent.append(this.$el);
        this.$el.fadeIn(function(){
            //try catch to prevent error in IE
            try{
                if(App.utils.isVisitorMobile()) {
                    App.chat_stream_view.myscroll.refresh();
                    App.chat_stream_view.myscroll.scrollTo(0, this.myscroll.maxScrollY);
                }
                if(!App.utils.isVisitorMobile()) {
                    if(App.chat_stream_view){
                        App.chat_stream_view.$('.window-view').tinyscrollbar_update('bottom');
                    }
                }
            }catch(error){}
        });

        // Add aria-label to div element for screen reader
        var label = this.getAriaLabel(message.get('author'), message.get('body'), message.get('time'));
        try{
            this.$el.attr('aria-label', label);
        }catch(error){
            this.$el[0]['aria-label'] = label;
        }

        // Live notification for messages sent by agent
        $("#g_Notification").text(this.getNotificationMessage(message.get('author'), this.$el.find('.body').html()));
    }
});