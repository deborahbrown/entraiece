App.transcript = {
	getTranscriptTemplate : function(){	
		var templateName = getUrlParameter('templateName');		
		if(templateName){	
			$.ajax({
				url : 'transcript/transcript.properties',
				type: 'GET',
				dataType : 'text',
				success : function(data){ 										
					chatTranscript.content = data;
					App.transcript.checkTranscriptFileVersion(data, templateName);
				}
			});
		}
	},
	checkTranscriptFileVersion : function(transcript, templateName){
		var hash = CryptoJS.MD5(transcript);
		$.ajax({
			url : App.connection.BOSH_SERVICE+'/checktranscriptfileversion/'+templateName,
			type: 'POST',
			data: {md5: hash.toString()},
			dataType : 'text',
			success : function(data){ 
				if(data == 'false')
				App.transcript.postTranscriptTemplate(transcript, templateName, hash.toString());
			}
		});
	},

	postTranscriptTemplate: function(transcript, templateName, md5){
		$.ajax({
			url : App.connection.BOSH_SERVICE+'/posttranscripttemplate/'+templateName,
			type: 'POST',
			data: {transcript: transcript, md5:md5}
		});
	}
}
