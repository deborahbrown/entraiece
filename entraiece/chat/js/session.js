/**

    Session is the single model that constitutes a single chat session.
    Upon login submission success, App.Session will be instantiated and it will set up submodels (Connection) and subviews (ChatWrapView) that it needs.

**/
App.Session = Backbone.Model.extend({

	initialize : function(){
		 //Create the connection object
		App.connection = new App.Connection();
		App.audio = new App.Audio();
	},
    start : function() {
        
        //Initialize the messenger
        App.messenger = new App.Messenger();

        if(eGainLiveConfig.enableChatDeflection &&  (typeof eGainLiveConfig.chatDeflectionPortalId != "undefined" && eGainLiveConfig.chatDeflectionPortalId != "")){
            //Initialize the deflection 
            App.deflection = new App.Deflection();
            App.deflection.render();
        }
        
        else{
            //connect to the server
            App.connection.connect(this.attributes.email);


            //Initialize the chat wrap view
            App.chat_wrap_view = new App.ChatWrapView();
            App.chat_wrap_view.render();

            this.trigger('start');
        }
    },

    login : function(info) {

		var d = new Date();
		App.waitTimeInQueueStart = d.getTime();
        this.set(info);
		this.start();
    },

    startWithInitialMessage : function(firstMessageString, isDataMasked) {

        //start should be after a successful login

        setTimeout(function() {

        	if(typeof firstMessageString != "undefined" && firstMessageString != '')
        	{
	          
	            //Submit the initial message as the customer.
				firstMessageString = firstMessageString.replace(/\n/g,'<br/>');
	            App.messenger.submitMessageString(firstMessageString, 'customer','Customer');
	        }

			if(isDataMasked && App.connection.sendSystemMessage(L10N_SENSITIVE_DATA_MASKED,'headline'))
				App.messenger.showNotification(L10N_SENSITIVE_DATA_MASKED,'headline');
        }, 500);
    },

    end : function() {
		//unbind resize event from window not showing
		$(window).off('.egain.chatwrap');
		$(window).off('.egain.chatstream');
		if(App.chat_sidebar_view){
			App.chat_sidebar_view.hide();
			$(window).off('.egain.sidebar');
		}
		if(App.chat_close_view){
			App.chat_close_view.hide();
		}

		if(App.editor.ckeditor){
			try {
			App.editor.ckeditor.destroy();
			} catch (e) {
				//nothing to do as we received exception while destroying the session
			}
		}
		//Initialize the SurveyView and renders it.
		//If the video was showing first stop and remove the video and then render the survey
		if(App.chat_video_view.isVideoShowing()){
			App.chat_video_view.stopVideo(function(){
				App.session.showSurvey();
			});
		}
		else
		{
			this.showSurvey();
		}

		this.trigger('end');
    },

     showSurvey : function() {
		if(typeof App.session.backStoppingTriggered != "undefined" && App.session.backStoppingTriggered)
		{
			//Not loading survey page, when back-stopping is triggered,
			//Calling the hook to load or do different things one backstopping is triggered
			onBackStoppingLoadPageHook();
		}
		else
		{
			if(!eGainLiveConfig.turnOffChatSurveyPage)
			{
				//Initialize the Survey and renders it.
				App.surveyView = new App.SurveyView();
				App.surveyView.render();
			}
			else
			{
				//Initialize the ThanksView and renders it.
				App.thanksView = new App.ThanksView();
        		App.thanksView.render();
			}
		}

    },

	//On backStopping triggered
	onBackStoppingTriggered : function(egainAdditionalParams){
			App.session.backStoppingTriggered = true;
			onBackStoppingTrigerredHook(egainAdditionalParams);
	}
})
