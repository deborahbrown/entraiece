/**

    Deflection is a view responsible for deflecting the chat by showing articles related to the
        question entered in login form. It also calls escalation APIs to register the events.

**/
App.AttachmentView = Backbone.View.extend({

    //className : 'eg-chat-deflection',

    template : _.template($('#tpl-attachment').html()),

    deflectionContent : '',

    clickedArticle : null,
    
    events : {
        
    },
    dragCounter : 0,
    filesArray : {},
    browsedFileArray:[],
    droppedFileArray:[],
    filesToSkip : [],
    CMD_CUST_REQUEST_ATTACHMENT : 'custrequestattachment',

      
        
    cleanUpFileList: function() {
            $('#fileList').empty();
            App.attachmentView.browsedFileArray = [];
            
            App.attachmentView.removeAndAddFileUploadField();
    },
    closeDropWarningModal : function(){
        
         $('#dropFileList').empty();
         App.attachmentView.droppedFileArray = [];
         App.attachmentView.filesToSkip = [];
        $("#openDragWarningModal").css({ 'opacity': 0,
                'pointer-events': 'none',
                'display' : 'none' });
    },
    removeAndAddFileUploadField : function(){
        $("#filesToUpload").remove();
        $("#fileUploadDiv").append('<input type="file" id="filesToUpload" name="files[]" multiple class="upload"/>');            
        $('#filesToUpload').on("change",this.handleFileSelect);
    },
    initialize : function() {
    },

    render : function() {
        var width = $(window).width();
        $('#eg-chat-header a.closechat').attr('title',L10N_WINDOW_CLOSE_BUTTON);
        $('#eg-chat-header').width('100%');
        $('#eg-chat-content').css('width','100%');
        this.$el.html(this.template());
        $('#eg-chat-content').append(this.$el);
        $.event.props.push('dataTransfer');
        $('#droparea').on("dragover", this.dragHandler);
        $('#droparea').on("dragenter", this.dragEnterHandler);
        $('#droparea').on("dragleave", this.dragLeaveHandler);
        $('#droparea').on("drop", this.filesDroped); 
        $('#filesToUpload').on("change",this.handleFileSelect);
    },
    dragHandler : function(event){   
        event.stopPropagation();
        event.preventDefault();
        if(!App.offRecord && App.isChatAttachmentEnabled){
            event.dataTransfer.dropEffect = 'move';
            $("#droparea").attr("class", "dragEnterEffect");
        }else{
            event.dataTransfer.dropEffect = 'none';
            $("#dragAndDropNotAllowed").css({ 'opacity': 1,
                'pointer-events': 'auto',
                'display' : 'block'});
            $("#droparea").attr("class", "drop-not-allowed"); 
        }       
        return false;
    },
    dragEnterHandler : function(event){
        event.stopPropagation();
        event.preventDefault();
        App.attachmentView.dragCounter ++; 
        return false;
    },
    dragLeaveHandler : function(event){
        if(event){
            event.stopPropagation();
            event.preventDefault();
        }
        App.attachmentView.dragCounter --;
        if(App.attachmentView.dragCounter == 0){
            $("#droparea").attr("class", "");
            $("#dragAndDropNotAllowed").css({ 'opacity': 0,
                   'pointer-events': 'none',
                   'display' : 'none'});   
        }   
        return false;
    },
    setDropNotAllowedMessage : function(message){
        $("#dropNotAllowedText").html(message);
    },
    filesDroped : function(event){
        App.attachmentView.dragLeaveHandler();
        if(!App.offRecord && App.isChatAttachmentEnabled){
            App.attachmentView.filesToSkip = [];
            App.attachmentView.droppedFileArray = [];
            $("#droparea").attr("class", "");
            event.stopPropagation();
            event.preventDefault();
            var files = event.dataTransfer.files;
            if(App.isChatAttachmentEnabled && App.attachmentView.dropedFilesEligible(files)){
                
                App.attachmentView.sendAttachmentNotification(files, false);
            }
        }
    },
    dropedFilesEligible : function(files){
        
        var listFileNames = App.attachmentView.validateAttachmentFiles(files, true);
        if(App.attachmentView.filesToSkip.length == 0){
            return true;
        }else{
            listFileNames ? $("#dropFileList").html(listFileNames.join('')) : '';
            $("#openDragWarningModal").css({ 'opacity': 1,
                'pointer-events': 'auto',
                'display' : 'block' });
            if(files.length <= 10){
                App.attachmentView.droppedFileArray = files;
            }
            return false;
        } 

    },
    sendAttachmentNotification : function(files, isBrowse){
        var timeStamp = (new Date()).getTime();
		var errorOccured = false;
        for (var i = 0; i < files.length; i++) {    
            (
				function(i) {
					if( App.attachmentView.filesToSkip.indexOf(i) === -1){
						var file = files[i];  
						var uniqueFileId = timeStamp+""+i;
						file.newName = uniqueFileId+"_"+file.name;
						App.attachmentView.filesArray[uniqueFileId] = file;                
						var cssClass = 'chatInput headline';
						$.ajax({
							type: 'POST',
							url: App.connection.BOSH_SERVICE+'/sendCustAttachmentNotificaiton',
							data : {'sid' : App.connection.sid,'customerIdentity' : App.session.get('name'), 'attachmentName' : file.name , 'attachmentInternalName' : file.newName , 'attachmentId': uniqueFileId, 'attachmentSize': file.size},
							dataType: 'text',
							file: file,
							success: function (rettext) {
								if(rettext == '1') {
									var filename = file.newName; 
									var filePrefix = filename.substring(0, filename.indexOf("."));
									var size = App.attachmentView.getAttachmentSizeUnit(file.size);
									var attachmentInfo = "<div id=attachmentImage_"+uniqueFileId+" class='attachemntImageDiv'><a href='#' onclick='App.attachmentView.onDownloadClick(this); return false;' id=_"+uniqueFileId+" download><img id="+"image_"+uniqueFileId+" src='chat/img/icon_upload_32.gif' alt='chat_cust_attachment_icon'></a><progress class='progress-bar' id=p"+uniqueFileId+" max='100' value='0'></progress></div><div class='chat-attachment-text'>"+file.name +" ("+size +")<div style='display:block' class='saving' id="+uniqueFileId+"_status_div>"+ L10N_WAITING_MESSAGE_STRING +L10N_ATTACHMENT_WAITING_SPAN_STRING+"</div></div>";
									App.messenger.addToTranscript(App.utils.getFormattedMessage(attachment_to_chat_agent, [App.session.get('name'),file.name]),'chatInput'+' '+'headline');
									App.messenger.submitMessageString(attachmentInfo,'customer',App.session.get('name'), true);
								} else {
									if(!errorOccured)
                                    {
									    alert(L10N_QUEUE_ATTACHMENT_DISABLED);
										errorOccured = true;
									}
								}
							}
						});
					}
			})(i);
        }
        if(isBrowse){
            $('#fileList').empty();
            App.attachmentView.filesToSkip = [];
            App.attachmentView.browsedFileArray = [];
                    
        }else{
            App.attachmentView.closeDropWarningModal();
        }
    },
    enableDisableAttachmentStatus : function(divId, status){
        $("#"+divId).html(status);
    },
    handleFileSelect : function(event){
        
        App.attachmentView.filesToSkip = [];
        App.attachmentView.browsedFileArray = [];
        var files = event.target.files;
        var listFileNames = App.attachmentView.validateAttachmentFiles(files, false);
        App.attachmentView.removeAndAddFileUploadField();
        if(listFileNames === false){
            return false;
        }else{
            $("#fileList").html(listFileNames.join(''));
        }
    },
    validateAttachmentFiles : function(files, isDropped){
        var listFileNames = [];
        var allowAllType;
        if (App.listValue) {
            allowAllType = App.listValue;
        } else {
            allowAllType = true;
        }
        if(files.length > 10){         
            var fileListId =  isDropped ? "#dropFileList" :"#fileList";
            $(fileListId).html(App.attachmentView.getListHtmlForFileRestricted('maxNumFilesExceeded', "", 0, ''));
            return false;
        }
        for (var i = 0; i < files.length; i++) {
            listHtml = "";
            var currentFile = files[i];
            if(!isDropped)
                App.attachmentView.browsedFileArray.push(currentFile);
            var fileExtension = '.' + currentFile.name.split('.').pop().toLowerCase();
            var fileSeprator = files.length > 1 && i != files.length-1 ? '<hr>' : '';

        if(App.maxChatAttachmentSize < (currentFile.size / (1024 * 1024))) {
            listHtml = App.attachmentView.getListHtmlForFileRestricted('sizeExceeded', currentFile.name, i, fileSeprator);
        } else if (allowAllType === true){
            listHtml = App.attachmentView.getListHtmlForFile(currentFile, i, fileSeprator);
        } else if(App.listType && App.listValue.split(',').indexOf(fileExtension) != -1) {
            listHtml = App.attachmentView.getListHtmlForFileRestricted('fileBlacklisted', currentFile.name, i, fileSeprator);
        } else if(!App.listType && App.listValue.split(',').indexOf(fileExtension) == -1) {
            listHtml = App.attachmentView.getListHtmlForFileRestricted('fileNotWhitelisted', currentFile.name, i, fileSeprator);
        } else {
            listHtml = App.attachmentView.getListHtmlForFile(currentFile, i, fileSeprator);
        }
            //listHtml = App.attachmentView.getListHtmlForFile(currentFile, i);
            
                listFileNames.push(listHtml);
            
        }
        return listFileNames;
    },
    getListHtmlForFileRestricted : function (reason, fileName, fileIndex,lineSeprator) {
         App.attachmentView.filesToSkip.push(fileIndex);
        var failureReason = '';
        switch(reason) {
            case 'sizeExceeded':
                failureReason = App.utils.getFormattedMessage(L10N_ATTACHMENT_SIZE_FAIL_MESSAGE, [fileName,App.maxChatAttachmentSize]);
                break;
            case 'fileBlacklisted':
                failureReason = App.utils.getFormattedMessage(L10N_FILE_BLACKLIST_MESSAGE, [fileName]);
                break;
            case 'fileNotWhitelisted':
                failureReason = App.utils.getFormattedMessage(L10N_FILE_NOT_IN_WHITE_LIST, [fileName]);
                break;  
            case 'maxNumFilesExceeded':
                failureReason = L10N_MAXIMUM_ATTACHMENT_NUMBER_EXCEEDED; 
                break;              
        }
        return '<li id="li' + fileIndex + '">' +
             
                '<div class="failure-message" tabindex="0">' + failureReason +  
                '<a href="#" class="remove-link" onclick="javascript:App.attachmentView.skipFileUploadForIndex(' + fileIndex + ');return false;" title ="'+ L10N_DELETE_CHAT +'">' +
                    '<img border="0" src="chat/img/icon_reject_16.gif">' +
                '</a>'+lineSeprator+'</div>' +
             
        '</li>'
    },
    onSendAttachment : function (isDropped) {
        //var filesAdded =$('#filesToUpload').prop('files');
        var files = isDropped ? App.attachmentView.droppedFileArray : App.attachmentView.browsedFileArray;
        App.attachmentView.sendAttachmentNotification(files, !isDropped);
    },

    getListHtmlForFile : function (file, fileIndex, lineSeprator) {
    return '<li id="li' + fileIndex + '">' +
                                '<div class = "file-info" tabindex="0">' + file.name +' ('+App.attachmentView.getAttachmentSizeUnit(file.size)+')' + 
                                '<a href="#" class="remove-link" onclick="javascript:App.attachmentView.skipFileUploadForIndex(' + fileIndex + ');return false;" title ="'+ L10N_DELETE_CHAT +'">' +
                                    '<img border="0" src="chat/img/icon_reject_16.gif">' +
                                '</a>'+lineSeprator+'</div>' +
                        '</li>'
    },

    uploadFiles : function(arr){
       var file = App.attachmentView.filesArray[arr.filekey];
       var uniqueFileId = arr.filekey;
       if(file !== -1){
            var chatAttachmentUrl=  App.connection.BOSH_SERVICE+"/uploadAttachments?language="+App.connection.getUrlParameter('languageCode').toLowerCase()+"&country="+App.connection.getUrlParameter('countryCode').toUpperCase();
            
                var filePrefix = file.newName.substring(0, file.newName.indexOf("."));
                App.attachmentView.enableDisableAttachmentStatus(uniqueFileId+"_status_div", L10N_SENDING_MESSAGE_STRING+ L10N_ATTACHMENT_WAITING_SPAN_STRING);
                var formData = new FormData();
                formData.append('sid',App.connection.sid);
                formData.append('agentName',arr.agentName);
                formData.append('fileSize',file.size);
                formData.append('fileId', uniqueFileId);                
                formData.append(file.newName,file);
                xhr = new XMLHttpRequest();
            
            // Update progress bar
            var pBar = document.getElementById("p"+uniqueFileId);
            xhr.upload.addEventListener("progress", function (evt) {
                if (evt.lengthComputable) {
                    pBar.value = Math.floor((evt.loaded / evt.total) * 100);
                }
                else {
                    // No data to calculate on
                }
            }, false);
            
            // File uploaded
            xhr.addEventListener("load", function (evt) {
                if(evt.target.responseText != parseInt(evt.target.responseText)){
                    $("#p"+uniqueFileId).hide();
                    $("#image_"+uniqueFileId).attr("src","chat/img/icon_reject.gif");
                    App.attachmentView.enableDisableAttachmentStatus(uniqueFileId+"_status_div", "<div class='rejected'>"+L10N_FAILED_MESSAGE_STRING+"("+evt.target.responseText+") </div>");               
                }
            }, false);

            xhr.open("post", chatAttachmentUrl, true);
            
            // Set appropriate headers
           // xhr.setRequestHeader("Content-Type", "multipart/form-data");

            // Send the file (doh)
            xhr.send(formData);
            App.attachmentView.filesArray[arr.filekey] = -1; 
        }        
        App.attachmentView.filesArray[arr.filekey] == -1;
    },
    setImageAttachmentIcon: function(fileId, attachmentId, attachmentType)
    {
        // If the attachment is image, set its thumbnail otherwise set file attachment icon
        $("#image_"+fileId).attr("src","chat/img/icon_attachment_32.gif");
        if(attachmentType != null && (new RegExp('^image\/', 'i').test(attachmentType)))
        {
            $.ajax({
                type: 'POST',
                url: App.connection.BOSH_SERVICE+'/getImageThumbnail',
                data : {'sid' : App.connection.sid, 'fileId' : attachmentId},
                dataType: 'text',
                success: function (responseText) {
                    if(responseText)
                    {
                        $("#image_"+fileId).on('load',function(){
                           App.messenger.updateScrollOnImageAccept();
                        });
                        $("#image_"+fileId).attr("src", responseText);
                        $("#image_"+fileId).addClass("imageThumbnail");
                        $("#attachmentImage_"+fileId).addClass("attachmentImage");
                        
                    }
                }
            });
        }
    },
    makeClickableLink : function(attachmentObject){
       var fileName = attachmentObject.attachmentInternalName;
       var filePrefix = fileName.substring(0, fileName.indexOf("_"));
       $("#_"+filePrefix).attr("id",attachmentObject.attachmentId);
       $("#p"+filePrefix).hide();
       
       App.attachmentView.setImageAttachmentIcon(filePrefix, attachmentObject.attachmentId, attachmentObject.attachmentType);
       App.attachmentView.enableDisableAttachmentStatus(filePrefix+"_status_div", "<div class='sent'>"+L10N_SENT_MESSAGE_STRING+"</div>");
    },
    addAttachmentIcon : function(attachmentObject){  
        var fileName = attachmentObject.attachmentName;
        var fileId = attachmentObject.attachmentId;
        App.attachmentView.enableDisableAttachmentStatus(fileId+"_status_div",L10N_RECIEVED_MESSAGE_STRING);
        
        App.attachmentView.setImageAttachmentIcon(fileId, fileId, attachmentObject.attachmentType);
        $("#_"+fileId).attr("id",fileId);
    },
    onDownloadClick : function(scope , args){
        if(parseInt(scope.id) == scope.id){
             $.download(App.connection.BOSH_SERVICE+"/getattachment/"+scope.id);
        }
    },
    onAgentRejectCustAttachment : function(arr){
        var file = App.attachmentView.filesArray[arr.filekey];
        var filePrefix = file.newName.substring(0, file.newName.indexOf("_"));
        $("#p"+filePrefix).hide();
        $("#image_"+filePrefix).attr("src","chat/img/icon_reject.gif");
        App.attachmentView.enableDisableAttachmentStatus(filePrefix+"_status_div", "<div class='rejected'>"+L10N_REJECTED_MESSAGE_STRING+"</div>");               

    },
    getAttachmentSizeUnit : function(bytes){

       if (bytes>=1048576)    {bytes=Math.floor((bytes*10 /1048576))/10+' MB';}
      else if (bytes>=1024)       {bytes=Math.floor((bytes*10/1024))/10+' KB';}
      else if (bytes>1)           {bytes=bytes+' bytes';}
      else if (bytes==1)          {bytes=bytes+' byte';}
      else                        {bytes='0 byte';}
      return bytes;
    },
    skipFileUploadForIndex : function(index){
        $('#li'+index).remove();
        App.attachmentView.filesToSkip.push(index);
    },
    onAgentInviteAttachment : function(attachmentObject, agentName){
        var fileName = attachmentObject.attachmentName;
        var fileId = attachmentObject.attachmentId;
        var size = attachmentObject.attachmentSize;
      
        var attachmentInfo = "<div id=attachmentImage_"+fileId+" class='attachemntImageDiv'><a href='#' onclick='App.attachmentView.onDownloadClick(this); return false;' id=_"+fileId+" download><img id="+"image_"+fileId+" src='chat/img/icon_upload_32.gif' alt='chat_cust_attachment_icon'></a></div><div class='chat-attachment-text'>"+fileName +" ("+App.attachmentView.getAttachmentSizeUnit(size)+")</br><div class='pending' id="+fileId+"_status_div>"+L10N_PENDING_MESSAGE_STRING
        +"&nbsp;&nbsp;&nbsp;&nbsp;<a href='#' id=accept_"+attachmentObject.attachmentId+" onclick='App.attachmentView.onClickAcceptChatAttachment("+fileId+",\""+fileName+"\"); return false;' download><img src='chat/img/accept.png' alt='"+ L10N_ACCEPT_CHAT +"'></a>   "
        +"&nbsp;<a href='#' id=reject_"+attachmentObject.attachmentId+" onclick='App.attachmentView.onClickRejectChatAttachment("+fileId+",\""+fileName+"\"); return false;' download><img src='chat/img/cross.png' alt='"+ L10N_REJECT_CHAT +"'></a>"
        +"</div></div>";

        App.messenger.submitMessageString(attachmentInfo,'agent', agentName,true);
        //$("#"+fileName.substring(0, fileName.indexOf("."))).attr("id",attachmentObject.attachmentId);
    },
    onClickAcceptChatAttachment : function (fileId, fileName){
        $("#accept_"+fileId).remove();
        $("#reject_"+fileId).remove();
        App.connection.sendAcceptChatAttachment(fileId, fileName);
    },
    onClickRejectChatAttachment : function (fileId, fileName){
        $("#accept_"+fileId).remove();
        $("#reject_"+fileId).remove();
        App.attachmentView.enableDisableAttachmentStatus(fileId+"_status_div", "<div class='rejected'>"+L10N_REJECTED_MESSAGE_STRING+"</div>");   
         $("#image_"+fileId).attr("src","chat/img/icon_reject.gif");
        App.connection.sendRejectChatAttachment(fileId, fileName);
        App.messenger.addToTranscript(App.utils.getFormattedMessage(rejected_chat_attachment, [App.session.get('name'),fileName]),'chatInput'+' '+'headline');      
    }
});
