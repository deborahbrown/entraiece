var eGainLiveConfig = {

	maxMessageSize : 800,

    showSidebar : false, //Show sidebar containing the iframe.

	showSmileyTool : false,

	sessionStastics	: 1, // This flag can have three values: 0 to show nothing, 1 to show wait time, 2 to show queue depth.. default value is 1.

	videoReofferTimeout : 60 * 1000, // milliseconds

    showAudioButton : true, //true to show audio button on the top.

    showVideoButton : true, //true to show video chat button on the top.

    minWidthForShowingSidebar : 796, //we only show sidebar if the window width is more than this. If the window resizes to less than this, then we have to force hide the sidebar,and show only the chat stream instead for better user experience.

	chatWindowWidthWithWidget : 830, //width of chat window with widget

	customerNoTypingTimeout : 5 * 1000, // milliseconds

	chatWindowWidth : 450, //required

	chatLoginWindowHeight : 650, //height of login page

	chatSurveyWindowHeight : 615, //height of survey page

	chatThanksWindowHeight : 450, //height of thankyou page

	turnOffChatSurveyPage : false, //Set to true if the survey page should not be displayed. Set to false to show the survey

	chatWindowHeight: 850, //height of chat window

	cobrowseWindowWidth : -1, /* -2: leave it to the browser, -1: take up available width, +ve values: use the value */

	cobrowseWindowHeight : -1, /* -2: leave it to the browser, -1: take up available height, +ve values: use the value */

	autoLogin : 0, // This flag can have two values - 0 or 1: if 1, then autologin is set to true for these templates
	
	/*Sample regexes: 
	* ".*?\.?egain\.com" matches all subdomains of egain.com i.e urls of the form egain.com, *.egain.com, *.egain.in
	* ".*?\.?(?:egain|chat)\.com" matches all subdomains of multiple domains - egain.com and chat.com.
	*/
	domain: '.*', //Regex to test the domain from which messages are posted.

	useTextEditor : 0, // This flag can have two values - 0 or 1: if 1, then text editor will be used instead of CKEditor.
	
	chatSoundURL : "sound/notify.wav",
	
	debug : 0, // This flag can have two values - 0 or 1: if 1, then $debug flag will be passed with Web Service requests and all requests and responses will be logged on the server.

	enableChatDeflection : false, //true is chat deflection should be enabled on start of chat
        
    chatDeflectionPortalId : "", //Self Service portal ID which should be used for searching articles for chat deflection

    maxDeflectionArticles : 5, // maximum number of the articles to be shown in the deflection window

	chatPauseInSec : 30, // chat pause in seconds, it should be less than Max pause seconds configured on server

	// These are the loginParameters that are displayed in the customer login page.
	// fieldType can be of three types: 1 - <input> i.e. text field; 2 - <textarea> i.e. text area; 3 - <select> i.e. single-select dropdown; 4 - <select> i.e. multi-select dropdown;
	

	loginParameters : [],
	
	// Changing this property to yes would show callback page instead of off hour page in case of agent unavailability
	useCallBackPageDuringOffHours : 'no',
	        
	// Callback window height can be configured by configuring this parameter 
        callBackWindowHeight : 530,
	
	
	// Callback options can be configured by configuring the settings below. 
	// You can add new options,remove options or edit the existing options
	callBackParams :[ {
	  
		  text :'L10N_CALLBACK_TEXT1',
		  image:'chat/img/phone.png',
		  hoverImage:'chat/img/phone_hover.png',
		  url:'http://customer.egain.net/system/templates/clicktocall/sunburst/#entrypoint/1000?languageCode=en&countryCode=US'
	},{
		  
		  text :'L10N_CALLBACK_TEXT2',
		  image:'chat/img/mail.png',
		  hoverImage:'chat/img/mail_hover.png',
		  url:'mailto:support@company.com'
	},{
		  
		  text :'L10N_CALLBACK_TEXT3',
		  image:'chat/img/faq.png',
		  hoverImage:'chat/img/faq_hover.png',
		  url:'http://customer.egain.net/system/templates/selfservice/kiwi/index.html#/help/customer/locale/en-US/portal/400000000001000'
	}  ],
	
	altEngagementOptions : {
		  clockImageSrc : "chat/img/icon_clock.png",
          altEngmtMessage : 'L10N_ALT_ENGMT_MESSAGE',	
	altEngmtParams :[ {
	  
		  text :'L10N_ALT_ENGMT_TEXT1',
		  image:'chat/img/icon_phone_call.png',
		  hoverImage:'chat/img/phone_hover.png',
		  bgcolor:'#22A0DB',
		  url:'http://customer.egain.net/system/templates/clicktocall/sunburst/#entrypoint/1000?languageCode=en&countryCode=US'
	},{
		  
		  text :'L10N_ALT_ENGMT_TEXT2',
		  image:'chat/img/icon_email.png',
		  hoverImage:'chat/img/mail_hover.png',
		  bgcolor:'#5CC8ES',
		  url:'mailto:support@company.com'
	},{
		  
		  text :'L10N_ALT_ENGMT_TEXT3',
		  image:'chat/img/icon_faq.png',
		  hoverImage:'chat/img/faq_hover.png',
		  bgcolor:'#23C4BC',		  
		  url:'http://customer.egain.net/system/templates/selfservice/kiwi/index.html#/help/customer/locale/en-US/portal/400000000001000'
	}
	]
  
 }
};