/**

    Audio basically plays the different sounds.

**/
App.Audio = Backbone.Model.extend( {

    initialize: function () {
        this.alertSoundThreshold = 2000;
        this.chatSoundURL = eGainLiveConfig[ 'chatSoundURL' ];
        this.lastSoundAlert = 0;
        if ( this.chatSoundURL ) {
            try {
                this.egsound = document.getElementById( "iesound" );
                this.tagType = "bgsound";
                if ( !this.egsound && App.utils.isIE() ) {
                    $( 'body' ).append(
                        "<bgsound id='iesound' src='' hidden='true' autostart='false' type='audio/wav' loop='false' style=''/>"
                    );
                    this.egsound = document.getElementById( "iesound" );
                } else if ( !App.utils.isIE() ) {
                    this.egsound = document.getElementById( "html5sound" );
                    this.tagType = "audio";
                    if ( this.egsound.canPlayType && this.egsound.canPlayType( "audio/wav" ) ) {
                        this.egsound.src = this.chatSoundURL;
                    } else {
                        this.egsound = null;
                    }
                }
            } catch ( err ) {
                this.egsound = null;
            }
        }
    },

    playChatMessageSound: function () {
        var currTime;
        if ( this.egsound && !App.editor.isfocus && this.chatSoundURL != null && this.chatSoundURL != "" && ( (
            currTime = new Date().getTime() ) - this.lastSoundAlert ) > this.alertSoundThreshold ) {
            this.playSound( this.chatSoundURL );
            this.lastSoundAlert = currTime;
        }
    },

    playSound: function ( url ) {
        try {
            if ( this.tagType == "bgsound" ) {
                this.egsound.src = url;
            } else if ( this.egsound.play )
                this.egsound.play();

        } catch ( err ) {
            this.egsound = null;
        }

    }
} );