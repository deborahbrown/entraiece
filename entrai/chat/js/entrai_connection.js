var entraiapp;
function wait(ms){
   var start = new Date().getTime();
   var end = start;
   while(end < start + ms) {
     end = new Date().getTime();
  }
}
var eGainChatUrl;
transfertoECE = function() {
    var egainChat = {};
    egainChat.egainChatParameters = {};
    egainChat.eglvchathandle = null;
    egainChat.liveServerURL = window.location.origin + '/system/';
    
    /*To be called by client website. All the parameters specified in eGainLiveConfig must be set here.*/
    egainChat.storeChatParameters = function(attributeName, attributeValue) {
                   egainChat.egainChatParameters[attributeName] = attributeValue;
   }

}
//  var eGainChatUrl = window.location.origin + '/system/templates/chat/entraiece/chat.html?subActivity=Chat&entryPointId=1002&templateName=entraiece&languageCode=en&countryCode=US&ver=v11';

var egainChat = {};
egainChat.openHelp = function() {
        var domainRegex = /^((?:https?:\/\/)?(?:www\.)?([^\/]+))/i;
        try{
            if( egainChat.eglvchathandle != null && egainChat.eglvchathandle.closed == false ){
                egainChat.eglvchathandle.focus();
                return;
            }
        }
        catch(err){}
        var refererName = "";
        refererName = encodeURIComponent(refererName);
        var refererurl = encodeURIComponent(document.location.href);
        var hashIndex = refererurl.lastIndexOf('#');
        if(hashIndex != -1){
            refererurl = refererurl.substring(0,hashIndex);
        }
        var eglvcaseid = (/eglvcaseid=[0-9]*/gi).exec(window.location.search);
        var vhtIds = '';

        var EG_CALL_Q =  [];
        EG_CALL_Q.push( ["enableTracker", true] );
        
      
        var domain = domainRegex.exec(eGainChatUrl)[0];
        
        if( (eGainChatUrl + refererurl).length <= 2000)
            eGainChatUrl += '&referer='+refererurl;
        
       // window.location.href = eGainChatUrl;
	   window.location.replace(eGainChatUrl);
        

    }
	
var newhtml;
var answerhtml;
var myWindow;

// To customize: QueryKB takes as input, the message sent from the server
// It parses the input, takes some action
// It should send a reply back to the server
// Either 'NOMATCH:<some optional message>' or 'MATCH:<tag>' where <tag> is the value grammar expecting

function QueryKB(KBstring) {
  var publicationId = KBstring.replace(/USEKB:/, '');
  publicationId = publicationId.replace(/:.*/, "");
  publicationId =publicationId.replace(/\s/g, "");
  var userquery = KBstring.replace(/.*:/, '');
  userquery = userquery.replace(/\n/, '');
  
  var query = "{ search(query: \"" +  userquery + "\") { results { id name content }} }";

  mesg = {
  "query": query,
  "variables": {
      "publicationId": publicationId,
      "api_token": "c037467a1a7d350c589fcc078edfd1"
  }};


  
  $.ajax( {
       type: "POST",
       url: 'https://api.polly.help/graphql',
       data: JSON.stringify(mesg),
       contentType: "application/json; charset=utf-8",
       dataType: "json",
	   error: function(status) {
		   App.connection.send('NOMATCH:' + JSON.stringify(status));
	   },
       success: function(data, status){
        results = data.data.search.results;
           if (results.length == 0 ) {
			App.connection.send('NOMATCH:no results from knowledge base'); 
            return;
        }
        newhtml="<div><B>Here's your requested info</B></div><br><br>"; answerhtml='';
        for (index = 0; index < results.length; ++index) {
            newhtml = newhtml + '<br><br><a href="#answer_' + index + '">' + results[index].name + '</a>';
            answerhtml = answerhtml + '<br><br><p id="answer_' + index + '">' + results[index].content + '</p>';
        }
		
		myWindow = window.open("", "_blank", "width=500,height=700");
		myWindow.document.body.innerHTML = newhtml + '<br><br>' + answerhtml ;
		var retmsg = 'MATCH:CoverageChiropractor'; 
		App.connection.send(retmsg);
	
       }
    });


}


// avmode values
var AGENT_VIDEO_MODE = 1;
var TWO_WAY_AUDIO_VIDEO_MODE = 2;
var AGENT_AUDIO_MODE = 3;
var TWO_WAY_AUDIO_MODE = 4;
var TWO_WAY_AUDIO_AGENT_VIDEO_MODE = 5;
var TWO_WAY_AUDIO_CUSTOMER_VIDEO_MODE = 6;

/**

    Connection is a wrapper object around Strophe's connection instance.

**/
App.Connection = Backbone.Model.extend({

	acceptRejectMsgSent: false,
	agentInvitationTime: 0,
	custInvitationTime: 0,

    initialize : function() {
    	this.chatStartedPrefix="";
		this.chatStartedSuffix="";
		this.chatEndedPrefix="";
		this.chatEndedSuffix="";
    	this.lastResponseMessageTime=-1;
		this.lastRequestMessageTime=-1;
		this.chatExited=false;
		this.systemTimeOutMessage="";

		//added to fetch msg values from properties file for chat instrumentation
		if(messagingProperty){
			var mesagingPropertyVariables=messagingProperty.split('\n');
			var foundStartChatString=false;
			var foundEndChatString=false;
			var foundsystemTimeOutMsg=false;
			for(i=0;i<mesagingPropertyVariables.length;i++){

				if(mesagingPropertyVariables[i].indexOf('agent_join_msg')===0){

					var keyValuePair=mesagingPropertyVariables[i].split('=');
					var chatStartedString=$.trim(keyValuePair[1]);
					var temp=chatStartedString.split('{0}');
					this.chatStartedPrefix=temp[0];
					this.chatStartedSuffix=temp[1];
					foundStartChatString=true;
				}

				else if(mesagingPropertyVariables[i].indexOf('agent_close_session')===0){

					var keyValuePair=mesagingPropertyVariables[i].split('=');
					var chatEndedString=$.trim(keyValuePair[1]);
					var temp=chatEndedString.split('{0}');
					this.chatEndedPrefix=temp[0];
					this.chatEndedSuffix=temp[1];
					foundEndChatString=true;

				}
				else if(mesagingPropertyVariables[i].indexOf('system_close_session')===0){
					var keyValuePair=mesagingPropertyVariables[i].split('=');
					this.systemTimeOutMessage=$.trim(keyValuePair[1]);
					foundsystemTimeOutMsg=true;
				}
				else if(foundStartChatString===true && foundEndChatString===true && foundsystemTimeOutMsg===true)
					break;
			}
		}


		//added for evaluating time difference for chat events
    	this.agentIdentityForNotification = null;
        //This will be strophe's connection instance.
        this._connection = null;
		this.connected = false;
		// BOSH_SERVICE must be same domain and port that served the chat client
		var base = document.URL;
		var contextRoot = base.substr(base.indexOf("//")+2, base.length);
		contextRoot = contextRoot.substr(contextRoot.indexOf("/") + 1);
		contextRoot = "/" + contextRoot.substr(0, contextRoot.indexOf("/"));
		this.BOSH_SERVICE = contextRoot+ '/egain/chat/entrypoint';
		this.QUEUE_STATUS_URL = this.BOSH_SERVICE + '/sessionStatistics'; // BOSH session id is to be appended
		this.SURVEY_URL = this.BOSH_SERVICE + '/survey';
		this.AGENT_AVAILABILITY_URL = this.BOSH_SERVICE + '/agentAvailability';
		this.CHAT_ELIGIBILITY_URL = this.BOSH_SERVICE + '/checkEligibility';
		this.NOTIFY_URL = './notify.html';
		this.ATTRIBUTES_INFO_URL = this.BOSH_SERVICE + '/attributesInfo';
		this.GET_MEDIA_SERVER_URL = this.BOSH_SERVICE + '/mediaServer';
		this.INITIALIZATION_URL = this.BOSH_SERVICE + '/initialize';

		this.entryPointId = '1000'; // default if no parameter
		this.jid = 'egain@egain.com'; // default if no parameter
		this.languageCode = 'en'; // default if no parameter
		this.countryCode = 'US'; // default if no parameter
		if (this.getUrlParameter('entryPointId')) {
				this.entryPointId = this.getUrlParameter('entryPointId');
		}
		if (this.getUrlParameter('jid')) {
			this.jid = this.getUrlParameter('jid');
		}
		if (this.getUrlParameter('languageCode')) {
			this.languageCode = this.getUrlParameter('languageCode').toLowerCase();
		}
		if (this.getUrlParameter('countryCode')) {
			this.countryCode = this.getUrlParameter('countryCode').toUpperCase();
		}

		if (this.getUrlParameter('video'))
			this.videochat = (parseInt(this.getUrlParameter('video')) == 1) && (typeof eGainMobileChat != 'undefined' || !App.utils.isVisitorMobile()); //Added eGainMobileChat check for mobile sdk

//		alert("connection.js initialize() avmode parameter: " + this.getUrlParameter('avmode'));
		if (this.getUrlParameter('avmode'))
			this.avmode = parseInt(getUrlParameter('avmode')); //read the video chat mode
		else
			this.avmode = 2;
		
	
	},

    //Connect to the BOSH server.
    connect : function(email) {

			var connectionUrl = this.BOSH_SERVICE + window.location.search;
			var timezoneoffset=new Date().getTimezoneOffset()*-1;
			connectionUrl = connectionUrl + '&custtimeoffset='+timezoneoffset;

			if (this.videochat)
				connectionUrl = connectionUrl + '&videoChat=1';


	       var host = this.getUrlParameter('entraihost');
		   entraiapp = this.getUrlParameter('entraiapp');
           App.session.set('name', JSON.parse(window.localStorage.getItem('formValues'))['name']);

           App.websocket = new WebSocket( host );
           App.websocket.onopen = function() {

	       mesg = {
	           rqst: "INTERPRETER:CONNECT",
			   config: entraiapp,
		       username: window.localStorage.getItem('formValues').full_name,
		       userid: "",
		       sessionid: "",
		       msg: ""
            };
           App.websocket.send( JSON.stringify(mesg));
           App.entraiapp = entraiapp;		   

           }
		   
		   App.websocket.onmessage = function( msg ) {
         
            var json = JSON.parse(msg.data);

            if  (json.rqst == "BROWSER:DISCONNECT"  ) {     
            App.websocket.close();        
           }

           else if  (json.rqst == "BROWSER:SESSIONNUM"  ) {

               App.sessionid = json.sessionid;

           }
           else if (json.rqst == "BROWSER:PROMPT" ){
	           if (json.msg.indexOf("USEKB:") >= 0) QueryKB(json.msg);      
               else   App.messenger.submitMessageString(json.msg,'agent', L10N_AGENT, false, 'chatInput' + ' ' + 'chat');
		   }
		   else if (json.rqst == "TRANSFERTOAGENT" ){
			    App.websocket.close();
			   	App.entryPointId = json.entryPointId;
			    App.countryCode = json.countryCode;
			    App.languageCode = json.languageCode;
			    var messages = App.messenger.getAllMessages();
				var transcription = JSON.stringify( { messages: messages} );
	            window.localStorage.setItem("entraiConvers", transcription);
				eGainChatUrl = window.location.origin + '/system/templates/chat/entrai/chatece.html?subActivity=Chat&entryPointId=' + App.entryPointId + '&templateName=entraiece&languageCode=' +
    App.languageCode + '&countryCode=' + App.countryCode + '&ver=v11';
	// eGainChatUrl = window.location.origin + '/system/templates/chat/test/chat.html?subActivity=Chat&entryPointId=' + App.entryPointId + '&templateName=test&languageCode=' +
 //   App.languageCode + '&countryCode=' + App.countryCode + '&ver=v11';
			    
                
                 wait(3000);
	            transfertoECE();
                egainChat.openHelp();
	

		   }
      }

            this._connection = App.websocket;
    
			// add egain specific attributes
			var from = 'anonymous@egain.com';
			if (email) from = email;

			this._connection.from = (from).replace(/'/g, '&apos;');

			this._connection.entryPointId = this.entryPointId;
			this._connection.xEgainEscalationHeader = this.xEgainSession;
			this._connection.lang = this.languageCode + '-' + this.countryCode;

			// Hook to set and override parameters for customization
			setConnectionParametersHook();
			//this._connection.connect(this.jid, '', this.onConnect);
            this.onConnect("connected", 1);

    },
	
	send: function (msg) {

			mesg = {
	           rqst: "INTERPRETER:USERINPUT",
		       config: entraiapp,
		       username: window.localStorage.getItem('formValues').full_name,
		       userid: "",
		       sessionid: App.sessionid,
		       msg: msg
            };

           App.websocket.send( JSON.stringify(mesg));

	}, 

	onConnect : function(status, condition) {

		if(status == "connected")
		{
			window.onbeforeunload = function() {
				return;
				if(App.mailClicked)
					return;
				else
					return L10N_BROWSER_CLOSE_MESSAGE;
			}
			if (App.session.get('subject')) {
				if(!eGainLiveConfig.enableChatDeflection || typeof eGainLiveConfig.chatDeflectionPortalId == "undefined" || eGainLiveConfig.chatDeflectionPortalId == "") {
					App.session.startWithInitialMessage(App.utils.masker.maskData(App.session.get('subject')));
				}
			}
			App.connection.sid = App.connection._connection.sid;
		//	App.connection._connection.addHandler(App.connection.notifyUser, null, null, null, null, null);

			App.editor.render();
			var langCode = App.connection.languageCode;
			var countryCode = App.connection.countryCode;
			App.chatStartTime = moment().lang(langCode+'-'+countryCode).format('h:mm A, DD MMM YYYY');
//			if(App.utils.isVisitorMobile())
//				App.editor.showAttachMobile(App.isChatAttachmentEnabled);
//			App.connection.startQueueStatusPoll();
//			App.connection.connected = true;
			setTimeout(function(){App.transcript.getTranscriptTemplate();},0);
		}
		
	
	},



    //Put the logic to submit the message
    //to the server in this function.
    sendNormalMessage : function(messageHtml, type, command) {

			mesg = {
	           rqst: "INTERPRETER:USERINPUT",
		       config: App.entraiapp,
		       username: window.localStorage.getItem('formValues').full_name,
		       userid: "",
		       sessionid: App.sessionid,
		       msg: messageHtml
            };

           App.websocket.send( JSON.stringify(mesg));


    	App.connection.lastRequestMessageTime=Date.now();
		return true ;

    	var CMD_CUST_INITIATE_VIDEO = 'custinitiatevideo';
    	var CMD_CUST_INITIATE_AUDIO = 'custinitiateaudio';
		var CMD_AGENT_INITIATE_VIDEO = 'agentinitiatevideo';
		var CMD_AGENT_AUTO_REJECT_VIDEO = 'agentautoreject';
		var CMD_CUST_AUTO_REJECT_VIDEO = 'custautoreject';
		var CMD_CUST_REQUEST_ATTACHMENT = 'custrequestattachment';

			if ($.trim(messageHtml
				//.replace(/<[^>]*>/g, '')
				.replace(/<br ?\/?>/g, '')
				.replace(/&nbsp;/g, '')).length == 0)
			return;

		// TODO remove once the agent console has been fixed to force anchors in
		// messages to be opened in a new window
		messageHtml = messageHtml.replace(/<a\b/g, '<a target="_blank"');

		// Below method escapes '\n' while counting the message length, so replacing all '\n' with 'carriage return'
		// to get the correct message which also includes line change and space (Specific to IE)
		var messageLength = $("<div>").html(messageHtml.replace(/\n/g, "&crarr;")).text().length ;

		// TODO remove once the server has been fixed to cope with line breaks
		messageHtml = messageHtml.replace(/\n/g, ' ');

		//code added to capture "ChatCustomerMsg" event
		if (typeof messageHtml !== 'undefined' && type != 'headline' && App.offRecord !== true){
			App.OneTagEvents.pushChatCustomerMsg(messageHtml);
		}

		var extraCharacters = messageLength - eGainLiveConfig.maxMessageSize;
		if (extraCharacters > 0) {
			if (L10N_MESSAGE_LENGTH_ERROR && L10N_MESSAGE_LENGTH_ERROR.length > 0) {
				var alertMessage = App.utils.getFormattedMessage(L10N_MESSAGE_LENGTH_ERROR,[eGainLiveConfig.maxMessageSize, extraCharacters]);
				setTimeout(function(){alert(alertMessage);},0);
			}
			return;
		}

		if (!type) {
			type = 'chat';
		}
		if(typeof command != "undefined")
		{

			this._connection.send(($msg( {
						'type' : type
					}).c('body').t(messageHtml).up().c(EGAIN_CMD,{xmlns:"http://bindings.egain.com/chat"}).c(EGAIN_SUB_CMD).t(command).tree()));
		}
		else
		{
		return true;
			this._connection.send($msg( {
				'type' : type
			}).c('body').t(messageHtml).tree());
		}


		if(command != CMD_CUST_INITIATE_VIDEO || command != 'CMD_CUST_INITIATE_AUDIO')
		{
			clearTimeout(customerIsTyping);
			customerIsTyping = null;
		}
		return true;
    },

	sendNotTypingMessage : function() {
		return;
		customerIsTyping = null;
		this._connection.send($msg( {
			'type' : 'normal'
		}).c('body').t(L10N_NO_TYPING_MESSAGE).tree());

    },

	sendTypingMessage : function() {
        return;
		this._connection.send($msg( {
			'type' : 'normal'
		}).c('body').t(L10N_TYPING_MESSAGE).tree());

    },

	checkExpiredLink : function() {
		if(App.chat_video_view.video.videoShowing ||
			((App.connection.agentInvitationTime > 0) && ((Date.now() - App.connection.agentInvitationTime) > eGainLiveConfig.videoReofferTimeout))){
			if(App.connection.sendNormalMessage(link_expired_alert, 'headline'))
				App.messenger.showNotification(link_expired_alert,'chatInput headline');
			return true;
		}

		App.connection.agentInvitationTime = Date.now() - eGainLiveConfig.videoReofferTimeout;	// Mark as expired for next time
		return false;
    },




	
    checkEligibility : function(callback) {
		var str = "";
		if (this.videochat)
			str = '&videoChat=1';

		$.ajax( {
			type : 'GET',
			url : this.CHAT_ELIGIBILITY_URL + '/' + this.entryPointId + window.location.search + str,
			dataType : 'xml',
			success : function(xml) {
				/*
				 * <?xml version="1.0" encoding="UTF-8" standalone="yes" ?>
				 * <agentAvailability available="true"
				 * xmlns:ns2="com/egain/live/framework/bosh/gen"
				 * xmlns:ns4="urn:ietf:params:xml:ns:xmpp-stanzas"
				 * xmlns:ns3="jabber:client" />
				 */
				var responseType = $(xml).find('checkEligibility').attr('responseType');
					callback(responseType);

			}
		});

    },

    initializeChat : function(callback) {
	
    json = {
		isOneTagOff: true,
		checkEligibility : {responseType : 0 } ,
		maskingPatterns : {maskingPattern: false },
		isVideoChatLicensed  : false,
		isVideoChatLicensed : false,
		isVideoChatEnabled : false,
		isVideoChatEnabled : false,
		videoChatMaxEscalation : 5,
		videoChatMaxEscalation : 5,
		isDirectAudioChatEnabled : false,
		isDirectAudioChatEnabled : false,
		isChatAttachmentEnabled : false,
		isChatAttachmentEnabled : false,
		maxChatAttachmentSize : false,
		listValue: false,
		isBlackListType : false,
		isOffRecordEnabled  : false} ;

				//onetag instrumentation
				
					callback(json);


    },

    chatAwaitingAgentCaptured : false,

	getUrlParameter : function(name) {
		name = name.replace(/[\[]/, '\\\[').replace(/[\]]/, '\\\]');
		var results = new RegExp('[\\?&]' + name + '=([^&#]*)')
				.exec(window.location.href);
		return results ? results[1] : null;
	},

	getAvMode : function() {
		if (App.videoChatMaxEscalation > 4) {
			// No adjustment required.  Optimized path for most common case.
		}
		else if (App.videoChatMaxEscalation == 2) {
			if (this.avmode > 0)
				return TWO_WAY_AUDIO_MODE;
		}
		else if (App.videoChatMaxEscalation == 3) {
			if (this.avmode == TWO_WAY_AUDIO_VIDEO_MODE)
				return TWO_WAY_AUDIO_CUSTOMER_VIDEO_MODE;
			else if (this.avmode == TWO_WAY_AUDIO_AGENT_VIDEO_MODE)
				return TWO_WAY_AUDIO_MODE;
		}
		else if (App.videoChatMaxEscalation == 4) {
			if (this.avmode == TWO_WAY_AUDIO_VIDEO_MODE)
				return TWO_WAY_AUDIO_AGENT_VIDEO_MODE;
			else if (this.avmode == TWO_WAY_AUDIO_CUSTOMER_VIDEO_MODE)
				return TWO_WAY_AUDIO_MODE;
		}

		return this.avmode;
	},

    /**

        Sends the login information to the server.

    **/


    logout : function() {

		if (this._connection && this._connection.connected)
			this._connection.disconnect(L10N_CLOSE_MESSAGE);
    },


	stopVideoCall : function() {


    },

    pauseVideoCall : function() {


    },



	populateoptions : function(callback){
		var inputXML = "<attributesInfo languageCode='";
		inputXML += this.languageCode;
		inputXML += "' countryCode='";
		inputXML += this.countryCode;
		inputXML += "' xmlns = 'http://bindings.egain.com/chat' >";

		var flag = 0;
		var parameterDefinitions = eGainLiveConfig.loginParameters;
		for ( var i = 0; i < parameterDefinitions.length; i++) {

			var paramDef = parameterDefinitions[i];

			if (paramDef.fieldType == 3 || paramDef.fieldType == 4)
			{
				flag = 1;

				inputXML += '<attributeInfo>';

				inputXML += '<attributeName>';
				inputXML += paramDef.attributeName;
				inputXML += '</attributeName>';

				inputXML += '<objectName>';
				inputXML += paramDef.objectName;
				inputXML += '</objectName>';

				inputXML += '</attributeInfo>';
			}
		}

		inputXML += '</attributesInfo>';

		if (flag == 1) {

			$.ajax( {
				url : this.ATTRIBUTES_INFO_URL,
				type : 'POST',
				data : inputXML,
				contentType : 'text/XML',
				dataType : 'xml',
				success : function(xml) {
							callback(xml);
				}


			});
		}
		else
			callback();
	},



	/**
		Send system message.
		message - system message which will be displayed in agent console
	**/
	sendSystemMessage : function(message,command){
		return App.connection.sendNormalMessage(message, 'headline',command);
	},



});


