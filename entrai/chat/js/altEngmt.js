
App.AlternateEngagementView = Backbone.View.extend({

    className : 'eg-chat-altEngmt',

    template : _.template($('#tpl-altEngmt').html()),

    events : {

        'click .queue-status' : 'toggleOptions'
        
    },
    initialize : function() {
       
    },

	 render : function() {
        
        this.$el.html(this.template());
    },
	
	resize : function(width){
	this.$el.find('#outer').width(width-2);

	},
    showOptions : function() {
	  
	   this.$('#alt-engmt-panel').show();
	   App.altEngmtHeight=$('#outer').height();
       $('#triangle').addClass('triangle-down').removeClass('triangle-up');	
		App.chat_stream_view.resize(App.altEngmtHeight);

    },

    lastStatus : '',
	polloingStatus : '',
	
	updatePollingStatus : function (status) {
		this.polloingStatus = status;
	},
	
	showQueueStatus : function(queueStatus) {
		if(queueStatus != undefined && queueStatus != null){
			if(this.polloingStatus)
			{
				if(this.lastStatus !== queueStatus)
				{
					this.$('.queue-status').css('display','block','align','center');
					this.$('.queue-status-text').html(queueStatus);
					this.lastStatus = queueStatus;
				}
				App.qStatusHt=$('#queueStatusTable').height();
				if (typeof App.resizedForStatus == 'undefined'){
					App.chat_stream_view.resize();
					App.resizedForStatus = true;
					}
				if (App.showAltEngmtOptions == true && typeof App.altEngmtOptionsShown=='undefined'){
					App.altEngmtOptionsShown = true;
					this.showOptions();
					this.$('#queueStatusTable').addClass('queueStatusTblAltEng');
				}
				else
					$( ".queue-status").unbind( "click" );
			}
		}	
		else{
			//update polloing status to stopped polling
			this.polloingStatus = 0;
			$( ".queue-status").unbind( "click" );
			this.$('.queue-status').css("display",'none');
			this.lastStatus='';
			delete App.resizedForStatus;
			if( App.showAltEngmtOptions == true ){
				$('#triangle').removeClass('triangle-up triangle-down');
				this.$('#alt-engmt-panel').hide();
				this.$('#queueStatusTable').removeClass('queueStatusTblAltEng');
			}
		App.chat_stream_view.resize();	
			
		}
	 },
	
	toggleOptions : function(){
			if (App.showAltEngmtOptions == true){
			if ($('#alt-engmt-panel').is(':hidden')) {
			$('#alt-engmt-panel').slideDown(500);
			$('#triangle').addClass('triangle-down').removeClass('triangle-up');
			App.chat_stream_view.resize(App.altEngmtHeight);
		 }
		else {
			$('#alt-engmt-panel').slideUp(500);
			$('#triangle').addClass('triangle-up').removeClass('triangle-down');
			App.chat_stream_view.resize(App.qStatusHt);
		 }
	  }	
   	}
});
