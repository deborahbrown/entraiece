/**

    Deflection is a view responsible for deflecting the chat by showing articles related to the
        question entered in login form. It also calls escalation APIs to register the events.

**/
App.Deflection = Backbone.View.extend({

    className : 'eg-chat-deflection',

    template : _.template($('#tpl-deflection').html()),

    deflectionContent : '',

    clickedArticle : null,
    
	backButtonData: [],
	
	currentArticleUrl: null,
    
	events : {
        'keydown #articleLink' : 'keyPressEventHandler',
        'keydown #articleContentDiv' : 'keyPressEventHandlerScrollbar',
        'click #articleLink' : 'onArticleLinkClick',
		'click #startChat' : 'onStartChatClick',
        'click #closeHelp' : 'onCloseHelpClick',
        'click #backButton' : 'onBackButtonClick' ,
        "click .print-deflection-button" : "onArticlePrintClick",
        "click .share-deflection-button" : "emailFriend",
        'click .overview .article-content a': 'clickedArticleContentLink'
    },


        /**
         * Checks if the value entered is valid for the given fieldType
         */
     validateEntry : function(fieldValue, fieldType) {
        
     switch(fieldType) {
                
        case 'emailaddress' :   var valid = true;
                        //Check if the email address entered is valid.
                    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\ ".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA -Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                    valid =  re.test(fieldValue);
                    return valid;
                
         case 'date' :   var valid = true;
                    var re = /^\d{1,2}\/\d{1,2}\/\d{4}$/;   //Check if the date format is correct
                    valid = re.test(fieldValue);
                    
                    var dateBits = fieldValue.split('/');
                    var dateObj = new Date(dateBits[2], dateBits[1] - 1, dateBits[0]);
                    var currentDate = new Date();
                    //check if the date is valid and after year 1900 and before current date.
                    valid = dateObj && (dateObj.getMonth() + 1) == dateBits[1] && dateObj.getDate() == Number(dateBits[0]) && (dateObj < currentDate) && (dateObj.getFullYear() > 1900) ;
                    return valid;
            }           
        },

         send : function(fromEmail, fromName, toEmail, toName, message, articleId, context, success) {
            
            if(!articleId)
                throw new Error("articleId is needed to send email through the Email model");
            $.ajax({
                type: 'POST',

                url : '../../../ws/v11/ss/email'+'?portalId='+eGainLiveConfig.chatDeflectionPortalId+'&usertype=customer&$format=json',
                contentType: "application/json",                
                headers: {
                    Accept: "application/json",
                    "X-egain-session": this.eGainEscalationSessionId
                },

                data: {
                    "portalId" : eGainLiveConfig.chatDeflectionPortalId,
                    "data" : JSON.stringify({
                
                        "fromEmail" : fromEmail,
                        "fromName" : fromName.replace(' ', '+'),
                        "toEmail" : toEmail,
                        "toName" : toName.replace(' ', '+'),
                        "message" : message,
                        "articleId" : articleId
                    }),
                    "usertype" : "customer",
                    "$format" : "json"
                },

                success : function() {

                    console.log("email sent successfully");
                    //Call callback, for now.
                    success.call(context);
                },
                error: function(){
                    console.log("error block");
              alert("error");
              }

            });
        },
        
     close: function() {

            var modal = $('.egce-modal-overlay');

            modal.fadeOut(400, function() { modal.remove(); });


    },

     emailFriend : function(e) {

                e.preventDefault();
                
                var _this = this;
                App.emailfriend = new App.Emailfriend();
                App.emailfriend.render();
                
                },


     onArticlePrintClick : function(e) {

                e.preventDefault();
               
               
                //Hide the global wrap.
                $('#eg-chat-global-wrap').hide();

                //Create the print wrap for the print version.
               
                divContent = $(".article-content")[1].outerHTML;
                articleName = $("#articleNameTag").text();

                $("body").append(divContent);
                $("body> .article-content").prepend("<h3>"+articleName+"</h3><hr>");
                window.print();
                console.log($(".article-content").length);

                $("body> .article-content").remove();

                $('#eg-chat-global-wrap').show();
                 
              
        },

    clickedArticleContentLink : function (e){
        e.preventDefault();
		var a = document.createElement('a');
        a.href = e.currentTarget.href;
		a.hostname;
		if(!(document.location.host==a.hostname))
		{
			window.open(e.currentTarget.href, "_blank" , 'toolbar=yes, scrollbars=yes, resizable=yes, top=60, left=500, width=600, height=600');
		}
		else
		{
			_this.backButtonData.push({goBackTo: 'article', articleUrl: _this.currentArticleUrl});
			_this.currentArticleUrl = e.currentTarget.href;
			_this.fetchArticleContent(e.currentTarget.href, function(response) {
				
			_this.displayArticleContent(response);
				
			});
		}
    },

    initialize : function() {
        this.AVERT_ESCALATION_URL = "../../../ws/v11/ss/escalate/avert";
        this.CHAT_DEFLECTION_URL = "../../../ws/v11/ss/escalate/search";
        this.ARTICLE_URL = "../../../ws/v11/ss/article";
        $('#eg-chat-content').empty();
        $(window).on('resize.egain.deflection', _.bind(this.onWindowResize, this));
        _this = this;
    },

    render : function() {
        var width = $(window).width();
        $('#eg-chat-header a.closechat').attr('title',L10N_WINDOW_CLOSE_BUTTON);
        $('#eg-chat-header').width('100%');
        $('#eg-chat-content').css('width','100%');
        $('#eg-chat-content').html(this.$el);
		this.$el.find('.submit-section').width(width-App.pageOffset);

        this.fetchDeflectionArticles(function(data){
            console.log(data);
            if(data.article.length > 0){
                _this.deflectionArticles = data.article;
                var html = _this.template({deflectionArticles:_this.deflectionArticles, window:window, article : {},  query: App.session.get('subject') });
                _this.$el.html(html);

                _this.$formWrap= _this.$el.find('.form-fields');
                try{
                _this.$formWrap.tinyscrollbar({ sizethumb: App.scrollsize });
                }
                catch(e)
                {}
                $('.article-content').hide();

                _this.resize();
                if(App.utils.isVisitorIos())
                {App.utils.hideAddressBar();}
            }
            else{
                    _this.eGainEscalationSessionId = "";
                _this.startChat();
            }
        });


    },
	keyPressEventHandler : function(event){
		if(event.keyCode == 13){
			this.onArticleLinkClick(event);
            event.preventDefault();
		}
	},

	keyPressEventHandlerScrollbar: function(event){
        if(event.keyCode == 38){
            _this.$formWrap.tinyscrollbar_updatescroll(150);
            event.preventDefault();
        }
        else if(event.keyCode == 40){
             _this.$formWrap.tinyscrollbar_updatescroll(-150);
             event.preventDefault();
        }
    },
    onArticleLinkClick :function(e) {
        var articleId = $(e.currentTarget).attr('data-article-id');
		_this.currentArticleUrl = _this.getArticleUrlFromId(articleId);
        this.clickedArticle = articleId;
        this.fetchArticleContent(_this.getArticleUrlFromId(articleId),function(data){
			_this.backButtonData.push({goBackTo: 'list'});
			_this.displayArticleContent(data);

        });
    },

    onCloseHelpClick : function(e) {
        /*Avert Escalation*/
        $.ajax({
        type: "GET",
        headers: {
            Accept: "application/json",
            "X-egain-session": this.eGainEscalationSessionId
        },
        url: this.AVERT_ESCALATION_URL,
        contentType: "application/json",
        data: {
            "usertype": "customer",
            "entryPointId" : App.connection.entryPointId,
            "portalId": eGainLiveConfig.chatDeflectionPortalId
        },
        success: function(data, status, request) {
            console.log("Escalation Avoided");
            _this.escalationAvertSuccess = true;
        }});

        this.closeChatWindow();
    },

    onStartChatClick : function() {
        this.startChat();
    },

    onWindowResize : function() {
		try{
        this.resize();
        }
        catch(err)
        {
			//IE8 specific handling- while loading the deflection window to is not available
        }
    },

    resize : function() {
       /* var windowHeight = $(window).height();
    var windowWidth = $(window).width();
        var topOffset = this.$el.offset().top;

    this.$('.submit-section').width(windowWidth-App.pageOffset);
        this.$('.deflection-option').width(windowWidth-App.pageOffset);*/

        if(App.utils.isVisitorMobile()){
                var $form = this.$el.find('.form-fields');
                this.$el.height('auto');
                var height = $form.find('.viewport .overview').height();


                $form.height(height);
                $form.find('.viewport').height(height);

                $('#eg-chat-content').addClass('Nofloating-submit-section');
        }
        else{
                var windowHeight = $(window).height();
        var windowWidth = $(window).width();
                var topOffset = this.$el.offset().top;
        console.log('offset inside deflection', topOffset);

        this.$('.submit-section').width(windowWidth-App.pageOffset);

        this.$el.height(windowHeight-App.submitSectionHeight-topOffset);
        $form = this.$el.find('.form-fields');
        var formOffset = $form.offset().top;
        var submitTop = $('.submit-section').position().top;
        var height = submitTop-formOffset;

        var width = $(window).width();
        $form.height(height - (this.$('.deflection-option').height()+40));
        $form.find('.viewport').height(height - (this.$('.deflection-option').height()+40));
        $form.find('.scrollbar').height(height - (this.$('.deflection-option').height()+40));
        try{
            $form.tinyscrollbar_update('relative');

        }catch(error){}

        }
        this.delegateEvents();

    },

    onBackButtonClick :function(e) {
        e.preventDefault();
        var width = $(window).width();
        $('#eg-chat-header a.closechat').attr('title',L10N_WINDOW_CLOSE_BUTTON);
        $('#eg-chat-header').width('100%');
        $('#eg-chat-content').css('width','100%');
        $('#eg-chat-content').html(this.$el);
    this.$el.find('.submit-section').width(width-App.pageOffset);
	var backButtonCurrentState = _this.backButtonData.pop();
        if(backButtonCurrentState.goBackTo == 'article') 
		{
			_this.fetchArticleContent(backButtonCurrentState.articleUrl,function(data){
			_this.displayArticleContent(data);

            
        });
		}
		else{
        var html = this.template({deflectionArticles:this.deflectionArticles, window:window, article : {}, query: App.session.get('subject')});
        this.$el.html(html);
        this.$formWrap= this.$el.find('.form-fields');
        try{
        this.$formWrap.tinyscrollbar({ sizethumb: App.scrollsize });
        }
        catch(e)
        {}
        this.$el.find('.article-content').hide();

        this.resize();
        if(App.utils.isVisitorIos())
        {App.utils.hideAddressBar();}
		}

        return this;
    },

    fetchDeflectionArticles : function(callback){

        var xEgainSessionId = "";
        try
        {
            if(typeof window.opener != "undefined" && typeof window.opener.$ != "undefined")
            {
                xEgainSessionId = window.opener.app.getCookie("sessionId")? window.opener.app.getCookie("sessionId"):"";
            }
            else
            {
                xEgainSessionId = this.getCookie("sessionId")? this.getCookie("sessionId"):"" ;
            }
        }
        catch(err)
        {
        }

		if (xEgainSessionId == "") {
		    $.ajax({
                type: "POST",
                headers: {
                    Accept: "application/json",
					"Accept-Language": "en-US"
                },
		        url: "../../../ws/v15/ss/portal/"+eGainLiveConfig.chatDeflectionPortalId+"/authentication/anonymous",
                contentType: "application/json",
                data: {},
                success: function(data, status, request) {
                    _this.eGainEscalationSessionId = request.getResponseHeader('X-egain-session');
			        _this.searchArticles(callback);
                },
                error: function(){
			        _this.startChat();
                }
            });	
		} else {
            _this.eGainEscalationSessionId = xEgainSessionId;
			_this.searchArticles(callback);
		}
    },
	
	searchArticles: function(callback) {
		$.ajax({
			type: "GET",
			headers: {
				Accept: "application/json",
				"X-egain-session": _this.eGainEscalationSessionId //fetch from cookie if exists
			},
			url: this.CHAT_DEFLECTION_URL,
			contentType: "application/json",
			data: {
				"usertype": "customer",
				"portalId": eGainLiveConfig.chatDeflectionPortalId,
				"subject" : App.session.get('subject'),
				"description" : App.session.get('subject'),
				"name" : App.session.get('full_name'),
				"emailAddress" : App.session.get('email_address'),
				"phone" : App.session.get('phone_number'),
				"entryPointId" : App.connection.entryPointId,
				"maxResults"  : eGainLiveConfig.maxDeflectionArticles,
				"$attribute" : "description,name",
				"$pagesize" : eGainLiveConfig.maxDeflectionArticles    
			},
			success: function(data, status, request) {
				callback(data);
			},
			error: function(){
				_this.startChat();
			}
        });
	},

	getArticleUrlFromId: function(articleId) {
		return (this.ARTICLE_URL + "/" + articleId);
	},
	
	displayArticleContent:function(data){
		if(data.article.length > 0){
                var html = _this.template({deflectionArticles:_this.deflectionArticles, window:window, article : data.article[0], query: App.session.get('subject') });
                _this.$el.html(html);

                _this.$formWrap= _this.$el.find('.form-fields');
                try{
                _this.$formWrap.tinyscrollbar({ sizethumb: App.scrollsize });
                }
                catch(e)
                {}
                _this.$el.find('.title-bar').hide();
                _this.$el.find('.header').hide();
                _this.$el.find('.article-list').hide();
                _this.$el.find('.article-content').show();

                _this.resize();
                if(App.utils.isVisitorIos())
                {App.utils.hideAddressBar();}
            }
			else{

            }
		},
    fetchArticleContent : function(articleUrl, callback){
        $.ajax({
            type: "GET",
            headers: {
                Accept: "application/json",
                "X-egain-session": this.eGainEscalationSessionId
            },
            url: articleUrl,
            contentType: "application/json",
            data: {
                "usertype": "customer",
                "portalId": eGainLiveConfig.chatDeflectionPortalId,
                "$attribute" : "name,content,contentText"
            },
            success: function(data, status) {
                callback(data);

            }
        });
    },

    startChat : function(){
         $('#eg-chat-content').empty();
         $(window).off('.egain.deflection');
        //connect to the server
        App.connection.xEgainSession = this.eGainEscalationSessionId;
        App.connection.connect(App.session.get('email_address'));


        //Initialize the chat wrap view
        App.chat_wrap_view = new App.ChatWrapView();
        App.chat_wrap_view.render();

        this.trigger('start');
        App.session.startWithInitialMessage(App.utils.masker.maskData(App.session.get('subject')));
    },

    closeChatWindow : function(){
        if(this.escalationAvertSuccess === undefined){
            setTimeout(function(){_this.closeChatWindow();},10);
            return;
        }
        window.open("","_self","");
        window.close();
    },

    getCookie : function(cname){
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for(var i=0; i<ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0)==' ') c = c.substring(1);
            if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
        }
        return "";
    }
});
