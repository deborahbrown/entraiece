// avmode values
var AGENT_VIDEO_MODE = 1;
var TWO_WAY_AUDIO_VIDEO_MODE = 2;
var AGENT_AUDIO_MODE = 3;
var TWO_WAY_AUDIO_MODE = 4;
var TWO_WAY_AUDIO_AGENT_VIDEO_MODE = 5;
var TWO_WAY_AUDIO_CUSTOMER_VIDEO_MODE = 6;

/**

    Connection is a wrapper object around Strophe's connection instance.

**/
App.Connection = Backbone.Model.extend({

	acceptRejectMsgSent: false,
	agentInvitationTime: 0,
	custInvitationTime: 0,

    initialize : function() {
    	this.chatStartedPrefix="";
		this.chatStartedSuffix="";
		this.chatEndedPrefix="";
		this.chatEndedSuffix="";
    	this.lastResponseMessageTime=-1;
		this.lastRequestMessageTime=-1;
		this.chatExited=false;
		this.systemTimeOutMessage="";

		//added to fetch msg values from properties file for chat instrumentation
		if(messagingProperty){
			var mesagingPropertyVariables=messagingProperty.split('\n');
			var foundStartChatString=false;
			var foundEndChatString=false;
			var foundsystemTimeOutMsg=false;
			for(i=0;i<mesagingPropertyVariables.length;i++){

				if(mesagingPropertyVariables[i].indexOf('agent_join_msg')===0){

					var keyValuePair=mesagingPropertyVariables[i].split('=');
					var chatStartedString=$.trim(keyValuePair[1]);
					var temp=chatStartedString.split('{0}');
					this.chatStartedPrefix=temp[0];
					this.chatStartedSuffix=temp[1];
					foundStartChatString=true;
				}

				else if(mesagingPropertyVariables[i].indexOf('agent_close_session')===0){

					var keyValuePair=mesagingPropertyVariables[i].split('=');
					var chatEndedString=$.trim(keyValuePair[1]);
					var temp=chatEndedString.split('{0}');
					this.chatEndedPrefix=temp[0];
					this.chatEndedSuffix=temp[1];
					foundEndChatString=true;

				}
				else if(mesagingPropertyVariables[i].indexOf('system_close_session')===0){
					var keyValuePair=mesagingPropertyVariables[i].split('=');
					this.systemTimeOutMessage=$.trim(keyValuePair[1]);
					foundsystemTimeOutMsg=true;
				}
				else if(foundStartChatString===true && foundEndChatString===true && foundsystemTimeOutMsg===true)
					break;
			}
		}


		//added for evaluating time difference for chat events
    	this.agentIdentityForNotification = null;
        //This will be strophe's connection instance.
        this._connection = null;
		this.connected = false;
		// BOSH_SERVICE must be same domain and port that served the chat client
		var base = document.URL;
		var contextRoot = base.substr(base.indexOf("//")+2, base.length);
		contextRoot = contextRoot.substr(contextRoot.indexOf("/") + 1);
		contextRoot = "/" + contextRoot.substr(0, contextRoot.indexOf("/"));
		this.BOSH_SERVICE = contextRoot+ '/egain/chat/entrypoint';
		this.QUEUE_STATUS_URL = this.BOSH_SERVICE + '/sessionStatistics'; // BOSH session id is to be appended
		this.SURVEY_URL = this.BOSH_SERVICE + '/survey';
		this.AGENT_AVAILABILITY_URL = this.BOSH_SERVICE + '/agentAvailability';
		this.CHAT_ELIGIBILITY_URL = this.BOSH_SERVICE + '/checkEligibility';
		this.NOTIFY_URL = './notify.html';
		this.ATTRIBUTES_INFO_URL = this.BOSH_SERVICE + '/attributesInfo';
		this.GET_MEDIA_SERVER_URL = this.BOSH_SERVICE + '/mediaServer';
		this.INITIALIZATION_URL = this.BOSH_SERVICE + '/initialize';

		this.entryPointId = '1000'; // default if no parameter
		this.jid = 'egain@egain.com'; // default if no parameter
		this.languageCode = 'en'; // default if no parameter
		this.countryCode = 'US'; // default if no parameter
		if (this.getUrlParameter('entryPointId')) {
				this.entryPointId = this.getUrlParameter('entryPointId');
		}
		if (this.getUrlParameter('jid')) {
			this.jid = this.getUrlParameter('jid');
		}
		if (this.getUrlParameter('languageCode')) {
			this.languageCode = this.getUrlParameter('languageCode').toLowerCase();
		}
		if (this.getUrlParameter('countryCode')) {
			this.countryCode = this.getUrlParameter('countryCode').toUpperCase();
		}

		if (this.getUrlParameter('video'))
			this.videochat = (parseInt(this.getUrlParameter('video')) == 1) && (typeof eGainMobileChat != 'undefined' || !App.utils.isVisitorMobile()); //Added eGainMobileChat check for mobile sdk

//		alert("connection.js initialize() avmode parameter: " + this.getUrlParameter('avmode'));
		if (this.getUrlParameter('avmode'))
			this.avmode = parseInt(getUrlParameter('avmode')); //read the video chat mode
		else
			this.avmode = 2;
	},

    //Connect to the BOSH server.
    connect : function(email) {

			var connectionUrl = this.BOSH_SERVICE + window.location.search;
			var timezoneoffset=new Date().getTimezoneOffset()*-1;
			connectionUrl = connectionUrl + '&custtimeoffset='+timezoneoffset;

			if (this.videochat)
				connectionUrl = connectionUrl + '&videoChat=1';

			this._connection  =  new Strophe.Connection(connectionUrl);
			// add egain specific attributes
			var from = 'anonymous@egain.com';
			if (email) from = email;
			from = 'smompt1@aol.com';

			this._connection.from = (from).replace(/'/g, '&apos;');

			this._connection.entryPointId = this.entryPointId;
			this._connection.xEgainEscalationHeader = this.xEgainSession;
			this._connection.lang = this.languageCode + '-' + this.countryCode;

			// Hook to set and override parameters for customization
			setConnectionParametersHook();
			this._connection.connect(this.jid, '', this.onConnect);


    },

	onConnect : function(status, condition) {

		if(status == Strophe.Status.CONNECTED)
		{
			window.onbeforeunload = function() {
				if(App.mailClicked)
					return;
				else
					return L10N_BROWSER_CLOSE_MESSAGE;
			}
		//	if (App.session.get('subject')) {
			if (false) {
		   if(!eGainLiveConfig.enableChatDeflection || typeof eGainLiveConfig.chatDeflectionPortalId == "undefined" || eGainLiveConfig.chatDeflectionPortalId == "") {
	       App.session.startWithInitialMessage(App.utils.masker.maskData(App.session.get('subject')));
				}
			}

			
						

			App.connection.sid = App.connection._connection.sid;
			App.connection._connection.addHandler(App.connection.notifyUser, null, null, null, null, null);
//			if(App.connection.videochat)
//				App.chat_video_view.initializeVideo(true);
			App.editor.render();
			var langCode = App.connection.languageCode;
			var countryCode = App.connection.countryCode;
			App.chatStartTime = moment().lang(langCode+'-'+countryCode).format('h:mm A, DD MMM YYYY');
			if(App.utils.isVisitorMobile())
				App.editor.showAttachMobile(App.isChatAttachmentEnabled);
			App.connection.startQueueStatusPoll();
			App.connection.connected = true;
			setTimeout(function(){App.transcript.getTranscriptTemplate();},0);
		}
		else if(status == Strophe.Status.DISCONNECTED)
		{
			if(App.connection.confail)
				return;

			App.session.end();
			window.onbeforeunload = null;
		}
		else if (status == Strophe.Status.CONNFAIL) {
			window.onbeforeunload = null;
			if(!customWrapUpMessagesHook(condition)){
				if (condition == 'system-shutdown') {
					App.connection.confail = true;
					App.chat_wrap_view.showOffHours();
				}
				else if (condition == 'other-request') {
					App.connection.confail = true;
					App.chat_wrap_view.showDuplicateSession();
				}
				else if (condition.indexOf('remote-connection-failed') != -1) {
					App.connection.confail = true;
					var errorCode = condition.split(":");
					App.chat_wrap_view.showErrorPage(errorCode[1]);
				}
				else if (!App.connection._connection.disconnecting && condition != 'unknown') {
					App.connection.confail = true;
					App.messenger.showNotification(L10N_CONNFAIL + ' <span style="display:none">'
							+ condition + '</span>', 'error');
				}
			}
		}
	},

	notifyUser : function(msg) {
		var EG_CALL_Q = window.EG_CALL_Q || [];
		App.connection.connected = true;
		if (msg.getAttribute('type') && msg.getAttribute('from') && msg.getAttribute('from') == 'system')
		{
			var clearPollQueueStatusTimerVar = systemMessageIntegHook(msg);
			if (clearPollQueueStatusTimerVar)
				App.connection.clearPollQueueStatusTimer();
		}
		if (msg.getAttribute('type') == 'chat' && msg.getAttribute('from')) {
			agentIdent = msg.getAttribute('from');
			agentIdent = decodeMessage(agentIdent);
		}
		for ( var i = 0; i < msg.childNodes.length; i++) {
			if (!msg.childNodes[i].tagName) {
				continue;
			}
			if (msg.childNodes[i].tagName.indexOf(':widget') > 0) {
				continue;
			}
			if (msg.childNodes[i].tagName.indexOf(':body') < 0) {
				// continue;
			}
			var messageHtml = msg.childNodes[i].textContent;
			if (typeof messageHtml == "undefined") {
				messageHtml = msg.childNodes[i].text; // Internet Explorer
			}
			if((typeof msg.childNodes[i].baseName != "undefined" && msg.childNodes[i].baseName != null && msg.childNodes[i].baseName == EGAIN_CMD) || (typeof msg.childNodes[i].localName != "undefined" && msg.childNodes[i].localName != null && msg.childNodes[i].localName.trim() == EGAIN_CMD))
			{
				var egainCmd = null;
				if(typeof msg.childNodes[i].text != "undefined" && msg.childNodes[i].text!= null)
					egainCmd = msg.childNodes[i].text;
				else if(typeof msg.childNodes[i].textContent != "undefined" && msg.childNodes[i].textContent != null)
					egainCmd = msg.childNodes[i].textContent.trim();

				if(egainCmd != null && egainCmd != '')
				{
					messageHtml = "";
					if(egainCmd == CMD_CUST_INITIATE_VIDEO)
					{
						videochat = true;
//						App.chat_video_view.initializeVideo(true);
					}
					else if(egainCmd == CMD_AGENT_INITIATE_VIDEO)
					{
						// Getting agent's name from message object
						agentName = msg.getAttribute('from');
						agentIdent = "";
						agentInitiatedVideo = true;
						App.connection.agentInvitationTime = App.chat_video_view.videoStopped ? Date.now() : 0;
					}
					else if( egainCmd == "startVideo")
					{
						// right now, we are not doing anything, but if we need to do any changes for start video, then this is the place where we can handle it.
					}
					else if( egainCmd == "stopVideo")
					{
						App.connection.custInvitationTime = 0;	// Allow re-invitation immediately
						App.chat_video_view.hideVideo();
					}
                    else if (egainCmd.indexOf("egainCBCmd_") == 0)
                    {
                        App.messenger.addCBParamsToMsg(egainCmd);
                    }

					if(msg.childNodes[i].childNodes)
					{
						var addtionalParams = msg.childNodes[i];
						egainCmd = $(addtionalParams).find('ns2\\:subcmd,subcmd').text();

						if(egainCmd != null)
						{
							if(egainCmd == BACK_STOPPING_TRIGGERED || egainCmd == "VIDEO_CHAT_TOKEN" || egainCmd == 'custattachmentuploaded' || egainCmd == 'acceptcustattachment' || egainCmd == 'custacceptattachment' || egainCmd == 'rejectcustattachment' || egainCmd == "agentinitiateattachment")
							{
								var arr = {};
								$(addtionalParams).find('ns2\\:egainAdditionalParam,egainAdditionalParam').each(function(){
									arr[$(this).find('ns2\\:paramKey,paramKey').text()]=$(this).find('ns2\\:paramValue,paramValue').text();
								});
								if(egainCmd == BACK_STOPPING_TRIGGERED) {
									App.session.onBackStoppingTriggered(arr);
								}
								else if(egainCmd == 'custattachmentuploaded'){
									App.attachmentView.makeClickableLink(arr);
								}
								else if(egainCmd == 'custacceptattachment'){
									//addAcceptMessage to transcript
									App.messenger.addToTranscript(App.utils.getFormattedMessage(accepted_chat_attachment, [App.session.get('name'),arr.attachmentName]),'chatInput'+' '+'headline');
									//App.messenger.addToTranscript(App.utils.getFormattedMessage(attachment_received_by_receiver, [arr.agentName,arr.attachmentName]),'chatInput'+' '+'headline');
									App.attachmentView.addAttachmentIcon(arr);
								}
								else if (egainCmd == 'acceptcustattachment'){
                   			 		App.attachmentView.uploadFiles(arr);
                   			 		//App.messenger.addToTranscript(App.utils.getFormattedMessage(attachment_received_by_receiver, [arr.agentName,arr.attachmentName]),'chatInput'+' '+'headline');
                   			 	}
                   			 	else if(egainCmd == 'rejectcustattachment'){
                   					App.attachmentView.onAgentRejectCustAttachment(arr);
                   			 	} else if (egainCmd == 'agentinitiateattachment') {
                   			 		// Show image of accept/reject
                   			 		App.messenger.addToTranscript(App.utils.getFormattedMessage(attachment_to_chat_customer, [arr.agentName,arr.attachmentName]),'chatInput'+' '+'headline');
                   			 		App.attachmentView.onAgentInviteAttachment(arr, msg.getAttribute('from'));
                   				}
                   				else
                   				{
                   					App.connection.avmode = arr["VIDEO_CHAT_AVMODE"];

                   					App.chat_video_view.vendor = arr["VIDEO_CHAT_VENDOR"];
                   					if("opentok" == arr["VIDEO_CHAT_VENDOR"])
                   					{
                   						App.chat_video_view.openTokParams = arr;
                   					}
                   					// for mobile apps
                   					window.location.hash="#startvideo";
                   					App.connection.videochat = true;
                   					App.chat_video_view.initializeVideo(true);
                   				}
							}
							else if(egainCmd == CMD_AGENT_AUTO_REJECT_VIDEO)
							{
								App.connection.custInvitationTime = 0;	// Allow re-invitation immediately
							}
							else if(egainCmd == CMD_CUST_AUTO_REJECT_VIDEO)
							{
								agentInitiatedVideo = false;
							}
						}
					}
				}
			}

				var transferParam=msg.childNodes[i];
				var arr = {};
				$(transferParam).find('ns2\\:egainAdditionalParam,egainAdditionalParam').each(function(){
								arr[$(this).find('ns2\\:paramKey,paramKey').text()]=$(this).find('ns2\\:paramValue,paramValue').text();
							});
				var transType="";
				if("eglvtransfertype" in arr)
					{
					transType = arr['eglvtransfertype'];
						if(parseInt(transType)>0)
							{
							//Capturing ChatTransferEvent
							var transferType = (transType === '3') ? 'Agent'
									: (transType === '2') ? 'Queue' : 'Department';

							EG_CALL_Q.push([ "send", "cht", "ctx",
							                 document.URL, 101, {
								"EventName" : "ChatTransfer",
								"TransferType" : transferType
							} ]);

							App.connection.chatAwaitingAgentCaptured=false;
								var day= new Date();
								App.waitTimeInQueueStart = day.getTime();
								App.connection.startQueueStatusPoll();
							}
					}

				if("vchatCapable" in arr)
				{
					var vchatCapable = arr['vchatCapable'];
					if(transType=="2" || transType=="1")
					{
					 App.isVideoChatEnabled =(vchatCapable == "1")?true:false;
					 App.chat_wrap_view.showVideoButton(true);
					}
					else
					{
						var usrPermission = (vchatCapable == "1")?true:false;
						App.chat_wrap_view.showVideoButton(usrPermission);
					}
				}
				if("vchatMaxEscalation" in arr)
				{
					App.videoChatMaxEscalation = arr['vchatMaxEscalation'];
					App.isDirectVideoChatEnabled = ((App.videoChatMaxEscalation == 3 || App.videoChatMaxEscalation > 4));
					App.chat_video_view.adjustDirectChatButtons();
				}
				if("enableDirectAudioChat" in arr)
				{
					var enableDirectAudioChat = arr['enableDirectAudioChat'];
					App.isDirectAudioChatEnabled = (enableDirectAudioChat == "1");
					App.chat_video_view.adjustDirectChatButtons();
				}
				if("chatAttachment" in arr)
				{
					var chatAttachment = arr['chatAttachment'];
					console.log('chatAttachment came '+chatAttachment);
					var param = chatAttachment == 1 || chatAttachment == 3 ? true : false;
					App.isChatAttachmentEnabled = param;
					App.editor.showHideAttachmentIcon(param, App.offRecord);
					if(App.utils.isVisitorMobile())
					{
						App.editor.showAttachMobile(param);
					}
				}

			if(msg.getAttribute('type')){
				messageHtml = unescape(messageHtml);
				messageHtml = decodeMessage(messageHtml);
			}

			if (messageHtml.length && messageHtml.length >= 0) {
				var cssClass = 'chatInput';
				var type = msg.getAttribute('type');
				var agentIdentity = msg.getAttribute('from');
				agentIdentity = decodeMessage(agentIdentity);
				if (type) {
					if (type == 'normal') {

						if(messageHtml=="typing_message")
							 App.messenger.trigger('chatter-start-typing',agentIdentity);
						else
							App.messenger.trigger('chatter-stop-typing',agentIdentity);

						return true;
					}
					cssClass += ' ' + type;
				}
				if (type != 'headline')
					App.connection.lastResponseMessageTime=Date.now();
				if (type == 'headline') {
					// code added for capturing "ChatEngaged" Event
					if (messageHtml.search(App.connection.chatStartedPrefix)!=-1 && messageHtml.search(App.connection.chatStartedSuffix)!=-1) {
						var mode = (window.location.search
								.search("video=1") != -1) ? "video"
										: "text";
						EG_CALL_Q.push([ "send", "cht", "aev",
						                 document.URL, 101, {
							"EventName" : "ChatEngaged",
							"Chat-Type" : mode
						} ]);
					}
					if (messageHtml.search(App.connection.chatEndedPrefix)!=-1 && messageHtml.search(App.connection.chatEndedSuffix)!=-1) {
						App.connection.chatExited = true;
						EG_CALL_Q.push([ "send", "cht", "uac",
						                 document.URL, 101, {
							"EventName" : "ChatExit",
							"Termination-Initiation" : "Agent"
						} ]);
					}
					if (messageHtml.search(App.connection.systemTimeOutMessage)!=-1) {
						App.connection.chatExited = true;
						EG_CALL_Q.push([ "send", "cht", "aev",
						                 document.URL, 101, {
							"EventName" : "ChatExit",
							"Termination-Initiation" : "System"
						} ]);
					}
				}

				if(messageHtml == 'ActivitySubtypeChangedToVideoChat')
						continue;
				if(messageHtml == 'AgentInitiateAttachment') {
					continue;
				}
				App.audio.playChatMessageSound();
				if(agentIdentity != 'system' && messageHtml.length > 0 && type == 'chat'){
					startTitleAlert();

					if(messageHtml.indexOf('window.open') > -1 && navigator.userAgent.indexOf("Firefox") > -1)
					{
						var found = messageHtml.match(/window.open\((.*?(\,))/gi);
						var url = found[0].substring(13, found[0].length-2);
						url = url.trim();
						url = url.replace(/ /gi,'%20');
						messageHtml  = messageHtml.replace(/window.open\((.*?(\,))/gi,'window.open(\''+url+'\',');
					}
					
					App.messenger.submitMessageString(messageHtml,'agent',agentIdentity,false,cssClass);
				}
				else if(messageHtml.length > 0){
					App.connection.agentIdentityForNotification = agentIdentity;
					App.messenger.showNotification(messageHtml,cssClass);
				}

			}
		}
		//code added to capture "ChatAgentMsg" event
		if (typeof messageHtml !== 'undefined' && messageHtml.length > 0 && type != 'headline' && App.offRecord !== true){
			App.OneTagEvents.pushChatAgentMsg(messageHtml);
		}
		return true;
	},

    //Put the logic to submit the message
    //to the server in this function.
    sendNormalMessage : function(messageHtml, type, command) {

    	App.connection.lastRequestMessageTime=Date.now();

    	var CMD_CUST_INITIATE_VIDEO = 'custinitiatevideo';
    	var CMD_CUST_INITIATE_AUDIO = 'custinitiateaudio';
		var CMD_AGENT_INITIATE_VIDEO = 'agentinitiatevideo';
		var CMD_AGENT_AUTO_REJECT_VIDEO = 'agentautoreject';
		var CMD_CUST_AUTO_REJECT_VIDEO = 'custautoreject';
		var CMD_CUST_REQUEST_ATTACHMENT = 'custrequestattachment';

			if ($.trim(messageHtml
				//.replace(/<[^>]*>/g, '')
				.replace(/<br ?\/?>/g, '')
				.replace(/&nbsp;/g, '')).length == 0)
			return;

		// TODO remove once the agent console has been fixed to force anchors in
		// messages to be opened in a new window
		messageHtml = messageHtml.replace(/<a\b/g, '<a target="_blank"');

		// Below method escapes '\n' while counting the message length, so replacing all '\n' with 'carriage return'
		// to get the correct message which also includes line change and space (Specific to IE)
		var messageLength = $("<div>").html(messageHtml.replace(/\n/g, "&crarr;")).text().length ;

		// TODO remove once the server has been fixed to cope with line breaks
		messageHtml = messageHtml.replace(/\n/g, ' ');

		//code added to capture "ChatCustomerMsg" event
		if (typeof messageHtml !== 'undefined' && type != 'headline' && App.offRecord !== true){
			App.OneTagEvents.pushChatCustomerMsg(messageHtml);
		}

		var extraCharacters = messageLength - eGainLiveConfig.maxMessageSize;
		if (extraCharacters > 0) {
			if (L10N_MESSAGE_LENGTH_ERROR && L10N_MESSAGE_LENGTH_ERROR.length > 0) {
				var alertMessage = App.utils.getFormattedMessage(L10N_MESSAGE_LENGTH_ERROR,[eGainLiveConfig.maxMessageSize, extraCharacters]);
				setTimeout(function(){alert(alertMessage);},0);
			}
			return;
		}

		if (!type) {
			type = 'chat';
		}
		if(typeof command != "undefined")
		{
			this._connection.send(($msg( {
						'type' : type
					}).c('body').t(messageHtml).up().c(EGAIN_CMD,{xmlns:"http://bindings.egain.com/chat"}).c(EGAIN_SUB_CMD).t(command).tree()));
		}
		else
		{
			this._connection.send($msg( {
				'type' : type
			}).c('body').t(messageHtml).tree());
		}


		if(command != CMD_CUST_INITIATE_VIDEO || command != 'CMD_CUST_INITIATE_AUDIO')
		{
			clearTimeout(customerIsTyping);
			customerIsTyping = null;
		}
		return true;
    },

	sendNotTypingMessage : function() {
		customerIsTyping = null;
		this._connection.send($msg( {
			'type' : 'normal'
		}).c('body').t(L10N_NO_TYPING_MESSAGE).tree());

    },

	sendTypingMessage : function() {

		this._connection.send($msg( {
			'type' : 'normal'
		}).c('body').t(L10N_TYPING_MESSAGE).tree());

    },

	checkExpiredLink : function() {
		if(App.chat_video_view.video.videoShowing ||
			((App.connection.agentInvitationTime > 0) && ((Date.now() - App.connection.agentInvitationTime) > eGainLiveConfig.videoReofferTimeout))){
			if(App.connection.sendNormalMessage(link_expired_alert, 'headline'))
				App.messenger.showNotification(link_expired_alert,'chatInput headline');
			return true;
		}

		App.connection.agentInvitationTime = Date.now() - eGainLiveConfig.videoReofferTimeout;	// Mark as expired for next time
		return false;
    },

	sendAcceptVideoChat : function(userId, avmode) {
		if(this.checkExpiredLink())
			return;

		var sid = this.sid;
		var entryPointId = this.entryPointId

		var convertVideoChatUrl= this.BOSH_SERVICE+"/convertvideochat";
		//this check is to prevent second request being sent in case of double click
		if(!this.acceptRejectMsgSent){
			this.acceptRejectMsgSent = true;
			$.ajax( {
				type : 'POST',
				url : convertVideoChatUrl,
				data : {'sid' : sid, 'userId' : userId, 'entryPointId' : entryPointId, 'avmode' : avmode },
				dataType : 'text',
				success : function(string){
					App.connection.acceptRejectMsgSent = false;
					agentInitiatedVideo = false;
					if(string == "true"){
						App.connection.videochat = true;
					}
					else{
						if(App.connection.sendNormalMessage(link_expired_alert, 'headline'))
							App.messenger.showNotification(link_expired_alert,'chatInput headline');
					}
				}
			});
		}

    },

    sendDeclineVideoChat : function(userId, avmode){
		if(this.checkExpiredLink())
			return;

		var sid = this.sid;
		var entryPointId = this.entryPointId

		var rejectVideoChatUrl= this.BOSH_SERVICE+"/rejectvideochat";
		//this check is to prevent second request being sent in case of double click
		if(!this.acceptRejectMsgSent){
			this.acceptRejectMsgSent = true;
			$.ajax( {
				type : 'POST',
				url : rejectVideoChatUrl,
				data : {'sid' : sid, 'userId' : userId, 'avmode' : avmode},
				dataType : 'text',
				success : function(string){
					App.connection.acceptRejectMsgSent = false;
					agentInitiatedVideo = false;
					if(string == "true"){
					}
					else{
						if(App.connection.sendNormalMessage(link_expired_alert, 'headline'))
							App.messenger.showNotification(link_expired_alert,'chatInput headline');
					}
				}
			});
		}
	},

	sendAcceptChatAttachment : function(fileId, fileName) {
		var sid = this.sid;
		var entryPointId = this.entryPointId

		var attachmentAcceptUrl=  App.connection.BOSH_SERVICE+"/acceptattachment/"+encodeURIComponent(fileId);;
		//this check is to prevent second request being sent in case of double click
		if(!this.acceptRejectAttachSent){
			this.acceptRejectAttachSent = true;
			$.ajax( {
				type : 'POST',
				url : attachmentAcceptUrl,
				data : {'sid' : sid, 'entryPointId' : entryPointId, 'fileId' : fileId, 'fileName' : fileName},
				dataType : 'text',
				success : function(string){
					App.connection.acceptRejectAttachSent = false;
				},
				error : function () {
					App.messenger.showNotification(attachment_action_taken,'chatInput headline');
				}
			});
		}
    },

	sendRejectChatAttachment : function(fileId, fileName) {
		var sid = this.sid;
		var entryPointId = this.entryPointId

		var attachmentAcceptUrl =  App.connection.BOSH_SERVICE+"/rejectattachment/"+encodeURIComponent(fileId);;
		//this check is to prevent second request being sent in case of double click
		if(!this.acceptRejectAttachSent){
			this.acceptRejectAttachSent = true;
			$.ajax( {
				type : 'POST',
				url : attachmentAcceptUrl,
				data : {'sid' : sid, 'entryPointId' : entryPointId, 'fileId' : fileId, 'fileName' : fileName},
				dataType : 'text',
				success : function(string){
					App.connection.acceptRejectAttachSent = false;
				},
				error : function () {
					App.messenger.showNotification(attachment_action_taken,'chatInput headline');
				}
			});
		}
    },

    checkEligibility : function(callback) {
		var str = "";
		if (this.videochat)
			str = '&videoChat=1';

		$.ajax( {
			type : 'GET',
			url : this.CHAT_ELIGIBILITY_URL + '/' + this.entryPointId + window.location.search + str,
			dataType : 'xml',
			success : function(xml) {
				/*
				 * <?xml version="1.0" encoding="UTF-8" standalone="yes" ?>
				 * <agentAvailability available="true"
				 * xmlns:ns2="com/egain/live/framework/bosh/gen"
				 * xmlns:ns4="urn:ietf:params:xml:ns:xmpp-stanzas"
				 * xmlns:ns3="jabber:client" />
				 */
				var responseType = $(xml).find('checkEligibility').attr('responseType');
					callback(responseType);

			}
		});

    },

    initializeChat : function(callback) {
		var url = this.INITIALIZATION_URL + '/' + this.entryPointId + (this.videochat ? '?videoChat=1' : '');
    	$.ajax( {
			type : 'GET',
			url : url,
			dataType : 'json',
			success : function(json) {

				//onetag instrumentation
				if (json.isOneTagOff)
					callback(json);
				else if(json.oneTagAId && json.oneTagServerDomainName){
					//to prevent OneTag Tracker default behaviour
					EG_CALL_Q = window.EG_CALL_Q || [];
					EG_CALL_Q.push( ["disableTrackOnLoad"] );
					//snippet added to load OneTag Script
					var EG_ACT_ID=json.oneTagAId; (function(e,f){var d,c,b,a=e.createElement("iframe");a.src="about:blank";a.title="";a.id="egot_iframe";(a.frameElement||a).style.cssText="width:0;height:0;border:0";b=e.getElementsByTagName("script");b=b[b.length-1];b.parentNode.insertBefore(a,b);try{c=a.contentWindow.document}catch(g){d=e.domain,a.src="javascript:var d=document.open();d.domain='"+d+"';void(0);",c=a.contentWindow.document}c.open()._d=function(){var a=this.createElement("script");d&&(this.domain=d);a.src=f;this.isEGFIF= !0;this.body.appendChild(a)};c.write('<body onload="document._d();">');c.close()})(document,"//"+json.oneTagServerDomainName+"/onetag/"+EG_ACT_ID);
					callback(json);
				}
				else{
					App.messenger = new App.Messenger();
					(App.chat_wrap_view = new App.ChatWrapView()).showGenericErrorPage(L10N_ONE_TAG_FAILURE);
				}
			},
			error : function () {
					App.connection.confail = true;
					App.messenger = new App.Messenger();
					(App.chat_wrap_view = new App.ChatWrapView()).showOffHours();
				}
		});
    },

    chatAwaitingAgentCaptured : false,
    pollQueueStatus : function() {
		if ((typeof customSessionStatisticsHook != 'undefined' && customSessionStatisticsHook()) || eGainLiveConfig.sessionStastics == 0){
			// This means we do not need to send any request for session statistics.
			return;
		}

		var inputXML = "<sessionStatistics xmlns='http://bindings.egain.com/chat'><sid>";
		inputXML += this._connection.sid;
		inputXML += "</sid></sessionStatistics>";

		$.ajax( {
				type : 'POST',
				url : this.QUEUE_STATUS_URL,
				data : inputXML,
				contentType : 'text/XML',
				dataType : 'xml',
				success : function(xml) {
					/*
						<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
						<sessionStatus xmlns:ns2="com/egain/live/framework/bosh/gen"
						 xmlns:ns4="urn:ietf:params:xml:ns:xmpp-stanzas"
						 xmlns:ns3="jabber:client">
							<ns2:waitTime>8.0</ns2:waitTime>
							<ns2:queueDepth>2</ns2:queueDepth>
						</sessionStatus
					*/
					var queueDepth = $(xml).find('ns2\\:queueDepth, queueDepth')
							.text();
					var altEngmtTime = $(xml).find('ns2\\:altEngmtTime, altEngmtTime')
							.text();

					var waittime = $(xml).find('ns2\\:waitTime, waitTime')
							.text();
					waittime = Math.ceil(parseFloat(waittime) / 60);
					App.showAltEngmtOptions = false;
					altEngmtTime = parseInt(altEngmtTime);

					if (altEngmtTime > -1)
					{
						var day= new Date();
						var currTime = day.getTime();
						if (((currTime-App.waitTimeInQueueStart)/60000) >= altEngmtTime)
							App.showAltEngmtOptions = true;
					}

					var message = null;
					var valueToReplace = null;

					if (eGainLiveConfig.sessionStastics == 2){
						// This means we have to show queue depth to the customer.
						valueToReplace = queueDepth;
						message = L10N_QUEUE_LOAD_INFO;
					}
					else{
						// This means we have to show wait time to the customer.
						valueToReplace = waittime;
						message = L10N_WAIT_TIME;
						if (waittime == 1 && L10N_WAIT_TIME_SINGULAR) {
							message = L10N_WAIT_TIME_SINGULAR;
						}
					}
					//added code for onetag instrumentation
					if (App.connection.chatAwaitingAgentCaptured === false) {
					var EG_CALL_Q = window.EG_CALL_Q|| [];
					EG_CALL_Q.push(["send",
					                "cht",
					                "aev",
					                document.URL,
					                101,
					                {
						"EventName":"ChatAwaitingAgent",
						"EstimatedWaitTime":waittime
					                }]);
					App.connection.chatAwaitingAgentCaptured=true;
					}
					var altEngmtTimeout = 0;
					if (App.showAltEngmtOptions) {
						altEngmtTimeout = 1 * 1000;
					}
					message = message.replace(/TOREPLACE/, '<b>'+valueToReplace+'</b>');
					setTimeout(function(){App.altEngmtView.showQueueStatus(message);}, altEngmtTimeout);

					App.connection.pollQueueStatusTimer = setTimeout('App.connection.pollQueueStatus()',
								5 * 1000);

				}
			});

    },

    startQueueStatusPoll : function() {
		App.altEngmtView.updatePollingStatus(1);
		this.queueStatusPollStarted = true;
		delete App.altEngmtOptionsShown;
		this.pollQueueStatus();
    },

	clearPollQueueStatusTimer :function() {
		if(this.queueStatusPollStarted && !App.mailClicked){
			this.queueStatusPollStarted = false;
			if (this.pollQueueStatusTimer) {
				clearTimeout(this.pollQueueStatusTimer);
				this.pollQueueStatusTimer = null;
			}
			App.altEngmtView.showQueueStatus();
		}
	},
	getUrlParameter : function(name) {
		name = name.replace(/[\[]/, '\\\[').replace(/[\]]/, '\\\]');
		var results = new RegExp('[\\?&]' + name + '=([^&#]*)')
				.exec(window.location.href);
		return results ? results[1] : null;
	},

	getAvMode : function() {
		if (App.videoChatMaxEscalation > 4) {
			// No adjustment required.  Optimized path for most common case.
		}
		else if (App.videoChatMaxEscalation == 2) {
			if (this.avmode > 0)
				return TWO_WAY_AUDIO_MODE;
		}
		else if (App.videoChatMaxEscalation == 3) {
			if (this.avmode == TWO_WAY_AUDIO_VIDEO_MODE)
				return TWO_WAY_AUDIO_CUSTOMER_VIDEO_MODE;
			else if (this.avmode == TWO_WAY_AUDIO_AGENT_VIDEO_MODE)
				return TWO_WAY_AUDIO_MODE;
		}
		else if (App.videoChatMaxEscalation == 4) {
			if (this.avmode == TWO_WAY_AUDIO_VIDEO_MODE)
				return TWO_WAY_AUDIO_AGENT_VIDEO_MODE;
			else if (this.avmode == TWO_WAY_AUDIO_CUSTOMER_VIDEO_MODE)
				return TWO_WAY_AUDIO_MODE;
		}

		return this.avmode;
	},

    /**

        Sends the login information to the server.

    **/


    logout : function() {

		if (this._connection && this._connection.connected)
			this._connection.disconnect(L10N_CLOSE_MESSAGE);
    },


	stopVideoCall : function() {


    },

    pauseVideoCall : function() {


    },



	populateoptions : function(callback){
		var inputXML = "<attributesInfo languageCode='";
		inputXML += this.languageCode;
		inputXML += "' countryCode='";
		inputXML += this.countryCode;
		inputXML += "' xmlns = 'http://bindings.egain.com/chat' >";

		var flag = 0;
		var parameterDefinitions = eGainLiveConfig.loginParameters;
		for ( var i = 0; i < parameterDefinitions.length; i++) {

			var paramDef = parameterDefinitions[i];

			if (paramDef.fieldType == 3 || paramDef.fieldType == 4)
			{
				flag = 1;

				inputXML += '<attributeInfo>';

				inputXML += '<attributeName>';
				inputXML += paramDef.attributeName;
				inputXML += '</attributeName>';

				inputXML += '<objectName>';
				inputXML += paramDef.objectName;
				inputXML += '</objectName>';

				inputXML += '</attributeInfo>';
			}
		}

		inputXML += '</attributesInfo>';

		if (flag == 1) {

			$.ajax( {
				url : this.ATTRIBUTES_INFO_URL,
				type : 'POST',
				data : inputXML,
				contentType : 'text/XML',
				dataType : 'xml',
				success : function(xml) {
							callback(xml);
				}


			});
		}
		else
			callback();
	},



	/**
		Send system message.
		message - system message which will be displayed in agent console
	**/
	sendSystemMessage : function(message,command){
		return App.connection.sendNormalMessage(message, 'headline',command);
	},

	/**
		API to pause the connection manager. The pause time should be less than the MAX pause time
		configured at chat server. This API will send XMPP puase request to server and then will
		pause the request manager.
	**/
	pauseConnection : function () {
		var connection = this._connection;
		var body = connection._buildBody().attrs( { pause : eGainLiveConfig.chatPauseInSec });
		var req = new Strophe.Request(body.tree(), connection._onRequestStateChange.bind(
			connection).prependArg(connection._dataRecv.bind(connection)), body.tree()
			.getAttribute("rid"));
		connection._requests.push(req);
		connection._processRequest(this._connection._requests.length - 1);
		connection._throttledRequestHandler();
		connection.flush();
		connection.pause();
	},

	/**
		API to resume the connection manager. This API needs to be called after puaseConnection.
	**/
	resumeConnection : function () {
		this._connection.resume();
	}

});


