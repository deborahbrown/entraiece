/**

    SurveyView is a view object that is responsible
    for showing and submitting the survey form.

**/
App.SurveyView = Backbone.View.extend({

    className : 'eg-chat-survey',

    template : _.template($('#tpl-survey').html()),

    events : {
        'submit form' : 'onSubmitForm',
		'onload' : 'hideAddBar',
		'orientationchange' : 'hideAddBar',
		'keydown #wrapper' : 'keyPressEventHandlerScrollbar'
    },

    initialize : function() {
		App.chat_utilities_view = new App.ChatUtilitiesView();
        $(window).on('resize.egain.survey', _.bind(this.onWindowResize, this));
    },
    keyPressEventHandlerScrollbar: function(event){
    	var $form = this.$el.find('.form-fields');
        
        if (event.keyCode == 38) {
          // up arrow
       	 
        	$form.tinyscrollbar_updatescroll(40);
        	 event.preventDefault();
       	 
        } else if (event.keyCode == 40) {
         // down arrow
       	 
        	$form.tinyscrollbar_updatescroll(-40);
        	 event.preventDefault();
        }
      },

    render : function() {
		var height = eGainLiveConfig.chatSurveyWindowHeight;
		var isSurveyTextFilled = false;
		if($.browser.msie && $.browser.version == '7.0')
			height+=10;
		else if($.browser.msie && $.browser.version == '9.0')
			height-=10;
		else if($.browser.mozilla)
			height-=10;
		if(!(CONFIG_KNOWLEDGE_BASE_URL && CONFIG_KNOWLEDGE_BASE_URL.length>0)){	
			height+=53;
		}
		var d = getDimensions('');
		if(App.offerConstraints.size){
			d.width = App.offerConstraints.size.width;
			height = App.offerConstraints.size.height;
		}
		if(top && top.resizeTo)
			top.resizeTo(d.width,height);
        this.$el.html(this.template());
		if($.browser.msie && $.browser.version == "7.0")
			this.$el.find('.submit-section').height('50px');

 		$('#eg-chat-header a.closechat').attr('title',L10N_WINDOW_CLOSE_BUTTON);

        //Initialize the raty plugin.
        this.$('#answerability, #promptness, #would-suggest').raty({
            path : 'libs/img',
            hints: [L10N_POOR, L10N_FAIR, L10N_GOOD, L10N_VERY_GOOD, L10N_EXCELLENT]
        });
		$('#eg-chat-content').css('width','100%');
		$('#eg-chat-header').width('100%');
        $('#eg-chat-content').html(this.$el);
		this.$('.box').append(App.chat_utilities_view.el);
		App.chat_utilities_view.render({

            'showFont' : false,
            'showVideo' : false,
            'showAudio' : false,
            'showSaveTranscript' : true && !App.utils.isVisitorMobile(),
            'showChangeFont' : false,
            'showPrintTranscript' : true,
            'showDivider' : true && !App.utils.isVisitorMobile(),
			'showFontDivider':false,
            'showFaq' : false,
			'showOffRecord':false
        });
        $(".eg-chat-utilities .buttons a.divider").addClass("divider-toolbar");
		$(".eg-chat-utilities .buttons a.js-save").addClass("save-left");
		$(".eg-chat-utilities .buttons a.js-print").addClass("print-left");
        //Use tooltip as text
        $(".eg-chat-utilities .buttons a").each(function(){ $(this).text($(this).attr("title"));});

		$("#surveyText").focus(function() {
			if (!isSurveyTextFilled && $.trim($('#surveyText').val()) == L10N_SURVEY_ADDITIONAL)
				$('#surveyText').val('');
			return true;
		});
		
		$("#surveyText").blur(function() {
			if ($.trim($('#surveyText').val()) == ''){
				isSurveyTextFilled = false;
				$('#surveyText').val(L10N_SURVEY_ADDITIONAL);
			}else{
				isSurveyTextFilled = true;
			}
			$('#surveyText').attr("isSurveyTextFilled", isSurveyTextFilled);
		});
		
		if(!App.utils.isVisitorMobile()) {		
		 this.$el.find('.form-fields').tinyscrollbar({ sizethumb: App.scrollsize });
		}
		 if(App.utils.isVisitorMobile()) {
             this.scroll();
         }
		this.resize();
		if(App.utils.isVisitorIos())
		{App.utils.hideAddressBar();}
		
		if(navigator.userAgent.indexOf('Edge') >= 0){
			$(window).unbind('click');
			$('#surveyText').focus();
			$('#surveyText').blur();
		}
		$("#g_Notification").text(L10N_CHAT_ENDED_NOTIFICATION);
		

    },

     hideAddBar :function(){
	  if(App.utils.isVisitorIos())
	   App.utils.hideAddressBar();
	 },
	 scroll : function() {
	        if(App.utils.isVisitorMobile()) {
	            $.bind('touchmove',function(e){e.preventDefault();});
	            this.myscroll = new iScroll('scroller',{zoom:true, hideScrollbar:false,onScrollEnd:function(){if (this.y==this.maxScrollY){$("html, body").animate({scrollTop: $(document).height()}, 100)}}});
	        }
	    },

	   
    onSubmitForm : function(e) {

        e.preventDefault();

        // Get all the ratings of all the input fields.
        var answerability = $('#answerability').raty('score');
        var promptness = $('#promptness').raty('score');
        var would_suggest = $('#would-suggest').raty('score');
		
		// If no score is given i.e. score is undefined, set it to zero
		answerability = (typeof answerability === 'undefined') ? 0 : answerability;
		promptness = (typeof promptness === 'undefined') ? 0 : promptness;
		would_suggest = (typeof would_suggest === 'undefined') ? 0 : would_suggest;
		
		// Pass blank if the survey comment is not filled.
		// Anything except "true" i.e. "false", null, undefined should be treated as false.
		if(!($('#surveyText').attr("isSurveyTextFilled") == "true"))
			$('#surveyText').val('');
		
		if (eGainLiveConfig.maxMessageSize)
			surveyCommentSize = eGainLiveConfig.maxMessageSize;
		
		var regex =  /(<[\/\s]*script|<[\/\s]*img|<[\/\s]*iframe|javascript\s*:|on[a-zA-Z]*\s*=)/gi
		if(regex.test($('#surveyText').val()) ){
			alert(error_invalidInput);
			return; 
		}	

		if ($('#surveyText').val().length > surveyCommentSize) {
			if (L10N_SURVEY_VALIDATION_ERROR
					&& L10N_SURVEY_VALIDATION_ERROR.length > 0) {
				alert(L10N_SURVEY_VALIDATION_ERROR);
			}
			return;
		}

		var surveyURL = App.connection.SURVEY_URL;
		var inputXML = '<egainSurvey sid="' +App.connection.sid + '" xmlns="http://bindings.egain.com/chat">';
		inputXML += '<survey>' //
				+ '<question>' //
				+ xmlEscape(L10N_SURVEY_Q1) //
				+ '</question>' //
				+ '<answer>' //
				+ answerability //
				+ '</answer>' //
				+ '</survey>';
		inputXML += '<survey>' //
				+ '<question>' //
				+ xmlEscape(L10N_SURVEY_Q2)//
				+ '</question>' //
				+ '<answer>' //
				+ promptness //
				+ '</answer>' //
				+ '</survey>';
		inputXML += '<survey>' //
				+ '<question>' //
				+ xmlEscape(L10N_SURVEY_Q3) //
				+ '</question>' //
				+ '<answer>' //
				+ would_suggest //
				+ '</answer>' //
				+ '</survey>';
		inputXML += '<survey>' //
				+ '<question>surveyText</question>' //
				+ '<answer><![CDATA[' //
				+ xmlEscape($.trim(App.utils.masker.maskData($('#surveyText').val()))) //
				+ ']]></answer>' //
				+ '</survey>';
		inputXML += '</egainSurvey>';

		//Put the ajax code to submit survey results below here...
		$.ajax( {
			url : surveyURL,
			type : 'POST',
			contentType : 'text/XML',
			data : inputXML,
			processData : false,

			dataType : 'xml',
			success : function(data) {
			}
		});
		//unbind resize event of this window
		$(window).off('.egain.survey');

		//Initialize the ThanksView and renders it.
		App.thanksView = new App.ThanksView();
		//code added for onetag instrumentation

        App.thanksView.render();
    },

    onWindowResize : function(e) {

        this.resize();
    },

    resize : function() {

        var windowHeight = $(window).height();
		var windowWidth = $(window).width();
        var topOffset = this.$el.offset().top;

		this.$('.submit-section').width(windowWidth-App.pageOffset);
		
		if(App.utils.isVisitorMobile()){
			var $form = this.$el.find('.form-fields');
			this.$el.height('auto');
			var height;
			if(App.utils.isVisitorAndroid() && App.utils.hasMobile()){
				height = windowHeight-App.submitSectionHeight-topOffset-60;
			}
			else
			{
				height = $form.find('.viewport .overview').height();
			}
						
			$form.height(height);
			$form.find('.viewport').height(height);
			this.myscroll.refresh();
			
			$('#eg-chat-content').addClass('Nofloating-submit-section');
		}
		else{
			this.$el.height(windowHeight-App.submitSectionHeight-topOffset);
			var $form = this.$el.find('.form-fields');
			var formOffset = $form.offset().top;
			var submitTop = $('.submit-section').position().top;
			var height = submitTop-formOffset;
			$form.height(height);
			$form.find('.viewport').height(height);
			$form.find('.scrollbar').height(height);
			try{
				
				$form.tinyscrollbar_update('relative');
			}catch(error){}
		}
        console.log('offset', topOffset);
    }
	
});
