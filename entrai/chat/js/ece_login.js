/**
    
    LoginView is the view that encapsulates the login form.

**/
App.LoginView = Backbone.View.extend({
    
    className : 'eg-chat-login',
    urlbarshown :true,
	dropdownrefresh	:true,
    template : _.template($('#tpl-login').html()),

    events : {
        
        'submit form' : 'onFormSubmit',
        'focus input[type=text], textarea, select' : 'onInputFocus',
		'keydown textarea' : 'onEnter',
        'blur input[type=text], textarea, select' : 'onInputBlur',
		'onload' : 'hideAddBar',
		'orientationchange' : 'hideAddBar',
		'keydown #wrapper' : 'keyPressEventHandlerScrollbar',

		
	},
	 keyPressEventHandlerScrollbar: function(event){
		 if(!((event.target.tagName.toUpperCase()=='INPUT')||(event.target.tagName.toUpperCase()=='TEXTAREA')))
		 {
		 var $form = this.$el.find('.form-fields');
	        
	        if (event.keyCode == 38) {
	          // up arrow
	        	$form.tinyscrollbar_updatescroll(40);
	        	 event.preventDefault();
	        
	        } else if (event.keyCode == 40) {
	         // down arrow
	        	$form.tinyscrollbar_updatescroll(-40);
	        	 event.preventDefault();
	        	
	        }
		 }
	      },
	
    initialize : function() {

        this._loginParameters = eGainLiveConfig.loginParameters;

        console.log(this._loginParameters);
        
        this.validator = new App.LoginValidator();

        $(window).on('resize.egain.login', _.bind(this.onWindowResize, this));
    },
    
    render : function(callback) {
        var html = this.template({param:this._loginParameters, window:window})
        var postedChatParameters = getPostedChatParameters();
        this.$el.html(html);
		if($.browser.msie && $.browser.version == "7.0")
			this.$el.find('.submit-section').height('50px');
		var _this =this;
		var useChatAttributes  = getUrlParameter('postChatAttributes');
		this.populateoptions(function(){
			$('#eg-chat-content').html(App.login.el);
			_this.$formWrap= _this.$el.find('.form-fields');
			if(!App.utils.isVisitorMobile()){
				_this.$formWrap.tinyscrollbar({ sizethumb: App.scrollsize });				
			}
			// Add multiselect function to dropdowns
			
			this.$(".multi").multiselect({
					selectedText : '',
					selectedList : 7

				});
				this.$(".single").multiselect({ 			
					noneSelectedText: 'Select an option',
					multiple : false,
					header : false,
					selectedText : '',
					selectedList : 1			
				});	
				this.$(".multi").each( function() {			
					$(this).parent().css({'padding' : '0px'});	// To remove padding from input-wrap-sub					
					
					$(this).bind("multiselectopen", function() {																	
						$(this).multiselect('getButton').css({'color' : '#000'}).css({'background' : '#FFFFFF'})
							.css({'-moz-box-shadow' : 'inset 0px 0px 1px 1px #bae7f5'})
							.css({'-webkit-box-shadow' : 'inset 0px 0px 1px 1px #bae7f5'})
							.css({'box-shadow' : 'inset 0px 0px 1px 1px #bae7f5'});
						// Add scrolling within dropdown menu
						if(!App.utils.isVisitorMobile()){
							$(this).multiselect('getMenu').find('#multiScrolling').tinyscrollbar({sizethumb: 30 });							
						}
					});
					$(this).bind("multiselectclose", function() {
						var inputName = $(this).attr('name');
						var param = _this._getInputParam(inputName);
						var errors = [];
						var inputValue = $(this).val() || '';
						if (param.required == '1' && inputValue.length == 0) {
							errors.push(error_select);
						}
															
						//Show errors if there are some
						if(errors.length) {            
							console.log('errors', errors);
							_this._showInputValidationErrors(inputName, errors);

						} else {
							_this._showInputValidatedSignal(inputName);
						}
					});
					
				});
				this.$(".single").each( function() {			
					$(this).parent().css({'padding' : '0px'}); // To remove padding from input-wrap-sub
					$(this).bind("multiselectopen", function() {										
						$(this).multiselect('getButton').css({'color' : '#000'})
						.css({'background' : '#FFFFFF'})
						.css({'-moz-box-shadow' : 'inset 0px 0px 1px 1px #bae7f5'})
						.css({'-webkit-box-shadow' : 'inset 0px 0px 1px 1px #bae7f5'})
						.css({'box-shadow' : 'inset 0px 0px 1px 1px #bae7f5'});
						// Add scrolling within dropdown menu
						if(!App.utils.isVisitorMobile()){
							$(this).multiselect('getMenu').find('#singleScrolling').tinyscrollbar({sizethumb: 30 });							
						}
					});
				});				
			//If parameters are posted to the templates, then we populate them in the appropriate fields.
			if( useChatAttributes && useChatAttributes != "false" && postedChatParameters) {
				var parameterDefinitions = eGainLiveConfig.loginParameters;
				var errors = {}, attributeName, attributeValue;
				for ( var i = 0; i < parameterDefinitions.length; i++) {
					var paramDef = parameterDefinitions[i];
				    var fieldName = '';
				    if (typeof paramDef.egainAttributeName != 'undefined' && paramDef.egainAttributeName != 'null')
					    fieldName = paramDef.egainAttributeName;
				    if (fieldName == '')
					    fieldName = 'fieldname_' + (i + 1);
					var attributeName = paramDef.attributeName; 
					attributeValue = postedChatParameters[fieldName];
					if (attributeValue)
						errors[attributeName] = _this.fillLoginFormParamter(attributeName, attributeValue, paramDef);
				}
			}
			 if(App.utils.isVisitorMobile()) {
	             _this.scroll();
	         }
			App.login.resize();
			if(callback) callback();
		});
			 
    },
	getSubmitTop : function(){	
		// For use in multiselect.js open menu eventhandler, to calculate height of drop down menu
		var submitTop = this.$el.find('.submit-section').offset().top;		
		return submitTop;
	},
    hideAddBar : function(){
	if(App.utils.isVisitorIos())
    App.utils.hideAddressBar();
   },
  	populateoptions : function(callback){
		App.connection.populateoptions(function(xml){
			/*
				<attributesInfo languageCode="en" countryCode="US" xmlns:ns2="com/egain/live/framework/bosh/gen" xmlns:ns4="urn:ietf:params:xml:ns:xmpp-stanzas" xmlns:ns3="jabber:client">
				    <ns2:attributeInfo>
				        <ns2:attributeName>account_type</ns2:attributeName>
				        <ns2:objectName>casemgmt::activity_data</ns2:objectName>
				        <ns2:internalValue>bank_checking,bank_saving,bank_cc,bank_cd</ns2:internalValue>
				        <ns2:defaultValue>bank_saving</ns2:defaultValue>
				        <ns2:displayValue>Checking,Saving,Credit Card,CD</ns2:displayValue>
				    </ns2:attributeInfo>
				</attributesInfo>
				*/
				try{
					var attributeInfoList = $(xml).find('ns2\\:attributeInfo, attributeInfo');
					for (var n = 0; n < attributeInfoList.length; n++) {
						var attributeInfo = attributeInfoList[n];
						var attributeName = $(attributeInfo).find('ns2\\:attributeName, attributeName').text();
						var objectName = $(attributeInfo).find('ns2\\:objectName, objectName').text();
						var internalValue = $(attributeInfo).find('ns2\\:internalValue, internalValue').text();
						var defaultValue = $(attributeInfo).find('ns2\\:defaultValue, defaultValue').text();
						var displayValue = $(attributeInfo).find('ns2\\:displayValue, displayValue').text();
						var parameterDefinitions = eGainLiveConfig.loginParameters;
						for ( var ij = 0; ij < parameterDefinitions.length; ij++) {

							var paramDef = parameterDefinitions[ij];
							// Field Type 3 - Single Select DropDown, 4 - Multi Select DropDown
							if ( (paramDef.fieldType == 3 || paramDef.fieldType == 4) && paramDef.attributeName == attributeName && paramDef.objectName == objectName){

								var inputOptions = "";
								var internalValArray = internalValue.split(',');
								var displayValArray = displayValue.split(',');

								if (internalValArray.length == displayValArray.length) {

									for ( var i = 0; i < internalValArray.length; i++) {

										inputOptions += "<option value='";
										inputOptions += internalValArray[i];
										inputOptions += "'";

										if (internalValArray[i] == defaultValue)
											inputOptions += "selected='true'";

										inputOptions += ">";
										inputOptions += (JSON && JSON.parse)?JSON.parse('"' + displayValArray[i] + '"'):displayValArray[i];
										inputOptions += "</option>";
									}
								}
								App.login.$el.find("#" + paramDef.attributeName).append(inputOptions);
							}
						}
					}
				}
				catch(error){}
				callback();
		
		});
	},

    onInputFocus : function(e) {

        var $input = $(e.target);
        
        if(!$input.attr('typed')) {

            $input.val('');
			$input.css('color','#000');
			$(e.target.parentElement).css("background-color","#FFFFFF")
			$(e.target.parentElement).css('-moz-box-shadow' ,'inset 0px 0px 1px 1px #bae7f5' );
			$(e.target.parentElement).css('-webkit-box-shadow' ,'inset 0px 0px 1px 1px #bae7f5' );
			$(e.target.parentElement).css('box-shadow' ,'inset 0px 0px 1px 1px #bae7f5' );
            $input.attr('typed', true);
        } 
    },

    onWindowResize : function() {

        this.resize();
		setTimeout("App.login.refreshDropDowns()", 10);	
    },
	
	refreshDropDowns : function() {
		$(".multi").multiselect('refresh');
		$(".single").multiselect('refresh');
		
		// To close menu on resize
		$(".multi").multiselect("close");
		$(".single").multiselect("close");
	},
	scroll : function() {
        if(App.utils.isVisitorMobile()) {
           $.bind('touchmove',function(e){e.preventDefault();});
            this.myscroll = new iScroll('scroller',{zoom:true, hideScrollbar:false,onScrollEnd:function(){if (this.y==this.maxScrollY){$("html, body").animate({scrollTop: $(document).height()}, 100)}}});
        }
    },
    resize : function() {
		var width = $(window).width() ;
		var windowHeight = $(window).height() ;
		var topOffset = this.$el.offset().top;
        console.log('offset', topOffset);
       
        if(	App.chat_sidebar_view && App.chat_sidebar_view.isVisible()){
			
			width = width/2 - App.pageOffset;
			$('#eg-chat-content').css('width',width);
			$('#eg-chat-header').width(width);
			this.$el.find('.submit-section').width(width);
		}else{
			this.$el.find('.submit-section').width(width-App.pageOffset);
		}
		if(!App.utils.isVisitorMobile()){
			this.$el.height(windowHeight - topOffset -App.submitSectionHeight);
			var $form = this.$formWrap;
			var formOffset = $form.offset().top;
			var submitTop = this.$el.find('.submit-section').position().top;
			var height = submitTop-formOffset;
			$form.height(height);
			//bottom padding 28px for login form
			$form.find('.viewport').height(height-28);
			$form.find('.scrollbar').height(height-28);
			try{
				$form.tinyscrollbar_update('relative');
			}catch(error){}
		}
		if(App.utils.isVisitorMobile()){
			
			
			var $form = this.$formWrap;
			this.$el.height('auto');
			var height;
			if(App.utils.isVisitorAndroid() && App.utils.hasMobile()){
				height = windowHeight - topOffset -App.submitSectionHeight-48;
				}
			else
				{
				height =$form.find('.viewport .overview').height();
				
				}
			 
			
			$form.height(height);
			$form.find('.viewport').height(height);
			$form.find('.scrollbar').height(height);

                this.myscroll.refresh();
				
			
			$('#eg-chat-content').addClass('Nofloating-submit-section');
			if(this.urlbarshown && App.utils.isVisitorIos())
			{App.utils.hideAddressBar(); 
			 this.urlbarshown=false;
            }			
		}
		if(this.dropdownrefresh) {
			setTimeout("App.login.refreshDropDowns()", 100);
			this.dropdownrefresh = false;
		}
    },

    
    onInputBlur : function(e) {

        //Let's validate the input when it's blurred.
        var $input = $(e.target);
        var value = $input.val();
        var inputName = $input.attr('name');

        var errors = this._validateInputElement(e.target);

        //Show errors if there are some
        if(errors.length) {
            console.log('errors', errors);
            this._showInputValidationErrors(inputName, errors);
            for (var i=0; i<errors.length;i++) {
            	$("#g_Notification").text(errors[i]);
            }
        } else {
            
            this._showInputValidatedSignal(inputName);
        }
    },

    _getInputParam : function(name) {

        return _.find(this._loginParameters, function(param) { 
            
            return param.attributeName == name;
        });
    },

    onFormSubmit : function(e) {
        //to capture user's first message time
    	App.connection.lastRequestMessageTime = Date.now();		
        
        e.preventDefault();

        var $form = $(e.target);
        var _this = this;

        //Get all the input elements references.
        var $inputs = $form.find("input[type=text], textarea, "+
                                 "select");
        
        var errors = {};
        //`formValues` is a map containing all the values of the inputs, with the
        //key being the attributeName of the form inputs.
        var formValues = {};

        //Go through each $input element.
        $inputs.each(function() {
            
            var name = $(this).attr('name');
            
            //Get the errors for the corresponding element.
            errors[name] = _this._validateInputElement(this);

            formValues[name] = $(this).val();
        });
        
        var isDataMasked =false;
        var origStr = formValues['subject'];

        if(typeof origStr != "undefined")
        {

        	formValues['subject'] = App.utils.masker.maskData($form.find('textarea[name="subject"]').val());
        	if(formValues['subject'] != origStr)
        		isDataMasked=true;		

        	//Do html encoding of the subject
        	formValues['subject'] = App.utils.htmlEncode(formValues['subject']);
        }
        //if there are still any errors, then show the errors and don't login.
        if(this._errorsArePresent(errors)) {
            
            this._showFormValidationErrors(errors);

        } else {
            //If not, then let's login and submit the initial message.
			this.login(formValues);
        }
    },
	
	login : function(formValues){
		 //code added for ChatRequested event
        var EG_CALL_Q = window.EG_CALL_Q || [];
        EG_CALL_Q.push([ "send", "cht", "aev", document.URL, 101, {
        	"EventName" : "ChatRequested",
        	"Name" : formValues['name'],
        	"Email" : formValues['email_address'],
        	"PhoneNumber" : formValues['phone_number'],
        	"Question" : formValues['subject']
        } ]); 
		
		formValues['name'] = formValues['full_name'] ;
		if(!formValues['name'])
		{
			formValues['name'] = formValues['first_name'];
			formValues['name'] = formValues['name'] ?  (formValues['last_name'] ? ( formValues['first_name'] + ' ' +  formValues['last_name']) : formValues['name'] ): formValues['last_name']  ;
		}
		formValues['name'] = formValues['name'] || L10N_CUSTOMER;
		formValues['email_address']='someone@something.com';
		$(window).off('.egain.login');
		App.session.login(formValues);

	},
		
	autologin : function() {
		var _this = this;
		var postedChatParameters = getPostedChatParameters();
		if (postedChatParameters)
			eGainLiveConfig.samlResponse = postedChatParameters['SAMLResponse'];
		this.$el.hide();
		this.render(function(){
			var parameterDefinitions = eGainLiveConfig.loginParameters;
			var formValues = {};
			var errors = {};
			var useChatAttributes  = getUrlParameter('postChatAttributes');
			//set field value in login form
			for ( var i = 0; i < parameterDefinitions.length; i++) {
				var paramDef = parameterDefinitions[i];
				var fieldName = '';
				if (typeof paramDef.egainAttributeName != 'undefined' && paramDef.egainAttributeName != 'null')
					fieldName = paramDef.egainAttributeName;
				if (fieldName == '')
					fieldName = 'fieldname_' + (i + 1);
				var attributeName = paramDef.attributeName; 
				if (getUrlParameter(fieldName))
					formValues[attributeName]=decodeURI(getUrlParameter(fieldName));
				else if( useChatAttributes && useChatAttributes != "false" )
					formValues[attributeName]=postedChatParameters[fieldName];
				else
					formValues[attributeName] = "";
				errors[attributeName] = _this.fillLoginFormParamter(attributeName, formValues[attributeName], paramDef);
			}		
			var $inputs = _this.$el.find("input[type=text], textarea");
			
			//Go through each $input element.
			$inputs.each(function() {
				var name = $(this).attr('name');
				//Get the errors for the corresponding element.
				if(!errors[name])
					errors[name] = _this._validateInputElement(this);
			});
			
			//if there are still any errors, then show the errors and don't login.
			if(_this._errorsArePresent(errors)) {
				_this.$el.show();
				_this.resize();
				_this._showFormValidationErrors(errors);

			} else {
				//If not, then let's login and submit the initial message.			
				_this.login(formValues);         
			}
		});
	},

	//Return the error or empty string if successful
	fillLoginFormParamter: function( attributeName, attributeValue, paramDef) {
		var $element = $('[name='+attributeName+']');
		var error = "";
		//set field status to changed so validation does not fail and css is updated
		this.onInputFocus({target:$element});
		if(paramDef.fieldType != '3' && paramDef.fieldType != '4') //for text fields
			$element.val(attributeValue);
		else if(attributeValue || (paramDef.required != '0'))
		{
			//update css for multiselect and single select drop downs
			$element.multiselect('getButton').css({'color' : '#000'}).css({'background' : '#FFFFFF'})
					.css({'-moz-box-shadow' : 'inset 0px 0px 1px 1px #bae7f5'})
					.css({'-webkit-box-shadow' : 'inset 0px 0px 1px 1px #bae7f5'})
					.css({'box-shadow' : 'inset 0px 0px 1px 1px #bae7f5'});

			//for single select dropdown check if it is a valid option
			if (paramDef.fieldType == '3') {
				if ( $("#"+attributeName+" option[value='"+attributeValue+"']").length > 0 ){
					$element.val(attributeValue);
				}
				else
				{
					//otherwise set error message
					error = [];
					error.push(error_single_select);
				}
			}
			//for multi select dropdown check if specified options are valid
			else if (paramDef.fieldType == '4') {
				this.validateMultiSelectOption(attributeValue, $element);
				// If this is a required field then at least one valid value should be specified
				var elementValue = $element.val() || '';
				if ((paramDef.required != '0') && (elementValue.length == 0)) {
					error = [];
					error.push(error_select);
				}
			}
		}
		return error;
	},

	validateMultiSelectOption : function(values, $element) {
		var elementValues = values.split(",");
		var validatedValues = [];
		
		for ( var i = 0; i < elementValues.length; i++) {
			var val = elementValues[i];			
			$element.children('option').each( function() {
				if (val == $(this).val()) {
					validatedValues.push(val);
				}
			});
			
			
		}
		$element.val(validatedValues);		
	},
	
    _validateInputElement : function(input) {

        var $input = $(input);
        var name = $input.attr('name');
        var inputValue = $input.val() || "";
        var param = this._getInputParam(name);
        var _errors = [];
		
		if (param.secureAttribute == '1') {
			return _errors;
		}
		
		if(!$input.attr('typed') && inputValue == window[param.paramName]){
			$input.val('');
			inputValue ='';
		}
		//checking for name , email, etc.
		if(inputValue.length > 0){
			var err = this.validator.getInputValidationErrors(name,inputValue);
			if(err.length)
				return err;
		}

        //Check emptiness / char minLength
        if((param.minLength >= 1 || param.required == '1')
            && inputValue.length == 0) {

            _errors.push(error_input);
        }
        //Check if the length is valid.
        else if(!(parseInt(param.minLength) <= inputValue.length)) {

            _errors.push(error_minLength.replace('{0}', param.minLength ));
        }
        
        //check count maxLength
        if(!(inputValue.length <= parseInt(param.maxLength))) {

            _errors.push(error_maxLength.replace('{0}', param.maxLength ));
        }
    
        //Check if the inputValue is valid based on the validationString.
        if(!this._validateInputValue(inputValue, param.validationString)) {

            _errors.push(error_invalidInput);         
        }
        
        //Changes for xss issue
        if(this._validateCrossSiteScripting(inputValue)){
		_errors.push(error_invalidInput);         		
	}

        return _errors;
    },

    _errorsArePresent : function(errors) {
        
        for(var key in errors) {
            
            if(errors[key].length > 0)
                return true;
        }

        return false;
    },

    _validateInputValue : function(inputValue, regexString) {

        return new RegExp(regexString, "g").test(inputValue);
    },
     /**
	Check if cross site scripting is used and prevent it
	return false if check passed i.e. no cross site script used
	else return true.
    **/
	_validateCrossSiteScripting : function(inputvalue){
		var regex =  /(<[\/\s]*script|<[\/\s]*img|<[\/\s]*iframe|javascript\s*:|on[a-zA-Z]*\s*=)/gi	
		return regex.test(inputvalue);	
	},
    
    /**

        Show the errors to the input wrap.
        
        @inputName - string - the name of the input validated
        @errors - arrays of string - the error messages of the input.

    **/
    _showInputValidationErrors : function(inputName, errors) {
        

        var $inputWrap = this
                        .$('.input-wrap[data-input-name="'+ inputName +'"]'); 

        var $errors = this
                        .$('.errors[error-input-name="'+ inputName +'"]');

       /*$inputWrap.find('.input-wrap-sub').css('background-color','#FEEDC1');*/
       $inputWrap.find('.input-wrap-sub').css('border-color' ,'#39BFC2' );
       $inputWrap.find('.input-wrap-sub').css('-moz-box-shadow' ,'inset 0px 0px 2px 1px #39BFC2' );
       $inputWrap.find('.input-wrap-sub').css('-webkit-box-shadow' ,'inset 0px 0px 2px 1px #39BFC2' );
       $inputWrap.find('.input-wrap-sub').css('box-shadow' ,'inset 0px 0px 2px 1px #39BFC2' );

       if(errors.length) {

            $inputWrap.find('.checkmark').hide();
            $inputWrap.find('.errormark').show();
			if (($inputWrap.find('.multi').length > 0) || ($inputWrap.find('.single').length > 0)){
				$inputWrap.find('.errormark').css('top','30px').css('right','32px');
			}
            $errors.empty();
            $errors.append(errors[0]);
        }
        
		if($.browser.msie && $.browser.version == '7.0'){
			$errors.css('top','0');
			$inputWrap.find('.errormark').css('top','27px');
		}
    },

    _showInputValidatedSignal : function(inputName) {

        var $inputWrap = this
                        .$('.input-wrap[data-input-name="'+ inputName +'"]'); 


		$inputWrap.find('.input-wrap-sub').css('background-color','#FFFFFF');
		$inputWrap.find('.input-wrap-sub').css('border-color' ,'#B8B8B8 #B8B8B8 #B8B8B8 #B8B8B8' );
		$inputWrap.find('.input-wrap-sub').css('-moz-box-shadow' ,'inset 0px 0px 1px 1px #bae7f5' );
		$inputWrap.find('.input-wrap-sub').css('-webkit-box-shadow' ,'inset 0px 0px 1px 1px #bae7f5' );
		$inputWrap.find('.input-wrap-sub').css('box-shadow' ,'inset 0px 0px 1px 1px #bae7f5' );
		$inputWrap.find('.errormark').hide();
		$inputWrap.find('.errors').empty();
		if($.browser.msie && $.browser.version == '7.0'){
			$inputWrap.find('.checkmark').css('top','29px');
		}
		if (($inputWrap.find('.multi').length == 0) && ($inputWrap.find('.single').length == 0)){
			$inputWrap.find('.checkmark').show();
		}
    },

    _showFormValidationErrors : function(formErrors) {

        _.each(_.keys(formErrors), function(inputName) {
        
            var errors = formErrors[inputName];
        
            if(errors.length) {

                this._showInputValidationErrors(inputName,
                                                formErrors[inputName]);
            } else {

                this._showInputValidatedSignal(inputName);
            }

        }, this);
    },
	
	onEnter : function(e) {
		if(e.keyCode == 13 && !e.shiftKey){
			$('input[type=submit]').click();
			return false;
		}
    }
});
