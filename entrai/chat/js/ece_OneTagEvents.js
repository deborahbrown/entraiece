/**
 * This js file has been created in order to push chat events to OneTag server
 */

App.OneTagEvents = Backbone.Model.extend({}, {
	regexForImg : /<\s*img[^>]+>/,

	pushChatAgentMsg : function(messageHtml) {

		var curTime = Date.now();
		var agentSideTimeElapsed = curTime
				- App.connection.lastRequestMessageTime;

		var messageToBePushed = messageHtml;
		if ((App.OneTagEvents.regexForImg).test(messageHtml)) {
			var el = document.createElement('el');
			el.innerHTML = messageHtml;
			var smileys = el.getElementsByTagName('img');
			if (smileys) {
				var noOfSmileys = smileys.length;
				for (var i = 0; i < noOfSmileys; i++) {
					var abridgedImg = "&lt;img alt='"
							+ (smileys[0].getAttribute('alt') || '') + "'&gt;";
					smileys[0].outerHTML = abridgedImg;
				}
				messageToBePushed = el.innerHTML;
			}
		}
		messageToBePushed = $.trim($("<div/>").html(messageToBePushed).text());
		var EG_CALL_Q = window.EG_CALL_Q || [];
		EG_CALL_Q.push([ "send", "cht", "uac", document.URL, 101, {
			"EventName" : "ChatAgentMsg",
			"Time-to-respond" : agentSideTimeElapsed,
			"Message" : messageToBePushed
		} ]);

	},

	pushChatCustomerMsg : function(messageHtml) {

		var currTime = Date.now();
		if (App.connection.lastResponseMessageTime !== -1)
			var customerSideTimeElapsed = currTime
					- App.connection.lastResponseMessageTime;
		else
			customerSideTimeElapsed = -1;
		var messageToBePushed = messageHtml;
		if ((App.OneTagEvents.regexForImg).test(messageHtml)) {
			var el = document.createElement('el');
			el.innerHTML = messageHtml;
			var smileys = el.getElementsByTagName('img');
			if (smileys) {
				var noOfSmileys = smileys.length;
				for (var i = 0; i < noOfSmileys; i++) {
					var abridgedImg = "&lt;img alt='"
							+ (smileys[0].getAttribute('alt') || '') + "'&gt;";
					smileys[0].outerHTML = abridgedImg;
				}
				messageToBePushed = el.innerHTML;
			}
		}
		messageToBePushed = $.trim($("<div/>").html(messageToBePushed).text());
		var EG_CALL_Q = window.EG_CALL_Q || [];
		EG_CALL_Q.push([ "send", "cht", "uac", document.URL, 101, {
			"EventName" : "ChatCustomerMsg",
			"Time-to-respond" : customerSideTimeElapsed,
			"Message" : messageToBePushed
		} ]);
	}

});