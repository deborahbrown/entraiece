/**

 Editor is a View object that wraps around a CKEditor instance.
 It listens for the editor's events and do things such as
 submitting user messages to the application's messenger.

 **/
App.Editor = Backbone.View.extend({

    className : 'eg-chat-editor',

    template : _.template($('#tpl-editor').html()),

    events : {

        'click input[type=button]' : 'onFormSubmit',
        'keyup textarea' : 'onEditorTyping',
        'click #off-record-button' : 'toggleOffRecord',
        'click #submit-chat-button' : 'sendChat',
		'click textarea' : 'clickHandler'
    },
	
	clickHandler: function(event){
        if(App.utils.hasMobile()){
            this.resizeForKeyboard(event);			
		}
        event.preventDefault();
	},

    resizeForKeyboard: function(event){
        if(window.innerHeight < window.innerWidth)
        {
            App.chat_stream_view.resize(100);
        }
        else
        {
            if(App.utils.isVisitorIosPhone()){
                App.chat_stream_view.resize(140);
            }
            else
                App.chat_stream_view.resize(window.innerHeight*0.25);
        }
    },

    initialize : function() {

    },

    render : function() {

        var _this = this;

        //Render
        this.$el.html(this.template());
        var ie7flag = $.browser.msie && $.browser.version == "7.0";
        if(ie7flag)
            this.$el.find('.submit-section').height('50px');
        //Create ckeditor if the visitor is not mobile.
        //(mobile visitors will just get plain <textarea>
        if(!App.utils.isVisitorMobile() && eGainLiveConfig.useTextEditor == 0 && !ie7flag) {

            //The ckEditor config.
            var ckEditorConfig = {

                startupFocus : true,
                scayt_autoStartup : false,
                enterMode : CKEDITOR.ENTER_BR,
                shiftEnterMode : CKEDITOR.ENTER_BR,
                toolbar : 'Basic',
                toolbarCanCollapse : false,
                height : 55,
                removePlugins : 'editingblock,undo,elementspath,enterkey,wysiwyg',
                resize_enabled : false,
                extraPlugins : 'smiley',
                removeButtons : '',
                contentsCss : 'chat/css/editorcustomization.css',
                forceCustomDomain: true,
                skin : 'kama',
                /*PSENG-18761 - Right alignment of cursor in textbox*/
				contentsLanguage : App.connection.languageCode,
				language : App.connection.languageCode,
				defaultLanguage : 'en',
				/*--------*/
                toolbar_Basic : [
                    {name:'styles', items:['-','-','-']}
                ]
            };

            //Insert smiley in the editor if eGainLiveConfig said so.
            if(eGainLiveConfig.showSmileyTool) {

                ckEditorConfig.toolbar_Basic.push({
                    name:'insert',
                    items:['Smiley']
                });
            }

            this.ckeditor = CKEDITOR.replace('editor', ckEditorConfig);

            this.ckeditor.on('focus', _.bind(this.onEditorFocus, this));
            this.ckeditor.on('blur', _.bind(this.onEditorBlur, this));
            this.ckeditor.on('key', _.bind(this.chatEditorKeystrokeHandler,this));
            this.ckeditor.on('key', _.bind(this.onEditorTyping, this));
            this.ckeditor.on('cut', _.bind(this.onEditorTyping, this));
            this.ckeditor.on('paste', _.bind(this.onEditorTyping, this));
            this.ckeditor.on('focus', _.bind(this.onEditorTyping, this));
            this.ckeditor.on('focus', _.bind(stopTitleAlert, this));
            this.ckeditor.on('key', _.bind(stopTitleAlert, this));
            this.ckeditor.on('click', _.bind(stopTitleAlert, this));
            this.ckeditor.on('insertElement', _.bind(this.onEditorTyping, this));

            //On editor instance ready, we'll show the whole editor view.
            this.ckeditor.on('instanceReady', function() {
                _this.$el.show();
				if(eGainLiveConfig.showSmileyTool) {
					var div = $( "<div class='fontDivider'> </div>" );
					$('.cke_toolbar').filter(":first").after(div);
				}

                _this.$el.find('.cke_wrapper').css({
                    'padding-bottom':'0',
                    'border-radius': '0',
                    '-webkit-border-radius': '0',
                    '-moz-border-radius': '0'
                });

                // Adding the attachment button on the ck editor tool bar
                //$("#cke_1_toolbox").append("<div id='attachmentIcon'><div class='fontDivider'> </div><a href='#openModal' class='attachment-icon' title='Attachment'></a></div>");
		App.editor.displayAttachmentIcon();
		App.editor.showHideAttachmentIcon(App.isChatAttachmentEnabled, App.isOffRecord);
                // Focus on ckEditor as soon as it loads
                window.setTimeout(function(){
                    App.editor.ckeditor.focus();
                }, 1);
            });

        } else {
            $('textarea').wrap($('<div>').addClass('textareawrap'));
            this.$el.find('textarea').keypress(this.chatEditorKeystrokeHandler)
                .blur(this.onEditorBlur)
                .focus(this.onEditorFocus)
                .focus(this.onEditorTyping)
                .focus(stopTitleAlert)
                .keypress(stopTitleAlert)
                .click(stopTitleAlert)
                .bind('cut', this.onEditorTyping)
                .bind('paste', this.onEditorTyping);
            this.$el.show();
            this.$el.find('textarea').focus();
            this.$el.find('#typing_status').addClass('texteditor');
        }
        if(App.utils.isVisitorMobile()){
        	this.$el.find('textarea').blur();
            this.$el.find('textarea').height(52);
            this.$el.find('.submit-section').css('position','static');
            $('#eg-chat-content').removeClass('Nofloating-submit-section');
            $('#eg-chat-content').addClass('freefloating-submit-section');
        }
        if(!App.utils.isVisitorMobile()){
            App.chat_wrap_view.resize();
            App.chat_stream_view.resize();
            if(this.ckeditor)
                this.ckeditor.focus();
            else
                this.$el.find('textarea').focus();
        }
    },

    chatEditorKeystrokeHandler : function(event){
        if (customerIsTyping) {
            clearTimeout(customerIsTyping);
        }
        else {
            App.connection.sendTypingMessage();
        }
        customerIsTyping = setTimeout('App.connection.sendNotTypingMessage();', eGainLiveConfig.customerNoTypingTimeout);

        if ((event.data && event.data.keyCode == 13 )|| ( event.keyCode && event.keyCode == 13)) {
            if (navigator.userAgent.indexOf('Opera') >= 0) {
                setTimeout('App.editor.sendChat()', 10);
            }
            else {
                App.editor.sendChat();
            }

            return App.editor.chatEditorCancelEvent(event);
        }
    },

    chatEditorCancelEvent : function(evt) {
        evt.cancelBubble = true;
        if (typeof evt.cancel == 'function') evt.cancel();
        else evt.cancel = true;
        evt.returnValue = false;
        if (evt.preventDefault) evt.preventDefault();
        if (evt.stopPropagation) evt.stopPropagation();
        return false;
    },

    //Callback that will be called when the user submitted the message.
    onFormSubmit : function(e) {

        e.preventDefault();
        this.sendChat();
    },

    onEditorFocus : function() {
        App.editor.isfocus = true;
    },

    onEditorBlur : function() {
        App.editor.isfocus = false;
        if(App.utils.isVisitorIosPhone()){
            App.chat_stream_view.resize();
        }
    },

    onEditorSave : function(e) {
        App.editor.chatEditorKeystrokeHandler(e);
    },

    sendChat : function(){

        var message_str = '';
        var text_message_str = '';
        if( this.ckeditor)	{
            message_str  = this.ckeditor.getData();
            text_message_str = $('<div>').html(message_str).text();
        }
        else{
            message_str = App.utils.htmlEncode(this.$el.find('textarea').val());
            text_message_str = message_str;
        }

        if(message_str == '')
            return;

        var origHTMLMsg = message_str;
        if(!App.offRecord)
            message_str = App.utils.masker.maskData(message_str, text_message_str);

        //Submit the message string to the messenger

 
            
            App.messenger.submitMessageString(message_str,'customer',App.session.get('name'));
            if( this.ckeditor)	{
				
                window.setTimeout(function() {
                    try{
                        App.editor.ckeditor.setData('', function(){setTimeout(function(){App.editor.ckeditor.focus();},10);});
                    }catch(err){}
                }, 1);
            }
            else{
				
                this.$el.find('textarea').val('');
            }
			App.connection.sendNormalMessage(message_str);
        
        if(origHTMLMsg!=message_str)/*send system message if data is masked*/
        {
            if(App.connection.sendSystemMessage(L10N_SENSITIVE_DATA_MASKED,'headline'))
                App.messenger.showNotification(L10N_SENSITIVE_DATA_MASKED,'headline');
        }
        if(!App.utils.isVisitorMobile()){
            if(this.ckeditor)
                setTimeout(function(){App.editor.ckeditor.focus();},10);
            else
                this.$el.find('textarea').focus();
        }
        else{
            this.$el.find('textarea').blur();
            $('.window-view').focus();
        }
        this.onEditorTyping();
    },

    onEditorTyping : function(e){
        //timeout so that the length is calculated after operation is complete

        setTimeout(function(){
            try{
                var maxCharacterCount = eGainLiveConfig.maxMessageSize;
                var currentCount = 0;

                if(App.editor.ckeditor)
                {
                    // Below method escapes '\n' while counting the message length, so replacing all '\n' with 'carriage return'
                    // to get the correct message which also includes line change and space (Specific to IE)
                    var messageString =App.editor.ckeditor.getData().replace(/\n/g, "&crarr;");
                    currentCount = $("<div>").html(messageString).text().length;
                    var ckSmileyRegex = /<[\s]*(img|IMG)[\s]+[^>]+smiley\/images\/[^>]+>/gm,
                        smileyArray = messageString.match(ckSmileyRegex),
                        smileyCount = ((smileyArray && smileyArray.length) || 0);
                    currentCount += smileyCount;
                }
                else
                {
                    currentCount =  App.editor.$el.find('textarea').val().length;
                }

                var remainderCount = maxCharacterCount - currentCount;
                App.editor.showCharacterLeft(L10N_CHARACTER_COUNT.replace('{0}', remainderCount));
			}catch(error){}
        },10);
    },
    /**
     Display the number of character left message
     **/
     displayAttachmentIcon : function(){
        $("#cke_1_toolbox").append("<div id='attachmentIcon'><div class='fontDivider'> </div><a href='#openModal' id='attachmentIconTag' class='attachment-icon' title="+L10N_ATTACHMENT_TOOLTIP+"></a></div>");
     },
     showHideAttachmentIcon : function(value, isOffRecord){

            value ? $("#attachmentIcon").show() : $("#attachmentIcon").hide();
            !isOffRecord ? $("#attachmentIcon").attr('class', "") : $("#attachmentIcon").attr('class', "disable-attachment-icon");
            if(isOffRecord)App.attachmentView.setDropNotAllowedMessage(L10N_OFF_RECORD_WARNING_MESSAGE);
            else if(!value)App.attachmentView.setDropNotAllowedMessage(L10N_ATTACHMENT_DISABLED_WARNING_MESSAGE);
            isOffRecord && $.browser.msie ? $("#attachmentIconTag").attr("href","#") : $("#attachmentIconTag").attr("href","#openModal");
     },
    /**
     Display the number of character left message
     **/
    showCharacterLeft :  function(msg){
        this.$el.find("#typing_status").html(msg);
		if($("#typing_status").length == 0) {
			$("#cke_1_toolbox").append("<div id='typing_status'>&nbsp;</div>");
		}

    },
	showAttachMobile :  function(param){
		if(param)
		{
			this.$("#attachment-button").attr('class', "");
			this.$("#attachmentMIcon").attr('class', "")
		}
		else
		{
			this.$("#attachment-button").attr('class', "disable-attachment-icon");
			this.$("#attachmentMIcon").attr('class', "disable-attachment-icon");
		}
    },

    toggleOffRecord : function(event){
        var msg;
        var cmd;
		event.preventDefault();
        if(App.offRecord){

            msg = L10N_ON_RECORD_MESSAGE;
            cmd = 'onrecord';
        }
        else{

            msg = L10N_OFF_RECORD_MESSAGE;
            cmd = 'offrecord';
        }
        if(App.connection.sendSystemMessage(msg,cmd)){
            App.messenger.showNotification(msg,'headline');
            if(cmd == 'offrecord'){
                App.offRecord =true;
                 this.$('#off-record-image').attr("src","chat/img/go-on-record-isolated-btn.png");
				 this.$("#attachment-button").attr('class', "disable-attachment-icon");
            }
            else 
			{
                App.offRecord = false;
                this.$('#off-record-image').attr("src","chat/img/go-off-record-isolated-btn.png");
				if(App.isChatAttachmentEnabled === true)
					this.$("#attachment-button").attr('class', "");

            }
        }
    }
});
