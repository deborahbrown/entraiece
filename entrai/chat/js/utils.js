App.utils = {

    submitUrl: function (url) {

        if (url != null && url != "") {
            if ($.trim(url).toLowerCase().indexOf("mailto:") == 0) {
                App.mailClicked = true;
                window.location.href = url;
                setTimeout(function () {
                    App.mailClicked = false;
                }, 10);
            }
            else
                window.open(url);
        }
    },

    isNameValid: function (name) {
        return true;
    },

    isEmailValid: function (email) {

        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\ ".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA -Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    },

    isPhoneValid: function (phone) {

        return phone.match(/[^0-9 \-\(\)]/) == null;
    },

    isQuestionValid: function (question) {

        return true;
    },
    isVisitorMobileSDKApp: function () {

        return ((typeof iseGainMobile != 'undefined') && iseGainMobile == true)
    },
    isVisitorMobile: function () {

        var ua = navigator.userAgent;
	

        if( ua.match(/(iPhone|iPod|iPad)/) ||
            ua.match(/Blackberry/) ||
            ua.match(/Android/) ||
            ua.match(/(Windows Phone|IEMobile)/)) return true;

	    return false; 		
    },
    isVisitorIos: function () {
        var ua = navigator.userAgent;
        return ua.match(/(iPhone|iPod|iPad)/);
    },
    isVisitorIosPhone: function () {
        var ua = navigator.userAgent;
        return ua.match(/(iPhone|iPod)/);
    },
    isVisitorIosTablet: function () {
        var ua = navigator.userAgent;
        return ua.match(/iPad/);
    },
    isVisitorAndroid: function () {
        var ua = navigator.userAgent;
        return ua.match(/Android/);
    },
    isVisitorWindowsPhone: function () {
        var ua = navigator.userAgent;
        return ua.match(/(Windows Phone|IEMobile)/);
    },
    hasMobile: function () {
        var ua = navigator.userAgent;
        return ua.match(/Mobile/);
    },
    hideAddressBar: function () {
        if (document.documentElement.scrollHeight < window.outerHeight / window.devicePixelRatio)
            document.documentElement.style.height = (window.outerHeight / window.devicePixelRatio) + 'px';
        setTimeout(window.scrollTo(1, 1), 0);

        if (App.utils.isVisitorMobile()) {
            window.scrollTo(0, 1);
        }
    },

    isIE: function () {
        return $.browser.msie || ((navigator.appName == 'Netscape') && (new RegExp("Trident/.*rv:([0-9]{1,}[\.0-9]{0,})").exec(navigator.userAgent) != null));
    },


    /**
     Get formatted message. It will replace all the place holders by value passed in args. For example. Message is : {0} has invited agent {1} for video chat. Passed args is {'<Customer_Name>', '<Agent_Name>'. Then it will replace '{0}' from <Customer_Name> and '{1}' from <Agent_Name> in message
        message - message with place holders.
        args - array which contains values to replace placeholders.
        return - formatted message
    **/
    getFormattedMessage: function (message, args) {
        if (message && args && args.length) {
            var replaceToken = null;
            for (var i = 0; i < args.length && typeof args[i] != 'undefined' && args[i] != null; i++) {
                replaceToken = '{' + i + '}';
                if (message.indexOf(replaceToken) != -1) {
                    message = message.replace(replaceToken, args[i]);
                }
            }
        }
        return message;
    },

    htmlEncode: function (value) {
        //create a in-memory div, set it's inner text(which jQuery automatically encodes), then get the encoded contents back out.
        return $('<div/>').text(value).html();
    },

    htmlDecode: function (value) {
        //create a in-memory div, set it's html, then get the decoded contents back out.
        return $('<div/>').html(value).text();
    },

    isRTLLanguage: function(name){
        return ["ar", "fa", "he", "ku", "ug"].indexOf(name) !== -1;
    },
    /*
     * masker would be used to mask data, based on configured patterns
     * for a department (and channel).
     */
    masker: {
        maskingPatterns: [],
        setMaskingPatternData: function (patterns, htmlTagMatcherRegex, htmlTagMatcherIncr) {
            this.maskingPatterns = patterns;
            this.htmlTagMatcherRegex = htmlTagMatcherRegex;
            this.htmlTagMatcherIncr = htmlTagMatcherIncr;
        },
        maskData: function (htmlData, textData) {
            var patterns = this.maskingPatterns;
            var htmlTagMatcherRegex = this.htmlTagMatcherRegex;
            var groupNumIncr = this.htmlTagMatcherIncr
            var replacementArr = [];
            for (var i = 0; i < patterns.length; i++) {
                var patternObj = patterns[i];
                if (typeof textData != 'undefined' && textData != htmlData)
                    textData = maskMatchingPatterns(textData, patternObj.compiledRegEx, patternObj.maskingChar,
                        patternObj.charToRetainRight, patternObj.charToRetainLeft, parseInt(patternObj.luhnAlgo) == 1, replacementArr);
                else
                    htmlData = maskMatchingPatterns(htmlData, patternObj.compiledRegEx, patternObj.maskingChar,
                        patternObj.charToRetainRight, patternObj.charToRetainLeft, parseInt(patternObj.luhnAlgo) == 1);
            }
            if (typeof textData != 'undefined' && textData != htmlData)
                htmlData = maskHtmlData(replacementArr, htmlData, htmlTagMatcherRegex, groupNumIncr);
            return htmlData;

            function maskHtmlData(replacementArr, htmlData, htmlTagMatcherRegex, groupNumIncr) {
                var maskedHtml = htmlData;
                if (replacementArr.length > 0) {
                    for (var i = 0; i < replacementArr.length; i++) {
                        var maskedStrArr = [];
                        var start = 0;
                        var idx = 0;

                        var replacementObj = replacementArr[i];
                        var replacementKey = replacementObj.orig;
                        replacementKey = enhanceReplacementKey(replacementKey, htmlTagMatcherRegex, replacementObj.startBoundary,
                            replacementObj.endBoundary);
                        var regEx = new RegExp(replacementKey, "g");
                        var match = regEx.exec(maskedHtml);

                        while (match) {
                            if (match.index + match[0].length == start)
                                break;
                            var replacementValue = replacementObj.replaced;
                            replacementValue = enhanceReplacementValue(replacementValue, groupNumIncr, match);
                            maskedStrArr[idx++] = maskedHtml.substring(start, match.index) + replacementValue;
                            start = match.index + match[0].length;
                            match = regEx.exec(maskedHtml);
                        }
                        maskedStrArr[idx++] = maskedHtml.substring(start, maskedHtml.length);
                        maskedHtml = maskedStrArr.join('');
                    }
                }
                return maskedHtml;
            }

            function enhanceReplacementKey(replacementKey, regex, startBoundary, endBoundary) {
                var enhancedKeyArr = [];
                var indx = 0;
                if (startBoundary)
                    enhancedKeyArr[indx++] = "\\b";
                for (var i = 0; i < replacementKey.length; i++) {
                    // Remove \r \n characters from key as they are already taken care of in extra regex added later in this function
                    if (replacementKey.charAt(i) == '\r' || replacementKey.charAt(i) == '\n')
                        continue;
                    enhancedKeyArr[indx++] = replacementKey.charAt(i);
                    // If it is the NOT the last character of the key then insert the extra regex
                    if (i != (replacementKey.length - 1))
                        enhancedKeyArr[indx++] = regex;
                }
                if (endBoundary)
                    enhancedKeyArr[indx++] = "\\b";
                var enhancedKey = enhancedKeyArr.join('').replace(/ /g, '(?: |\u00A0|&nbsp;)').replace(/\u00A0/g, '(?: |\u00A0|&nbsp;)');
                return enhancedKey;
            }

            function enhanceReplacementValue(replacementValue, groupNumIncr, match) {
                var groupNum = 1;
                var enhancedValueArr = [];
                var indx = 0;
                for (var i = 0; i < replacementValue.length; i++) {
                    // Remove \r \n characters from enhanced value as they are already taken care of in captured groups added to the enhanced value later in this function
                    if (replacementValue.charAt(i) == '\r' || replacementValue.charAt(i) == '\n')
                        continue;
                    if (replacementValue.charAt(i) == ' ')
                        enhancedValueArr[indx++] = '&nbsp;';
                    else
                        enhancedValueArr[indx++] = replacementValue.charAt(i);
                    // If it is NOT the last character of the value then add the captured group
                    if (i != (replacementValue.length - 1)) {
                        enhancedValueArr[indx++] = match[groupNum];
                        groupNum = groupNum + groupNumIncr;
                    }
                }
                return enhancedValueArr.join('');
            }

            function maskMatchingPatterns(str, re, maskingChar, numCharsToUnmaskFromRight,
                                          numCharsToUnmaskFromLeft, applyLunhAlgo, replacementArr) {
                var match = re.exec(str);
                //if match not found, return the original string.
                if (!match)
                    return str;
                /*
                 As long as the match is found, replace the matched text
                 with the masked character, honouring the numCharsToUnmaskFromRight and
                 numCharsToUnmaskFromLeft values.
                 */
                var maskedStrArr = [];
                var start = 0;
                var idx = 0;
                var maskedStr = '';
                var prependValue = "";
                var appendValue = "";
                var startWordBoundary = false;
                var endWordBoundary = false;
                while (match) {
                    prependValue = "";
                    appendValue = "";
                    startWordBoundary = false;
                    endWordBoundary = false;
                    if (match.index + match[0].length == start)
                        break;
                    maskedStr = maskChars(match[0],
                        maskingChar, numCharsToUnmaskFromLeft, numCharsToUnmaskFromRight, applyLunhAlgo);
                    maskedStrArr[idx++] = str.substring(start, match.index) + maskedStr;
                    if (replacementArr && (maskedStr != match[0]))//string has been replaced
                    {
                        /* identify the boundary values and set in in the return object, for use.
                         * The boundary value can be a word boundary, or a character surrounding
                         * the word.
                         */
                        if (match.index > 0 && match.index < str.length) {
                            prependValue = str.charAt(match.index - 1);
                            if (!isWordCharacter(prependValue)) {
                                prependValue = "";
                                startWordBoundary = true;
                            }
                        }
                        else
                            startWordBoundary = true;
                        if (re.lastIndex < match.input.length && re.lastIndex <= str.length) {
                            appendValue = str.charAt(re.lastIndex);
                            if (!isWordCharacter(appendValue)) {
                                appendValue = "";
                                endWordBoundary = true;
                            }
                        }
                        else
                            endWordBoundary = true;
                        replacementArr[replacementArr.length] = {
                            orig: (prependValue + match[0] + appendValue),
                            replaced: (prependValue + maskedStr + appendValue),
                            startBoundary: startWordBoundary,
                            endBoundary: endWordBoundary
                        };
                    }
                    start = match.index + match[0].length;
                    match = re.exec(str);
                }
                maskedStrArr[idx++] = str.substring(start, str.length);
                return maskedStrArr.join('');
            }

            function isWordCharacter(s) {
                return /\w/.test(s);
            }

            function maskChars(matchStr, maskingChar, numCharsToUnmaskFromLeft, numCharsToUnmaskFromRight, applyLunhAlgo) {
                if (applyLunhAlgo && !isValidCreditCard(matchStr)) {
                    return matchStr;
                }

                var maskedArr = [];
                var strLength = matchStr.length;
                numCharsToUnmaskFromLeft = numCharsToUnmaskFromLeft <= strLength ? numCharsToUnmaskFromLeft : strLength;
                numCharsToUnmaskFromRight = numCharsToUnmaskFromRight <= strLength ? numCharsToUnmaskFromRight : strLength;

                for (var j = strLength - 1; j >= (strLength - numCharsToUnmaskFromRight); j--) {
                    if (matchStr.charAt(j) == '\n' || matchStr.charAt(j) == '\r')
                        numCharsToUnmaskFromRight++;
                }
                for (var k = 0; k < numCharsToUnmaskFromLeft; k++) {
                    if (matchStr.charAt(k) == '\n' || matchStr.charAt(k) == '\r')
                        numCharsToUnmaskFromLeft++;
                }
                var len = strLength - numCharsToUnmaskFromRight;
                var i = numCharsToUnmaskFromLeft;
                for (; i < len; i++) {
                    if (matchStr.charAt(i) == '\n' || matchStr.charAt(i) == '\r')
                        maskedArr[i] = matchStr.charAt(i);
                    else
                        maskedArr[i] = maskingChar;
                }
                retStr = matchStr.substring(0, numCharsToUnmaskFromLeft) + maskedArr.join('') + matchStr.substring(i, strLength);
                return retStr;
            }

            /* this function checks whether the value
             is a valid credit card No or not, using Luhns algorighm*/
            function isValidCreditCard(value) {
                // accept only digits, dashes or spaces
                if (/[^0-9-\s]+/.test(value)) return false;

                var nCheck = 0, nDigit = 0, bEven = false;
                value = value.replace(/\D/g, "");

                for (var n = value.length - 1; n >= 0; n--) {
                    var cDigit = value.charAt(n),
                        nDigit = parseInt(cDigit, 10);
                    if (bEven) {
                        if ((nDigit *= 2) > 9) nDigit -= 9;
                    }

                    nCheck += nDigit;
                    bEven = !bEven;
                }
                return (nCheck % 10) == 0;
            }
        }
    }
}
