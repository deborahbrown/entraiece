/**
* Hook for BackStoppingTriggered
**/
function onBackStoppingTrigerredHook(egainAddtionalParams)
{

}

/**
* Hook to load different page when back-stopping is triggered,
* Currently in hook, loading default survey page.
**/
function onBackStoppingLoadPageHook()
{
	App.surveyView = new App.SurveyView();
	App.surveyView.render();
}


/**
* Set and override parameters for customization.
* App.eGainCustomParams is global object which can be used to set variables for customizations
* Example to set caseId : App.eGainCustomParams.eglvcaseid = 1115;
* Example to set Back Stopping params: App.eGainCustomParams.backStoppingTime = 40;
*                                      App.eGainCustomParams.backStoppingAction = "complete";
**/
function setConnectionParametersHook()
{

}
