var sessionId = "";
App.Cobrowse = Backbone.Model.extend({
    cbWndHandleArr: new Array(),
    postMessageAllowed:false,
    initialize: function () {
		//bind onlclick handler for handling join cobrowse links
		$(document).ready(function() {
			$(document).on('click', '#egcb_join_link', function(event) {
				event.preventDefault();
				var aTag = event.target ;
				sessionId = $(aTag).attr('egcb-sessionId') ;
				var winName = $(aTag).attr('egcb-winName') ;
				var href = $(aTag).attr('egcb-href') ; // href will be landing page url for tagged cobrowse.
				$(aTag).attr('target', winName) ;
                openCobrowseWindow(href, winName, sessionId) ;
			});
		});
        App.session.on('end', this.stopCobrowseSession, this);
    },
    stopCobrowseSession : function() {
	try{
		var egActId = "";
	    var sid = App.connection.sid;
		if(sid){
	    	egActId = sid.substr(0 , sid.indexOf('_'));
	    }
	    var vhSId = vhtIds?vhtIds.sId:"";
        if(sessionId || egActId || vhSId){
            localStorage.setItem('endCbSession', sessionId+'$eg$'+egActId+'$eg$'+vhSId);
        }
	    if(sessionId && this.cbWndHandleArr[sessionId] && !(this.cbWndHandleArr[sessionId].status==false || (this.cbWndHandleArr[sessionId].handle && this.cbWndHandleArr[sessionId].handle.closed))){
            if(App.cobrowse.postMessageAllowed)
            {
                var arr = [];
                arr[0] = 'endCbSession';
                arr[1] = sessionId+'$eg$'+egActId+'$eg$'+vhSId;
                App.cobrowse.postMessageToCobrowseWindow(this.cbWndHandleArr[sessionId].handle,arr);
            }
        }
		else if(egActId && window.opener){
			var arr = [];
			arr[0] = 'endCbSession';
			arr[1] = 'endsession';
			App.cobrowse.postMessageToCobrowseWindow(window.opener,arr);
		}
	}catch(error){}
    },
    stopCobrowseCallback : function(cbSessionId){
		if(this.cbWndHandleArr[cbSessionId]){
            this.cbWndHandleArr[cbSessionId].status = false;
        }
    },

    openCobrowseWindow: function(url, cbWndName, cbSessionId) {
	//check if window already open
	if(this.cbWndHandleArr[cbSessionId])
	{
			if(this.cbWndHandleArr[cbSessionId].status==false || (this.cbWndHandleArr[cbSessionId].handle && this.cbWndHandleArr[cbSessionId].handle.closed))
			alert(L10N_COBROWSE_SESSION_TERMINATED);
		else
			alert(L10N_COBROWSE_SESSION_ALREADY_JOINED);
	}
	else
	{
		var sid = App.connection.sid;
		var egActId="";
		if(sid){
		 	egActId = sid.substr(0 , sid.indexOf('_'));
	    }
		localStorage.setItem('egActId',egActId);
		localStorage.setItem('cbAutoSessionId',cbSessionId);
		var d = getDimensions(CONFIG_LOGIN_FORM_WIDGET_URL);
		// w,h -- -2: let browser decide, -1: take up available space, +ve values: use them
		var w = eGainLiveConfig.cobrowseWindowWidth || -2;
		var h = eGainLiveConfig.cobrowseWindowHeight || -2;
		if (w == -1) {
			w = window.screen.availWidth - d.width - 15; // 15: to avoid windows going out of screen to some extent - cross browser.
		}
		if (h == -1) {
			h = window.screen.availHeight;
		}
		var l = d.width;

		var options = 'resizable=yes, scrollbars=yes, top=0';
		if (l > 0)
			options += ', left='+l;
		if (w > 0)
			options += ', width='+w;
		if (h > 0)
			options += ', height='+h;
		var wndHandle = window.open(url, cbWndName, options);
		this.cbWndHandleArr[cbSessionId] = {status:true, handle:wndHandle};
		top.moveTo(0, 0);
		if(wndHandle){
            wndHandle.focus();

            App.cobrowse.addMessageListener();
            var keyValueArr = ["dummy","test"];
           /* keyValueArr[keyValueArr.length] = 'egActId';
            keyValueArr[keyValueArr.length] = egActId;
            keyValueArr[keyValueArr.length] = 'cbAutoSessionId';
            keyValueArr[keyValueArr.length] = cbSessionId;*/
            App.cobrowse.postMessageToCobrowseWindow(wndHandle,keyValueArr);
        }

	}
    },
    postMessageToCobrowseWindow:function(windowHandle,msgArr){
        var msgPrefix = "IntMsgForCobrowse:";
        var delimiter = "$egint$";
        var msg = "";
        for(var count = 0; count < msgArr.length;count++){
            msg = msg + msgArr[count] +delimiter ;
        }
        msg = msg.substring(0,msg.lastIndexOf(delimiter));
        var i = 0;
        var time = 2000;
         var intervalCounter = setInterval(function(){
            windowHandle.postMessage("counter:"+intervalCounter+":IntMsgForCobrowse:" + msg,"*");
        },time);
       //invoking clearInterval to make sure, if we don't get response of postMessage within 20 secs, then will clean interval and will assume that postMessage is not working.
        if(!App.cobrowse.postMessageAllowed){
            setTimeout(function(){window.clearInterval(intervalCounter);},time*10);
        }
  },
    addMessageListener:function(){
        var isIE = window.attachEvent != null;
        if (isIE) {
            window.attachEvent("onmessage", App.cobrowse.onPostMessageResp);
        } else {
            window.addEventListener("message", App.cobrowse.onPostMessageResp, false);
        }
    },
    onPostMessageResp:function(e){
        console.log(e.data);
        var arr;
        if(e.data.indexOf("ResponseFromCBWindow:") > -1){
            var counter = ((arr = e.data.split("ResponseFromCBWindow:")).length == 2)?arr[1].trim():-1;
            window.clearInterval(counter);
            App.cobrowse.postMessageAllowed = true;
        }else if (e.data.indexOf("CBSessionClosed:") > -1){
            var sessionId =  ((arr = e.data.split("CBSessionClosed:")).length == 2)?arr[1].trim():-1;
                App.cobrowse.stopCobrowseCallback(sessionId);
        }
    }
});

